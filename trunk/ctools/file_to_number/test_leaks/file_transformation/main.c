#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include "../../file_transformation.h"
#include "../../load_files.h"
#include "string.h"
#include "time.h"
#include "../../state_aho.h"
#include "../../classifier.h"
const char *REMOVE_ACCENTS = "NFD;[:Nonspacing Mark:] Remove;NFC;Lower";
u16_char_buffer *create_id()
{
	int32_t _length=0;
	int32_t _capacity=0;
	UErrorCode _pErrorCode;
	u16_char_buffer *_buffer=(u16_char_buffer *)malloc(sizeof(u16_char_buffer));
	if(_buffer==NULL)
	{
		return NULL;
	}
	_capacity=strlen(REMOVE_ACCENTS);
	_buffer->data=(UChar *)malloc(sizeof(UChar)*_capacity);
	if(_buffer->data==NULL)
	{
		free(_buffer);
		return NULL;
	}
	memset(&_pErrorCode,0,sizeof(UErrorCode));	
	u_strFromUTF8(_buffer->data,_capacity,&_length,REMOVE_ACCENTS,_capacity,&_pErrorCode);
	if(U_FAILURE(_pErrorCode))
	{	
		return NULL;
	}
	_buffer->length=_capacity;
	return _buffer;
}
void write_classifier_document(const char *_file_name,classifier_document *_classifier_document)
{
	
	node_score_buffer *_node_score_buffer=_classifier_document->node_score_buffer;
	node_score _node_score;
	uint32_t _size=_node_score_buffer->current_size;
	uint32_t _i;
	char _out_file_name[200];
	FILE *_fp=NULL;
	sprintf(_out_file_name,"./output/%s",_file_name);
	_fp=fopen(_out_file_name,"w");
	if(_fp==NULL)
	{
		return;
	}
	fprintf(_fp,"node id,document frequency,document coverage,summary frequency,summary coverage,fc,fc^15,fc^20,binary score,roc\n");
	for(_i=0;_i<_size;_i++)
	{
		_node_score=_node_score_buffer->buffer[_i];
		fprintf(_fp,"%zu,%f,%f,%f,%f,%f,%f,%f,%d,%zu\n",_node_score.node_id,_node_score.frequency,_node_score.coverage,_node_score.frequency_top,_node_score.coverage_top,_node_score.fc,_node_score.fc15,_node_score.fc20,_node_score.flags,_node_score.roc);
	}
	fclose(_fp);	
}
void delete_id(u16_char_buffer *_buffer)
{
	free(_buffer->data);
	free(_buffer);
}
int main(int argc,char **argv)
{
	memory_page_buffer *_buffer=NULL;
	clock_t _transform_document_time_old;
	clock_t _transform_document_time_current;
	clock_t _load_time_old=clock();
	clock_t _load_time_current;
	clock_t _all_files_time_old;
	clock_t _all_files_time_current;
	file_transformation *_file_transformation=NULL;
	u16_char_buffer *_id=NULL;
	char_buffer *_page=NULL;
	GHashTable *_hwords=NULL;
	GHashTable *_map_term_node=NULL;
	GHashTable *_map_term_weigth_list=NULL;
	GHashTable *_map_node_must_have=NULL;
	word_poll *_word_poll=NULL;
	state_aho_cache *_state_aho_cache=NULL;
	unfiltered_node_list_cache *_unfiltered_node_list_cache=NULL;
	classifier *_classifier=NULL;
	unfiltered_node_list *_unfiltered_node_list=load_unfiltered_nodes("unfiltered_nodes.data",&_buffer);
	classifier_document *_classifier_document=NULL;
	int _firts_line=0;
	FILE *_fp=NULL;
	DIR *_dir=NULL;
	struct dirent *_dirent=NULL;
	_id=create_id();
	_hwords=load_words("words.data",&_buffer);
	_classifier_document=create_classifier_document(&_buffer);
	_state_aho_cache=load_state_aho("state.aho",&_buffer);
	_word_poll=create_word_poll(&_buffer);
	_page=create_char_buffer(&_buffer);
	_map_term_node=load_list_node("list_node.data",&_buffer);
	_map_node_must_have=load_node_musthave_term("node_musthave.data",&_buffer);
	_map_term_weigth_list=load_node_sig_term("node_sig_vector.data",&_buffer);
	_dir=opendir(argv[1]);	
	if(_dir==NULL)
	{
		return -1;
	}
	if(_state_aho_cache==NULL)
	{
		printf("************** fail load state aho cache *****************\n");
		return -1;
	}
	_classifier=create_classifier(_state_aho_cache);
	if(_classifier==NULL)
	{
		printf("************** fail crate classifier *****************\n");
		return -1;	
	}
	if(_map_term_node==NULL)
	{
		printf("************** fail load list node *****************\n");
		return -1;
	}
	if(_unfiltered_node_list==NULL)
	{
		printf("************** unfiltered node list *****************\n");
		return -1;
	}
	_unfiltered_node_list_cache=build_unfiltered_node_list_cache(_unfiltered_node_list);
	if(_unfiltered_node_list_cache==NULL)
	{
		printf("************** unfiltered node list cache  *****************\n");
		return -1;
	}
	if(_map_node_must_have==NULL)
	{
		printf("************** node must have *****************\n");
		return -1;	
	}
	if(_map_term_weigth_list==NULL)
	{
		printf("************** fail load map term weight list *****************\n");
		return -1;
	}
	set_unfilterd_node(_classifier,_unfiltered_node_list);
	set_map_term_node(_classifier,_map_term_node);
	set_map_node_must_have(_classifier,_map_node_must_have);
	set_map_term_weight_list(_classifier,_map_term_weigth_list);
	set_unfiltered_node_list_cache(_classifier,_unfiltered_node_list_cache);
	if(_classifier_document==NULL)
	{
		printf("************** fail crate classifier document *****************\n");
		return -1;
	}
	if(_word_poll==NULL)
	{
		printf("******** error to create word poll  ************\n");
		return -1;
	}
	if(_hwords==NULL)
	{
		printf("******** error to load words  ************\n");
		return -1;
	}
	if(_id==NULL)
	{
		printf("******** error create id  ************\n");
		return -1;
	}
	if(chdir(argv[1])<0)
	{
		return -1;
	}
	
	_file_transformation=create_file_transformation(&_buffer,_id,_hwords);
		
	if(_file_transformation==NULL)
	{
		printf("********* error to create file transformation ********\n");
		return -1;
	}
	_load_time_current=clock();	
	printf("load time %f\n",(float)(_load_time_current-_load_time_old)/CLOCKS_PER_SEC);
	_all_files_time_old=clock();
	while((_dirent=readdir(_dir))!=NULL)
	{
		_firts_line=1;	
		if(_dirent->d_type!=DT_REG)
		{		
			continue;
		}
		if(_dirent->d_name==NULL)
		{
			continue;
		}
		printf("************* process new file %s **************\n",_dirent->d_name);	
		_fp=fopen(_dirent->d_name,"r");
		if(_fp==NULL)
		{
			continue;
		}
		_transform_document_time_old=clock();		
		while(fgets(_page->buffer,_page->capacity,_fp)!=NULL)
		{
			_page->size=strlen(_page->buffer);
			if(word_to_number(_file_transformation,_page,_word_poll)<0)
			{
				printf("********* error to convert word to number ********\n");
				continue;	
			}
			if(_firts_line!=0)
			{
				_classifier_document->title_size=_word_poll->current_size;
/*				printf("***************  title words size %d **************\n",_classifier_document->title_size);*/
			}
			_firts_line=0;
		}
		_transform_document_time_current=clock();
		printf("time for transform document is %f\n",(float)(_transform_document_time_current-_transform_document_time_old)/CLOCKS_PER_SEC);
/*		printf("*************** document word size %d ***********\n",_word_poll->current_size);
		printf("************* classify document %s **************\n",_dirent->d_name);*/
		classify(_classifier,_classifier_document,_word_poll);
		write_classifier_document(_dirent->d_name,_classifier_document);
/*		printf("************* clean classifier document **************\n");*/
		clean_classifier_document(_classifier_document);
		fclose(_fp);
/*		if(_word_poll->page->memory_size!=WORD_POLL_MEMORY_SIZE)
		{
			realloc_word_poll(_word_poll,WORD_POLL_MEMORY_SIZE);
			printf("************* reduce memory page from main **************\n");
			printf("************* memory poll size %d ***********\n",_word_poll->poll_size);
		}	*/
		_word_poll->current_size=0;
		printf("\n\n\n\n");
	}
	_all_files_time_current=clock();
	printf("time for process all files %f\n",(float)(_all_files_time_current-_all_files_time_old)/CLOCKS_PER_SEC);
	delete_id(_id);
	delete_word_poll(_word_poll);
	delete_char_buffer(_page);
	delete_file_transformation(_file_transformation);
	delete_classifier_document(_classifier_document);
	delete_classifier(_classifier);
	delete_state_aho_cache(_state_aho_cache);
	g_hash_table_destroy(_hwords);
	g_hash_table_destroy(_map_term_node);
	g_hash_table_destroy(_map_term_weigth_list);
	g_hash_table_destroy(_map_node_must_have);
	delete_unfiltered_node_list_cache(_unfiltered_node_list_cache);
	u_cleanup();
	closedir(_dir);
	printf("********* free memory pages ***************\n");
	free_memory_page_buffer(_buffer);
	return 0;
}
