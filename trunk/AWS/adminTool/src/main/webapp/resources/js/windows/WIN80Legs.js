/**
 * @author Andres Restrepo
 * window to 80 legs process
 */

var WIN80Legs = CToolWindow.extend({

    selectedFiles : [],
    truncated:{},
    marker:{},
    //@override
    loadContent : function(resource) {
        var that = this;
        $('#ajaxLoading').css("visibility","visible");
        $.getJSON(resource, function(data) {
            console.log(data.keys);
            var datasource = new kendo.data.DataSource({
                data : data.keys,
                pageSize : 20
            });
            
            that.truncated=data.truncated;
            that.marker=data.marker;
                        
            if(that.truncated){
                $("#nextPaginationBtn").css("visibility","visible");
            }else{
                $("#nextPaginationBtn").css("visibility","hidden");
            }
            
            if(!that.dataSourceLastReference)
            {
                $("#pager").kendoPager({
                    dataSource : datasource
                });
                that.dataSourceLastReference=datasource;
            }
            else
            {
                that.dataSourceLastReference.data(data.keys);
                that.dataSourceLastReference.page(1);
            }
            
            //$("#pager").trigger("change");
            
            $("#listView").kendoListView({
                dataSource : datasource,
                selectable : "multiple",
                dataBound : onDataBound,
                change : onChange,
                template : kendo.template($("#template").html())
            });
            
            


            function onDataBound() {

            }

            function onChange() {
                var data = datasource.view(), selected = $.map(this.select(), function(item) {
                    return data[$(item).index()].name;
                });
                that.selectedFiles = selected;
            }

        })
        .success(function() {
           
        })
        .error(function() {
            alert("Error loading content");
        })
        .complete(function() {
             $('#ajaxLoading').css("visibility","hidden");
             
        });
    },

    uploadFilesSelected : function() {
        //send data post
        var json = {};
        json.resources = [];
        json.resources = this.selectedFiles;
        $.ajax({
            type : 'POST',
            contentType : 'application/json',
            url : 'services/s3/process',
            dataType : 'json',
            data : JSON.stringify(json),
            success : function(data) {
                console.log("send data ok");
                alert("send data ok");
            }
        });
    },
        
    nextPagination : function (){
        url = "services/s3/80legs/nextPage?callback=?";
        url = url + "&marker=" + this.marker;
        this.loadContent(url);
    }
});

