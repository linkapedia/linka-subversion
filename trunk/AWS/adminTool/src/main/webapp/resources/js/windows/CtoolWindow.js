/**
 * @author Andres Restrepo
 *
 * Abstract class for the windows
 */

var CToolWindow = kendo.Class.extend({

    divContent : 'null',
    title : 'console tool',
    win : null,

    //Constructor
    init : function(divContent, title) {
        if (divContent) {
            this.divContent = divContent;
        }
        if (title) {
            this.title = title;
        }
    },

    createWindow : function() {
        var window = $("#" + this.divContent);
        if (!window.data("kendoWindow")) {
            window.kendoWindow({
                width : "1053px",
                height : "409px",
                actions : ["Minimize", "Maximize", "Close"],
                title : this.title,
                visible : false
            });
        }
        this.win = window;
    },

    getWindow : function() {
        return this.win;
    },

    show : function(win) {
        win.data("kendoWindow").open();
    },
	
    loadContent : function() {}
});
