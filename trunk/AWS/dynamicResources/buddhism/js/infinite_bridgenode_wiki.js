// created by Juan Idrobo

var nextIndexObject = null;
var nObjects = null;
var end = false;
var lock = false;
function loadNextLinks(nodeId, nextIndex, nObjectsToRetrieve) {
    var result = $("#content").css("display");
    if (!end && !lock && result == 'block') {
        console.log("infinite.js scroll");
        lock = true;
        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getLinkObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            result = $("#content").css("display");
            if (result == 'block') {
                createLinkObjectsForMasonry(obj, nodeId, nextIndexObject);
                nextIndexObject = obj.nextIndexObject;
                if (obj.end) {
                    end = true;
                }

            } else
            {
                lock = false;
            }
        });

    }
}

function loadNextNodes(nodeId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getNodeObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            createNodeObjectsForMasonry(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function loadNextUserFeeds(userId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        //TODO infinie scroll div 
        //$("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getUserFeedObjects/" + userId + "/" + nextIndexObject + "/" + nObjects, function(obj) {
            //alert (obj);
            createUserFeedObjects(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function createLinkObjectsForMasonry(obj, nodeId, nextIndex) {

    var objects = obj.linkObjectList;
    var box = "";
    var digestUrl;

    for (var i = 0; i < objects.length; i++) {

        digestUrl = CONTEXT_LINKAPEDIA+"/digest/" + objects[i].encodeTitle + "/" + nodeId + "/" + objects[i].fileName;
        box += "<div class='topics2'>";
        box += "<h2><a style='text-decoration:none;color:#0645ad;' href='"+digestUrl+"'>"+objects[i].title+"</a></h2>";
        box += "<div class='body_text'>";
        if (objects[i].imageSrc != null) {
            box += "<img class='show_pic' alt='' src='" + objects[i].imageSrc + "'/>";
        }
        box += "<p>"+objects[i].digest+"</p>";
        box += "</div>";
        box += "</div>";
    }

	$("#digests").append($(box));
	
    $("#infinite_scroll_loading").hide();
    lock = false;

}

function createNodeObjectsForMasonry(obj) {

    var objects = obj.nodeObjectList;
    var $box;
    var description = "";
    var sub = 320;

	//get the path from the nodes before loaded
	var s = $('#bridgeNodeContent').find(".topics:first").find("a").attr("href")
	var u = s.split("/");
	var ii =0;
	var page="";
	while(ii<u.length){
	   if(u[ii]=="page"){
		if(ii+1<u.length){
			page=u[ii+1];
			break;	
		}
	   }
	   ii++;
	}

    for (var i = 0; i < objects.length; i++) {


	var urlNode;
	if(page==""){
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}else{
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+page+"/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}
        description = objects[i].description;
        
        if(description){
			if(description.length > sub){
				description = clipText(description,sub);
			}
		}        
		
		description = description+"...";
		$box = $("<div class='topics'><a href='"+urlNode+"'><img class='show_pic' alt='' src='"+objects[i].imageSrc+"' onError='this.src=\"https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png\"' ></a>"+
                "<div class='body_text'>"+
                "<h2><a href='"+urlNode+"'>"+objects[i].name+"</a></h2>"+
                "<p class='bridge_text'>"+description+"</p>"+
                "</div>"+
                "</div>");	

        $('#bridgeNodeContent').append($box);
    }    
    $("#infinite_scroll_loading").hide();
    lock = false;
}

function createUserFeedObjects(obj) {

    var objects = obj.eventsList;
    for (var i = 0; i < objects.length; i++) {
        //$(".activity_list p.show").first().removeClass().addClass('no_show').hide('slow');
        
        $(".activity_list").append($(objects[i]));
       $(".activity_list p.show").last().hide().show(2000);
        //$(".activity_list:last-child").find("abbr.timeago").timeago();
        
      $(".activity_list p").last().find("abbr.timeago").timeago();
      
    }
    lock = false;

}

function infiniteLinkScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextLinks(nodeId, nextIndex, nObjects);

        }
    });

}

function infiniteNodeScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextNodes(nodeId, nextIndex, nObjects);
        }
    });

}

function infiniteUserFeedScroll(userId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            loadNextUserFeeds(userId, nextIndex, nObjects);
        }
    });

}


