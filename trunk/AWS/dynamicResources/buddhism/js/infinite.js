// created by Juan Idrobo

var nextIndexObject = null;
var nObjects = null;
var end = false;
var lock = false;
function loadNextLinks(nodeId, nextIndex, nObjectsToRetrieve) {
    var result = $("#content").css("display");
    if (!end && !lock && result == 'block') {
        console.log("infinite.js scroll");
        lock = true;
        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getLinkObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            result = $("#content").css("display");
            if (result == 'block') {
                createLinkObjectsForMasonry(obj, nodeId, nextIndexObject);
                nextIndexObject = obj.nextIndexObject;
                if (obj.end) {
                    end = true;
                }

            } else
            {
                lock = false;
            }
        });

    }
}

function loadNextNodes(nodeId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getNodeObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            createNodeObjectsForMasonry(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function loadNextUserFeeds(userId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        //TODO infinie scroll div 
        //$("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getUserFeedObjects/" + userId + "/" + nextIndexObject + "/" + nObjects, function(obj) {
            //alert (obj);
            createUserFeedObjects(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function createLinkObjectsForMasonry(obj, nodeId, nextIndex) {

    var objects = obj.linkObjectList;
    var box;
    var digestUrl;

    frag = document.createDocumentFragment();

    for (var i = 0; i < objects.length; i++) {

        digestUrl = CONTEXT_LINKAPEDIA+"/digest/" + objects[i].encodeTitle + "/" + nodeId + "/" + objects[i].fileName;
        box = "<div class=\"box " + objects[i].cols + " masonry-brick\" >";
        if (objects[i].imageSrc != null) {
            box += "<a href='" + digestUrl + "'><img class='linkImage' src='" + objects[i].imageSrc + "' width='inherit'></a><br>"
        }
        //WITH PLUS AND MINUS box += "<a href='" + digestUrl + "'><h2>" + objects[i].title + "</h2></a><br>" + "<p class='body_text'>" + objects[i].digest + "<br><br>" + "</p>" + "<ul class=\"buttons\" style=\"float: right;\">" + "<li><a href=\"javascript:void(0);\" class=\"save_b\"><span>Save</span></a><a href=\"javascript:void(0);\" class=\"add_b\"><span></span></a><a href=\"javascript:void(0);\" class=\"remove_b\"><span></span></a> </li>" + "</ul>" + "</div>";
        box += "<a href='" + digestUrl + "'><h2 class='LinkTitle'>" + objects[i].title + "</h2></a><br>" + "<p class='body_text'>" + objects[i].digest + "<br><br>" + "</p>" + "<ul class=\"buttons\" style=\"float: right;\">" + "<li><a href=\"#\" data-docId='"+objects[i].fileName+"' class=\"save_b saveDocumentLink\"><span>Save</span></a></li>" + "</ul>" + "</div>";

        frag.appendChild($(box)[0]);
    }

    var $newElems = $(frag.childNodes).css({
        opacity: 0
    });
    // ensure that images load before adding to masonry layout
    $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({
            opacity: 1
        });
        $('#digests').append($newElems).masonry('appended', $newElems, true);
    });

    $("#infinite_scroll_loading").hide();
    lock = false;

}

function createNodeObjectsForMasonry(obj) {

    var objects = obj.nodeObjectList;
    var $box;

	//get the path from the nodes before loaded
	var s = $('#bridgeNodeContent').find(".topics2:first").find("a").attr("href")
	var u = s.split("/");
	var ii =0;
	var page="";
	while(ii<u.length){
	   if(u[ii]=="page"){
		if(ii+1<u.length){
			page=u[ii+1];
			break;	
		}
	   }
	   ii++;
	}

    for (var i = 0; i < objects.length; i++) {


	var urlNode;
	if(page==""){
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}else{
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+page+"/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}

        $box = $("<div class=\"topics2 boxBridgeNode\" >" + "<img src=\"" + objects[i].imageSrc + "\" alt=\"\" class=\"pic_bg\"  >" + "<div class=\"text_bg1 text_bg2\"></div>" + "<div class=\"topics_text\">" + "<div class=\"topics_title\">" + "<a href=\""+urlNode+"\" title=\"" + objects[i].name + "\">" + objects[i].name + "</a>" + "</div>" + "<p class=\"body_text body_text2\">" + objects[i].description + "</p>" + "</div>" + "</div>");

        $box.css({
            opacity: 0
        });

        $('#bridgeNodeContent').append($box).masonry('appended', $box, true);

        $box.animate({
            opacity: 1
        }, {
            duration: 2000

        });

    }
    bridgeNodeEffect();
    //bridgeNode.js
    $("#infinite_scroll_loading").hide();

    lock = false;

}

function createUserFeedObjects(obj) {

    var objects = obj.eventsList;
    for (var i = 0; i < objects.length; i++) {
        //$(".activity_list p.show").first().removeClass().addClass('no_show').hide('slow');
        
        $(".activity_list").append($(objects[i]));
       $(".activity_list p.show").last().hide().show(2000);
        //$(".activity_list:last-child").find("abbr.timeago").timeago();
        
      $(".activity_list p").last().find("abbr.timeago").timeago();
      
    }
    lock = false;

}

function infiniteLinkScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextLinks(nodeId, nextIndex, nObjects);

        }
    });

}

function infiniteNodeScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextNodes(nodeId, nextIndex, nObjects);
        }
    });

}

function infiniteUserFeedScroll(userId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            loadNextUserFeeds(userId, nextIndex, nObjects);
        }
    });

}


