// created by Juan Idrobo

var nextIndexObject = null;
var nObjects = null;
var end = false;
var lock = false;
function loadNextLinks(nodeId, nextIndex, nObjectsToRetrieve) {
    var result = $("#content").css("display");
    if (!end && !lock && result == 'block') {
        console.log("infinite.js scroll");
        lock = true;
        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getLinkObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            result = $("#content").css("display");
            if (result == 'block') {
                createLinkObjectsForMasonry(obj, nodeId, nextIndexObject);
                nextIndexObject = obj.nextIndexObject;
                if (obj.end) {
                    end = true;
                }

            } else
            {
                lock = false;
            }
        });

    }
}

function loadNextNodes(nodeId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        $("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getNodeObjects/" + nodeId + "/" + nextIndexObject + "/" + nObjects, function(obj) {

            createNodeObjectsForMasonry(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function loadNextUserFeeds(userId, nextIndex, nObjectsToRetrieve) {
    if (!end && !lock) {
        lock = true;

        if (nextIndexObject == null) {
            nextIndexObject = nextIndex;
        }
        if (nObjects == null) {
            nObjects = nObjectsToRetrieve;
        }

        //TODO infinie scroll div 
        //$("#infinite_scroll_loading").show();
        //alert (1);
        $.getJSON(CONTEXT_LINKAPEDIA+"/rest/paginator/getUserFeedObjects/" + userId + "/" + nextIndexObject + "/" + nObjects, function(obj) {
            //alert (obj);
            createUserFeedObjects(obj);
            nextIndexObject = obj.nextIndexObject;
            if (obj.end) {
                end = true;
            }
        });

    }
}

function createLinkObjectsForMasonry(obj, nodeId, nextIndex) {

    var objects = obj.linkObjectList;
    var box;
    var digestUrl;

    frag = document.createDocumentFragment();

    for (var i = 0; i < objects.length; i++) {

        digestUrl = CONTEXT_LINKAPEDIA+"/digest/" + objects[i].encodeTitle + "/" + nodeId + "/" + objects[i].fileName;
        box = "<div class=\"box " + objects[i].cols + " masonry-brick\" >";
        if (objects[i].imageSrc != null) {
            box += "<a href='" + digestUrl + "'><img class='linkImage' src='" + objects[i].imageSrc + "' width='inherit'></a><br>"
        }
        //WITH PLUS AND MINUS box += "<a href='" + digestUrl + "'><h2>" + objects[i].title + "</h2></a><br>" + "<p class='body_text'>" + objects[i].digest + "<br><br>" + "</p>" + "<ul class=\"buttons\" style=\"float: right;\">" + "<li><a href=\"javascript:void(0);\" class=\"save_b\"><span>Save</span></a><a href=\"javascript:void(0);\" class=\"add_b\"><span></span></a><a href=\"javascript:void(0);\" class=\"remove_b\"><span></span></a> </li>" + "</ul>" + "</div>";
        box += "<a href='" + digestUrl + "'><h2 class='LinkTitle'>" + objects[i].title + "</h2></a><br>" + "<p class='body_text'>" + objects[i].digest + "<br><br>" + "</p>" + "<ul class=\"buttons\" style=\"float: right;\">" + "<li><a href=\"#\" data-docId='"+objects[i].fileName+"' class=\"save_b saveDocumentLink\"><span>Save</span></a></li>" + "</ul>" + "</div>";

        frag.appendChild($(box)[0]);
    }

    var $newElems = $(frag.childNodes).css({
        opacity: 0
    });
    // ensure that images load before adding to masonry layout
    $newElems.imagesLoaded(function() {
        // show elems now they're ready
        $newElems.animate({
            opacity: 1
        });
        $('#digests').append($newElems).masonry('appended', $newElems, true);
    });

    $("#infinite_scroll_loading").hide();
    lock = false;

}

function createNodeObjectsForMasonry(obj) {

    var objects = obj.nodeObjectList;
    var $box;	
	//get the path from the nodes before loaded
	var s = $('#bridgeNodeContent').find(".topics2:first").find("a").attr("href")
	var u = s.split("/");
	var ii =0;
	var page="";
	while(ii<u.length){
	   if(u[ii]=="page"){
		if(ii+1<u.length){
			page=u[ii+1];
			break;	
		}
	   }
	   ii++;
	}

    for (var i = 0; i < objects.length; i++) {


	var urlNode;
	if(page==""){
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}else{
		urlNode=CONTEXT_LINKAPEDIA+"/page/"+page+"/"+objects[i].encodeName+"/"+objects[i].nodeId;
	}

        $box = $("<div class=\"topics2 boxBridgeNode\" >" + "<img id=\"img_"+objects[i].nodeId+"\" src=\"" + objects[i].imageSrc + "\" alt=\"\" class=\"pic_bg\"  >" + "<div class=\"text_bg1 text_bg2\"></div>" + "<div class=\"topics_text\">" + "<div class=\"topics_title\">" + "<a href=\""+urlNode+"\" title=\"" + objects[i].name + "\">" + objects[i].name + "</a>" + "</div>" + "<p class=\"body_text body_text2\">" + objects[i].description + "</p>" + "</div>" + "</div>");

        $box.css({
            opacity: 0
        });

        $('#bridgeNodeContent').append($box).masonry('appended', $box, true);

        $box.animate({
            opacity: 1
        }, {
            duration: 2000

        });
        
       	$($box).click(function(e){	
			var url = $(this).find(".topics_title > a").attr("href"); 			
			location.href = url;
		});
        
		checkImage(obj.corpusIdReverse, objects[i].nodeId);
    }
    bridgeNodeEffect();
    //bridgeNode.js
    $("#infinite_scroll_loading").hide();

    lock = false;

}

function createUserFeedObjects(obj) {

    var objects = obj.eventsList;
    for (var i = 0; i < objects.length; i++) {
        //$(".activity_list p.show").first().removeClass().addClass('no_show').hide('slow');
        
        $(".activity_list").append($(objects[i]));
       $(".activity_list p.show").last().hide().show(2000);
        //$(".activity_list:last-child").find("abbr.timeago").timeago();
        
      $(".activity_list p").last().find("abbr.timeago").timeago();
      
    }
    lock = false;

}

function infiniteLinkScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextLinks(nodeId, nextIndex, nObjects);

        }
    });

}

function infiniteNodeScroll(nodeId, nextIndex, nObjects) {

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {

            loadNextNodes(nodeId, nextIndex, nObjects);
        }
    });

}

function infiniteUserFeedScroll(userId, nextIndex, nObjects) {

    $(window).scroll(function() {

        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            loadNextUserFeeds(userId, nextIndex, nObjects);
        }
    });

}

//new code for infinite scroll with wookmark
//create for Alexander Agudelo
var limit = 0;
var current_page = null;
var lockscroll = false;
var nodeid = null;
var container = null;
var page = 1;
var totalPages = null;
var nextpage = null;
var linkRestObject = new Rest();

function setInfiniteScroll(_currentpage, _limit, containerData, nodeId){
	if(nodeId){
		nodeid = nodeId;
	}
	if(containerData){
		container = $(containerData);
	}else{
		console.log("Error: Bad argument for containerData");
		return null;
	}	
	if(_limit){
		limit = _limit;
	}else{
		console.log("Error: Bad argument for limit");
		return null;
	}
	if(_currentpage){
		current_page = _currentpage;
		
		if(current_page == "linkpage"){
			getFirstPageLink();
		}else if(current_page == "bridgenodepage"){
			getFirstNodePage();
		}
	}else{
		console.log("Error: Bad argument for currentpage");
		return null;
	}			
	initInfinite();
}

function initInfinite(){	
	$(window).scroll(function() {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 150) {
            if(!lockscroll){
				lockscroll = true;
				if(current_page == "linkpage"){
					linkPageScroll();
				}
			}
        }
    });
}

function linkPageScroll(){
	try{			
			if(page < totalPages){						
				getMoreLink();				
			}else{								
				lockscroll = true;
				return;
			}					
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function getFirstPageLink(){
	try{
		var linkRest = new Rest();
		linkRest.getDocumentsByPage({
			topicId : nodeid,
			page : 1,
			success : function(data){
				if(data.documents.length > 0){
					totalPages = data.totalPages;
					paintDocsLink(data);
					moreDocsHidden(linkRest);
				}else{
					lockscroll = true;
					container.append("<p class='body_text' style='font-size: 1.3em; padding: 10px 0; margin-left: 20px;'>We are still gathering information for this topic. Click 'join this topic' to be notified when we have new information.</p>");
				}
			},
			error : function(err){
				$("html").html(err);
			}
		});
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function getMoreLink(){
	try{			
		paintDocsLink(nextpage);		
		moreDocsHidden();
	}catch(err){
		console.log("Error: " + err.message);
	}
}

function moreDocsHidden(){
	try{			
			if(page < totalPages){											
				page++;
				linkRestObject.getDocumentsByPage({
					topicId : nodeid,
					page : page,
					success : function(data){
						nextpage = data;
					},
					error : function(err){
						$("html").html(err);
					}
				});			
			}else{
				lockscroll = true;
				return;
			}
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function paintDocsLink(data){
	try{
		if(data.documents.length == 0){
			return;
		}
		var documents = data.documents;
		var html = "";
		var nameEncoded = "";		
		var urlImage = "";
			$("#totalDigest").text(data.totalItems);								
			$.each(documents, function(index, doc){					
				if(doc.digest != ""){		
					nameEncoded = encodeFileName(doc.title);	
					urlImage = _URL_DOCUMENT_IMAGE + doc.id + "_r3.jpg";
					html += '<div class="box">';
					html += '<a href="' + CONTEXT_LINKAPEDIA +'/digest/' + nameEncoded + '/' + nodeid + '/' + doc.id + '">' +
							'<img class="linkImage" src="' + urlImage + '" width="inherit"/></a><br>';						
					html += '<a href="' + CONTEXT_LINKAPEDIA + '/digest/' + nameEncoded + '/' + nodeid + '/' + doc.id + '">' +
							'<h2 class="LinkTitle">' + cutText(purgeEncoding(doc.title), 70, "...") + '</h2></a><br>' +
							'<p class="body_text">' + purgeEncoding(doc.digest) + '</p>';
					html += '<ul class="buttons" style="float: right;">' +
								  '<li>';
									 if(USER_ROLE == "guest"){
										 html += '<a href="javascript:void(0)" class="save_b" onclick="login()"><span>Save</span></a>';
									 }else{
										 html += '<a href="#" data-docId=' + doc.id + ' class="save_b saveDocumentLink"><span>Save</span></a>'; 
									 }
					html += '</li>' +
							'</ul>';
					html += '</div>';		
				}					
			});
			
			var html = $(html);
			$(html.filter(".box")).css("width", "270px");
			$(html.find(".linkImage")).error(function(){
				$(this).css("display", "none");
				//$(this).attr("src", "https://s3.amazonaws.com/resourcesweb/newdev/images/notImage.jpg");
			});
			container.append(html);					
			$(html.find(".linkImage")).imagesLoaded(function(){
				$('.box').wookmark({
				  align: 'left',
				  autoResize: true,
				  container: $('#digests'),
				  itemWidth: 0,
				  offset: 8,
				  resizeDelay: 50,
				  flexibleWidth: 0,
				  onLayoutChanged: undefined
				});						
			});												
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function getFirstNodePage(){
	try{
		var bridgeRest = new Rest();
		bridgeRest.getTopicInfo({
			topicId : nodeid,
			success : function(data){
				if(data.children.length > 0){
					$(".body_text").text(data.description);
					paintNodes(data);
				}else{
					$(".body_text").text(message_empty);
				}
			},
			error : function(err){
				$("html").html(err);
			}
		});
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function paintNodes(data){
	try{
		console.log("[paintNodes()]");	
		var html = "";
		var urlImage = "";	
		if(data.children.length == 0){
			return;
		}
		$.each(data.children, function(index, topic){
			urlImage = "https://s3.amazonaws.com/nodeimages/" + topic.id + "/images/" + topic.id + "_0_nocrop.jpg";			
			html += "<div class='topics2 boxBridgeNode'>";
			html += "<img src='" + urlImage + "' alt='' class='pic_bg' />";
			html += "<div class='text_bg1 text_bg2'></div>";
			html += "<div class='topics_text'>";
			html += "<div class='topics_title'>";
			html += "<a href='" + CONTEXT_LINKAPEDIA + "/page/" + encodeFileName(data.title) + "/" + encodeFileName(topic.title) + "/" + topic.id + "'>" + topic.title + "</a>";
			html += "</div>";
			html += "<p class='body_text body_text2'>" + topic.description + "</p>";
			html += "</div>";
			html += "</div>";
		});
		
			html = $(html);		
			$(html.find("img")).error(function(){
				$(this).attr("src", "https://s3.amazonaws.com/resourcesweb/newdev/images/notImage.jpg");
			});
			container.append(html);					
			$(html.find("img")).imagesLoaded(function(){
				$('.boxBridgeNode').wookmark({
				  align: 'left',
				  autoResize: true,
				  container: $('#bridgeNodeContent'),
				  itemWidth: 270,				  
				  offset: 10,
				  resizeDelay: 50,
				  flexibleWidth: 0,
				  onLayoutChanged: undefined
				});						
			});
			$(".topics2").click(function(e){	
				var url = $(this).find(".topics_title > a").attr("href"); 			
				location.href = url;
			});
			bridgeNodeEffect();
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function testRest(url, totalcalls){
	try{
		currentcall = 0;
		for(i = 0; i < totalcalls; i++){
			getData(url,function(data){
				console.log("done->" + (++currentcall));
				});
			}
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function testRestTaxonomie(id, maxTopics, maxChildren){
	try{
		var urlTaxonomie = "http://linkapedia-rest-dev.elasticbeanstalk.com/taxonomies/"+id+"/info";
		var urlTopics = "";
		var requestdone = 0;
		getData(urlTaxonomie, function(data){
			var topics = data.topics;
			requestdone++;
			console.log("Done->" + requestdone);
			$.each(topics, function(index, topic){
				if(index > maxTopics){
					return;
				}
				$.each(topic.children, function(index2, children){
					if(index2 > maxChildren){
						return;
					}
					urlTopics = "http://linkapedia-rest-dev.elasticbeanstalk.com/topics/"+children.id+"/info";
					getData(urlTopics, function(data){						
						requestdone++;
						console.log("Done->" + requestdone);
					});
				});
			});
		});
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}
