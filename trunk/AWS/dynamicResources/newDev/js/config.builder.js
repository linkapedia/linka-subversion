var CONFIGBUILDER = {
	uri_rest : "http://linkapedia-rest-dev.elasticbeanstalk.com",
	uri_document_image : "https://s3.amazonaws.com/test_new_image/",
	messages : {
		message_empty : "We are still gathering information for this topic. Click 'join this topic' to be notified when we have new information."
	}
};
