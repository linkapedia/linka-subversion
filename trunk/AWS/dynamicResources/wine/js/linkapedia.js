var instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item3">Browse</div><div id="item4">Explore </div> </div><div id="instructional"></div>');
function load() {
 //menu
    window.api = $("#slider1").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider2").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider3").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
}
function loadLinkPage(nodeId, infiniteScrollInit, infiniteScrollNobjects )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    }); 
    $('.fluid .box.col2').css("min-width","430px");
	$('.fluid .box.col1').css("min-width","200px");
    container.imagesLoaded(function() {
		$('.box').css("width","21%");
        container.masonry({
            itemSelector : '.box'/*,
            columnWidth : function(containerWidth) {				
                return containerWidth / 4;
            }*/
        });
                
        $(window).resize(function(){
			container.masonry( 'reload' );
			container.masonry( 'reloadItems' );
		});        
            /*var isiPad = navigator.userAgent.match(/iPad/i) != null;
            if(!isiPad){
                $('.fluid .box.col2').css("min-width","430px");
                $('.fluid .box.col1').css("min-width","200px");
            }*/
        infiniteLinkScroll(nodeId,infiniteScrollInit,infiniteScrollNobjects);
        //show the porlets slowly
        $('#digests').animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
	changeSourceMetaImg("linkpage");
	_kmq.push(['record', 'Link Page']);  //KISSMETRIC EVENT
    $(".titleNodeLinkpage").click(function(e){
		_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
	overlay();
}

function loadBridgeNodePage()
{
    //masonry
    var container = $('#bridgeNodeContent');
    //hide the container of portlets
    container.css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        
        container.masonry({
            itemSelector : '.boxBridgeNode',
           /* columnWidth : function(containerWidth) {

                return containerWidth / 4   ;
   
            }
            */
           columnWidth:70
        });

        //show the porlets slowly
        container.animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
    
    
    bridgeNodeEffect(); //bridgeNode.js
    changeSourceMetaImg("bridgepage");
    
    _kmq.push(['record', 'Bridge Node Page']);  //KISSMETRIC EVENT
    $(".titleNodeLinkpage").click(function(e){
		_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
	overlay();
}

function loadHomePage( )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }
        });
        //show the porlets slowly
        $('#digests').animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
    _kmq.push(['record', 'Viewed Homepage']);
    
    overlay();
}

function loadCollectionPage(){
    var container = $('#digests');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function loadProfilePage(){
    var container = $('#collectionsUser');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function changeSourceMetaImg(page){
	var exp = "";
	var imgHtml = null;
	if(page == "bridgepage"){
		exp="#bridgeNodeContent > div:first > img";
	}else if(page == "linkpage"){
		exp = "#digests > div:first .linkImage";	
	}
	imgHtml = $(exp);
	if(imgHtml.length > 0){
		$("meta[property='og:image']").attr("content",imgHtml.attr("src"));	
	}else{
		$("meta[property='og:image']").attr("content","https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png");	
	}
}

function loadDigestPage(){
	$("#seeOriginalSource").click(function(){
		_kmq.push(['record', 'See Original Source']);  //KISSMETRIC EVENT
	});
	instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item5_browse">Browse</div><div id="item6_save">Save </div> <div id="item7_discuss">Discuss </div> </div><div id="instructional"></div>');
	overlay();
}

function overlay(){
	if(!localStorage.getItem("overlay")){		
		instructional.appendTo(document.body);
		
		$('#instructional').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});

		$('#instructional_2').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});
	}
}