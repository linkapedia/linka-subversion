var instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item3">Browse</div><div id="item4">Explore </div> </div><div id="instructional"></div>');
var _URL_REST = "http://linkapedia-rest-dev.elasticbeanstalk.com";
var _URL_DOCUMENT_IMAGE = "https://s3.amazonaws.com/test_new_image/";
var message_empty = "We are still gathering information for this topic. Click 'join this topic' to be notified when we have new information.";

function load() {
 //menu
    window.api = $("#slider1").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider2").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
    window.api = $("#slider3").scrollable({
        mousewheel: true, 
        size: 2,
        touch:true,
        differentSize:true
    });
}
function loadLinkPage(nodeId)
{
	setInfiniteScroll("linkpage", 16, "#digests", nodeId);
	changeSourceMetaImg("linkpage");
	_kmq.push(['record', 'Link Page']);  //KISSMETRIC EVENT
		$(".titleNodeLinkpage").click(function(e){
			_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
	overlay();
}

function loadBridgeNodePage(nodeId)
{        
	$(".adsenseRightBridgeNode").css("margin-left", $(window).width()-205+"px");
	setInfiniteScroll("bridgenodepage", 16, "#bridgeNodeContent", nodeId);    
    changeSourceMetaImg("bridgepage");
    
    _kmq.push(['record', 'Bridge Node Page']);  //KISSMETRIC EVENT
    $(".titleNodeLinkpage").click(function(e){
		_kmq.push(['record', 'Link Page One Click']);  //KISSMETRIC EVENT
	});
	$(window).resize(function(){
		$(".adsenseRightBridgeNode").css("margin-left", $(this).width()-205+"px");
		$(".body_text").width(parseInt($(".adsenseRightBridgeNode").css("margin-left").replace("px", "")) - 30);
	});	
	overlay();
}

function loadHomePage( )
{
    //masonry
    var container = $('#digests');
    //hide the container of portlets
    $('#digests').css({
        opacity : 0
    });
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }
        });
        //show the porlets slowly
        $('#digests').animate({
            opacity : 1
        }, {
            duration : 2000
        });
    });
    _kmq.push(['record', 'Viewed Homepage']);
    
    overlay();
}

function loadCollectionPage(){
    var container = $('#digests');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function loadProfilePage(){
    var container = $('#collectionsUser');
    container.imagesLoaded(function() {
        container.masonry({
            itemSelector : '.box',
            /*columnWidth : function(containerWidth) {
                return containerWidth / 4;
            }*/
            columnWidth:50
        });
    });
}

function changeSourceMetaImg(page){
	var exp = "";
	var imgHtml = null;
	if(page == "bridgepage"){
		exp="#bridgeNodeContent > div:first > img";
	}else if(page == "linkpage"){
		exp = "#digests > div:first .linkImage";	
	}
	imgHtml = $(exp);
	if(imgHtml.length > 0){
		$("meta[property='og:image']").attr("content",imgHtml.attr("src"));	
	}else{
		$("meta[property='og:image']").attr("content","https://s3.amazonaws.com/resourcesweb/newdev/images/no_image.png");	
	}
}

function loadDigestPage(){
	$("#seeOriginalSource").click(function(){
		_kmq.push(['record', 'See Original Source']);  //KISSMETRIC EVENT
	});
	instructional = jQuery(' <div id="instructional_2"> <div id="item1">Login </div><div id="item2">Topic Search </div><div id="item5_browse">Browse</div><div id="item6_save">Save </div> <div id="item7_discuss">Discuss </div> </div><div id="instructional"></div>');
	overlay();
}

function overlay(){
	if(!localStorage.getItem("overlay")){		
		instructional.appendTo(document.body);
		
		$('#instructional').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});

		$('#instructional_2').click( function() {
			localStorage.setItem("overlay","true");
			$('#instructional_2').hide();
			$('#instructional').fadeOut();
		});
	}
}
