(function($) {
	$.fn.outerHTML = function(s) {
		return (s) 
			? this.before(s).remove() 
			: $('<p>').append(this.eq(0).clone()).html();
	}
})(jQuery);

function encodeFileName(text){
	try{
		text = text.trim();
		text = text.toLowerCase();
		text = text.replace(new RegExp("-", "g"), "");
		text = text.replace(new RegExp("\\s{2,}", "g"), " ");
		text = text.replace(new RegExp(" ", "g"), "-");
		return text;
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function purgeEncoding(text){
	try{
		text = text.replace(new RegExp("[\u0000-\u001f]", "g"), "");
		text = text.replace(new RegExp("[^\\x20-\\x7e]", "g"), "");
		return text;
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}

function cutText(text, limit, endtext){
	try{
		if(text.length < limit){
			return text;			
		}else{
			return text.substring(0, limit) + endtext;
		}
	}catch(err){
		console.log("Error: " + err.message);
		return null;
	}
}
