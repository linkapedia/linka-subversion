package com.linkapedia.builderpage.init;

import com.intellisophic.linkapedia.api.beans.User;
import com.linkapedia.builderpage.bo.BuildPageBo;
import com.linkapedia.builderpage.bo.BuilderPageController;
import com.linkapedia.builderpage.bo.output.WritablePage;
import com.linkapedia.builderpage.init.config.TaxonomyValidator;
import com.linkapedia.builderpage.security.ControlMessageAPI;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Create a page from one node.
 *
 * @author andres
 */
public class PageCreation extends HttpServlet {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(PageCreation.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameters
        log.info("Creating page");

        HttpSession session = request.getSession(true);
        User user = new User();
        if (session.getAttribute("user") != null) {
            user = (User) session.getAttribute("user");
        }
        String nodeId = null;
        String callback = null;
        ControlMessageAPI apiMessage = null;
        try {
            nodeId = request.getParameter("nodeId");
            callback = request.getParameter("callback");
            if (callback == null) {
                callback = "";
            }
            apiMessage = new ControlMessageAPI(response, callback);
            //validate nodeId
            if (nodeId == null || nodeId.isEmpty()) {
                apiMessage.messageMissingParameters();
            } else {
                //validate the taxonomy
                String path = request.getRequestURI().substring(request.getContextPath().length());
                path = path + "?" + request.getQueryString();
                TaxonomyValidator val = new TaxonomyValidator(nodeId, response, path);
                val.validate();
                //start process
                BuildPageBo bp = new BuildPageBo();
                WritablePage writable = new WritablePage(response);
                bp.addObserver(writable);
                boolean isOk;
                BuilderPageController bpv = new BuilderPageController(bp);
                isOk = bpv.createNodePage(nodeId, user);
                if (!isOk) {
                    if (UtilEnviroment.isLocal() || UtilEnviroment.isDev()) {
                        apiMessage.messageErrorPageCreation();
                    } else {
                        response.sendRedirect(UtilEnviroment.getContext() + "/home");
                    }
                }
            }
        } finally {
            if (apiMessage != null) {
                apiMessage.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PageCreation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(PageCreation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Take a nodeId and build the page";
    }// </editor-fold>
}
