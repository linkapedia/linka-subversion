package com.linkapedia.builderpage.init.config;

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBWrapperAbs;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCache;
import com.linkapedia.builderpage.util.ManageResourcesUtils;
import com.linkapedia.builderpage.util.UtilEnviroment;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class ContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //config the cache
        if (UtilEnviroment.asLocal() || UtilEnviroment.isLocal()) {
            ConcreteCache.cacheConfig(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.connection"));
        }
        DynamoDBWrapperAbs.env = UtilEnviroment.getEnv();
        ConcreteCache.env = UtilEnviroment.getEnv();
        log.info("Context Initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //release the cache
        ConcreteCache.shutdown();
        log.info("Context Destroyed");

    }
}
