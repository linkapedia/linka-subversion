package com.linkapedia.builderpage.bo.beans;

/**
 *
 * @author Bancolombia
 */
public class DigestInfoObject {

    private String nodeId = null;
    private String docId = null;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }
}
