/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.builderpage.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author alexander
 */
public class DigestUtil {

     public static String FilterDigestSpaces(String digest) {
        try {
            if (digest != null) {
                Pattern pattern = Pattern.compile("( )*(?:\".*?\")( )*");
                Matcher matcher = pattern.matcher(digest);
                List<String> coincidencias = new ArrayList<String>();
                String textTemp = "";

                while (matcher.find()) {
                    coincidencias.add(matcher.group());
                }
                
                for (String text : coincidencias) {
                    textTemp = " " + text.replaceAll("( )*(?:\\\")( )*", "\"") + " ";
                    digest = digest.replace(text, textTemp);
                }
                
                digest = digest.replaceAll(" \" ", " ");

                digest = digest.replaceAll("( )*\\,( )*", ", ");
                digest = digest.replaceAll("( )*\\.( )*", ". ");
                digest = digest.replaceAll("( )*\\;( )*", "; ");
                digest = digest.replaceAll("( )*\\!( )*", "! ");
                digest = digest.replaceAll("( )*(\\.)( )*(\\\")", ".\"");
                digest = digest.replaceAll("( )*\\:", ":");
                digest = digest.replaceAll(" {2,}", " ");
                digest = digest.replaceAll("( ){1,}(\\')", "'");
                digest = digest.trim();
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return digest;
    }
}
