package com.linkapedia.builderpage.util;

import com.intellisophic.linkapedia.api.beans.DocNodeWrapper;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.handlers.NodeDocumentAPIHandler;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.SentencesGenerator;
import com.linkapedia.builderpage.bo.beans.DebugVersionObject;
import com.linkapedia.builderpage.bo.beans.InfoVersionObject;
import com.linkapedia.builderpage.bo.comparators.SentenceComparator;
import com.linkapedia.core.DocumentSentence;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SentenceUtil {

    private static final Logger log = Logger.getLogger(SentenceUtil.class);

    /**
     * get all sentences. to see debug version
     *
     * @param corpusId
     * @param node
     * @param fileName
     * @return
     */
    public static List<DocumentSentence> getAllSentencesWithScore(NodeStatic node, String fileName) {
        String[] signatures = splitString(node.getSignatures());
        String[] mustHaves = splitString(node.getMusthaves());
        //get InputStream of document
        List<DocumentSentence> list = null;
        //Get sentences for debug
        list = SentencesGenerator.getSentencesWithScores(fileName,
                node, mustHaves, signatures);
        return list;
    }

    /**
     * get info version
     * TODO: bad code please check the data in dynamo
     *
     * @param corpusId
     * @param node
     * @param fileName
     * @return
     */
    public static InfoVersionObject getInfoDocuments(NodeStatic node, String fileName) {
        InfoVersionObject ivo = new InfoVersionObject();
        DocNodeWrapper docnode = DataHandlerFacade.getDoc_NodesByDocumentIdForBuilder(fileName);
        if (docnode == null) {
            docnode = new DocNodeWrapper();
        }
        String infoDocNode = "";        
        try {
            infoDocNode = new String(docnode.getData(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.error("Unssoported encode getting Doc_Node info", ex);
            return ivo;
        }catch(Exception e){
            log.error("work araoud");
        }

        JSONObject data = null;
        String title = "";
        
        try {
            data = (JSONObject) JSONSerializer.toJSON(infoDocNode);
            ivo.setUrl(data.getString("url"));
            
            if(NodeDocumentAPIHandler.getNodeDocument(node.getId(), fileName) != null){
                title = NodeDocumentAPIHandler.getNodeDocument(node.getId(), fileName).getDocTitle();
            }         
            
            ivo.setTitle(title);
        } catch (Exception e) {
            log.error("Error reading json: " + infoDocNode);
            
        }
        JSONArray nodes = null;
        
        try{
            nodes = data.getJSONArray("nodes");
        }catch(Exception ex){
            ex.printStackTrace(System.out);
        }

        List<InfoVersionObject.NodeRelated> relatedNodes = new ArrayList<InfoVersionObject.NodeRelated>();
        if (nodes != null) {
            for (Object nodeId : nodes) {
                InfoVersionObject.NodeRelated nodeR = ivo.new NodeRelated();
                NodeStatic nodeStatic = null;
                try {
                    nodeStatic = DataHandlerFacade.getNode(nodeId.toString());
                } catch (Exception ex) {
                    log.error("Error getting NodeStatic", ex);
                }
                //FIXME: we need a tool to get the relation between oracle and mysql
                //if a node exists in oracle and not exists in mysql then the nodeStatic = null
                if (nodeStatic == null) {
                    log.info("THIS IS A WORK AROUND, we need to organize the oracle and mysql relation");
                    continue;
                }

                //show related nodes in the correct environment
                if (UtilEnviroment.isWine()) {
                    if (!nodeStatic.getTaxonomyID().equals("2000001")) {
                        continue;
                    }
                }
                if (UtilEnviroment.isBuddhism()) {
                    if (!nodeStatic.getTaxonomyID().equals("3000001")) {
                        continue;
                    }
                }
                nodeR.setId(nodeStatic.getId());
                nodeR.setTitle(nodeStatic.getTitle());
                nodeR.setEncodeTitle(SEOUtils.encodeFileName(nodeStatic.getTitle()));
                relatedNodes.add(nodeR);

            }
        }
        ivo.setNodesRelated(relatedNodes);

        //calculate digest
        List<DocumentSentence> list = getAllSentencesWithScore(node, fileName);

        String digest = "";
        if (list != null && !list.isEmpty()) {
            SentenceComparator con = new SentenceComparator();
            Collections.sort(list, con);
            digest = SentencesGenerator.getDigest(list, 3, 2);
            //must have not found in the sentences
            if (digest == null || digest.isEmpty()) {
                //select the sentence with the bigger score > 0
                if (list.get(0).getScoreByMustHaves()
                        + list.get(0).getScoreByNodeTitle()
                        + list.get(0).getScoreBySignatures() > 0) {
                    digest = list.get(0).getSentence();
                }
            }
        }
        //digest = StringEscapeUtils.escapeHtml4(digest);
        ivo.setDocDigest(digest);
        return ivo;
    }

    private static String[] splitString(String str, String value) {
        if (str == null) {
            return new String[0];
        }
        return str.split(value);
    }

    private static String[] splitString(String str) {
        if (str == null) {
            return null;
        }
        return splitString(str, "\\|\\|");
    }
}
