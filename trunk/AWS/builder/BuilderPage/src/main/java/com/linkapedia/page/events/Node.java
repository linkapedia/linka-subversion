package com.linkapedia.page.events;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.api.beans.NodeUser;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.beans.UserNode;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.Date;

/**
 *
 * @author juan
 */
public class Node {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(Node.class);

    /**
     *
     * @param user
     * @param nodeId
     * @return
     * @throws Exception
     */
    public static boolean join(User user, String nodeId) throws Exception {
        NodeStatic nodeStatic = null;
        UserNode userNode = new UserNode();
        NodeUser nodeUser = new NodeUser();
        Date dateTime = new Date();
        long timeStamp = dateTime.getTime();
        try {
            nodeStatic = DataHandlerFacade.getNode(nodeId);
            if (nodeStatic != null) {

                userNode.setUserId(user.getUserId());
                userNode.setNodeId(nodeStatic.getId());
                userNode.setNodeTitle(nodeStatic.getTitle());
                userNode.setTimeStamp(timeStamp);

                nodeUser.setNodeId(nodeId);
                nodeUser.setUserId(user.getUserId());
                nodeUser.setUserFbId(user.getFacebookCredentials().getProperty("fbId"));
                nodeUser.setUserName(user.getFirstName());
                nodeUser.setUserLastName(user.getLastName());
                nodeUser.setTimeStamp(timeStamp);

                //FIXME: we need this process in a transaction
                DataHandlerFacade.saveUserNode(userNode);
                DataHandlerFacade.saveNodeUser(nodeUser);

            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception in joining node : " + ex.getMessage());
            return false;
        }

        return true;

    }

    /**
     * set all parameters in the objects to save in events table FIXME: search a
     * better logic to manage the events unjoin node
     *
     * @param user
     * @param nodeId
     * @return
     * @throws Exception
     */
    public static boolean unjoin(User user, String nodeId) throws Exception {
        NodeStatic nodeStatic = null;
        UserNode userNode = new UserNode();
        NodeUser nodeUser = new NodeUser();
        Date dateTime = new Date();
        long timeStamp = dateTime.getTime();
        try {
            nodeStatic = DataHandlerFacade.getNode(nodeId);
            if (nodeStatic != null) {
                userNode.setUserId(user.getUserId());
                userNode.setNodeId(nodeStatic.getId());
                userNode.setTimeStamp(timeStamp);
                userNode.setNodeTitle(nodeStatic.getTitle());

                nodeUser.setNodeId(nodeId);
                nodeUser.setUserId(user.getUserId());
                nodeUser.setTimeStamp(timeStamp);

                //FIXME: we need this process in a transaction
                DataHandlerFacade.deleteUserNode(userNode);
                DataHandlerFacade.deleteNodeUser(nodeUser);
            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception in unjoining node : " + ex.getMessage());
            return false;
        }
        return true;
    }
}
