package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeLink;
import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.bo.beans.NodeDataObject;
import com.linkapedia.builderpage.util.TreeMenuUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Create a structure tree from nodelink table BETA 1.0
 *
 * @author juan
 * @author andres
 */
public class LinkNodeTreeMenuGenerator {

    private static final Logger log = Logger.getLogger(TreeMenuUtils.class);
    private NodeStatic node;
    //levels needed for this tree
    public static final String TREE_LEVEL0 = "level0";
    public static final String TREE_LEVEL1 = "level1";
    public static final String TREE_LEVEL2 = "level2";

    /**
     * constructor to manage one node
     *
     * @param node
     */
    public LinkNodeTreeMenuGenerator(NodeStatic node) {
        this.node = node;
    }

    /**
     * get the select level
     *
     * @param level
     * @return
     */
    public LinkedList<NodeDataObject> generateLevel(String level) {
        log.info("Get " + level);
        if (level.equals(TREE_LEVEL1)) {
            return generateLevel1();
        } else if (level.equals(TREE_LEVEL2)) {
            return generateLevel2();
        } else if (level.equals(TREE_LEVEL0)) {
            return generateLevel0();
        } else {
            log.error("The level is not found");
            return null;
        }
    }

    /**
     * build the level1
     *
     * @return
     */
    private LinkedList<NodeDataObject> generateLevel1() {
        LinkedList<NodeDataObject> level1 = null;
        List<NodeDataObject> uncles = null;

        //look if is the first level of knomore
        if (!TreeMenuUtils.isTaxonomyRootNode(node)) {
            NodeStatic parent = null;
            uncles = new ArrayList<NodeDataObject>();

            //get the parents
            if (TreeMenuUtils.isRootNode(node)) {
                //search the uncles in nodelink
                uncles = consultParentSiblingsNodeLinks(node.getId());
            } else {
                //search the parent in nodestatic
                try {
                    parent = DataHandlerFacade.getNode(node.getParentID());
                    if (TreeMenuUtils.isRootNode(parent)) {
                        //search uncles in nodelink table
                        uncles = consultParentChildrenNodeLinks(parent.getId());
                    } else {
                        uncles = TreeMenuUtils.getSiblings(parent, parent.getId());
                    }

                } catch (Exception ex) {
                    log.error("Error getting nodestatic: " + parent.getParentID(), ex);
                }
            }
        }
        level1 = new LinkedList<NodeDataObject>();
        if (uncles == null || uncles.isEmpty()) {
            return null;
        } else {
            level1.addAll(uncles);
            return level1;
        }
    }

    /**
     * build the level2
     *
     * @return
     */
    private LinkedList<NodeDataObject> generateLevel2() {
        LinkedList<NodeDataObject> level2 = null;
        //get the nodelinks in NodeLink Table
        List<NodeLink> nodeLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(node.getId());
        //If the node has nodeLink in NodeLink Table
        if (nodeLinks != null && !nodeLinks.isEmpty()) {
            NodeStatic parent = null;
            List<NodeDataObject> parentChildrens = new ArrayList<NodeDataObject>();
            NodeDataObject nodeData = null;
            for (NodeLink n : nodeLinks) {
                try {
                    parent = DataHandlerFacade.getNode(n.getNodeParentStaticId());
                    if (parent == null) {
                        log.info("Error getting nodestatic, return null ");
                        continue;
                    }
                    nodeData = new NodeDataObject(node.getId(), node.getTitle());
                    parentChildrens.addAll(TreeMenuUtils.getChildren(parent, node.getId()));
                } catch (Exception e) {
                    log.error("Error getting nodestatic", e);
                }
            }
            level2 = new LinkedList<NodeDataObject>();
            //add All siblings
            level2.addAll(parentChildrens);
        }
        return level2;
    }

    /**
     * build the level0
     *
     * @return
     */
    private LinkedList<NodeDataObject> generateLevel0() {
        LinkedList<NodeDataObject> level0 = null;
        List<NodeDataObject> siblimgsGrandParent = null;

        //look if is the first level of knomore
        if (!TreeMenuUtils.isTaxonomyRootNode(node)) {
            NodeStatic parent = null;
            NodeStatic grandParent = null;
            siblimgsGrandParent = new ArrayList<NodeDataObject>();

            //get the parents
            if (TreeMenuUtils.isRootNode(node)) {
                //search the siblimgsGrandParent in nodelink
                siblimgsGrandParent = consultGranParentSiblingsNodeLinks(node.getId());
            } else {
                //search the parent in nodestatic
                try {
                    parent = DataHandlerFacade.getNode(node.getParentID());
                } catch (Exception ex) {
                    log.error("Error getting nodestatic: " + node.getParentID(), ex);
                    return null;
                }
                if (TreeMenuUtils.isRootNode(parent)) {
                    siblimgsGrandParent = consultParentSiblingsNodeLinks(parent.getId());
                } else {
                    if (TreeMenuUtils.isTaxonomyRootNode(parent)) {
                        siblimgsGrandParent = TreeMenuUtils.getSiblings(parent, parent.getId());
                    } else {
                        try {
                            grandParent = DataHandlerFacade.getNode(parent.getParentID());
                        } catch (Exception ex) {
                            log.error("Error getting nodestatic: " + parent.getParentID(), ex);
                            return null;
                        }
                        if (TreeMenuUtils.isRootNode(grandParent)) {
                            //search siblimgsGrandParent in nodelink table
                            siblimgsGrandParent = consultParentChildrenNodeLinks(grandParent.getId());
                        } else {
                            siblimgsGrandParent = TreeMenuUtils.getSiblings(grandParent, grandParent.getId());
                        }
                    }

                }
            }
        }
        level0 = new LinkedList<NodeDataObject>();
        if (siblimgsGrandParent == null || siblimgsGrandParent.isEmpty()) {
            return null;
        } else {
            level0.addAll(siblimgsGrandParent);
            return level0;
        }
    }

    /**
     * get parents from one nodeid
     *
     * @param id
     * @return
     */
    public static LinkedList<NodeDataObject> getParentNodeLinkToMenu(String id) {
        List<NodeLink> parentLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(id);
        LinkedList<NodeDataObject> nodes = new LinkedList<NodeDataObject>();
        NodeStatic parentStatic = null;
        NodeDataObject aux = null;
        for (NodeLink nodeLink : parentLinks) {
            try {
                parentStatic = DataHandlerFacade.getNode(nodeLink.getNodeParentStaticId());
                aux = new NodeDataObject(String.valueOf(parentStatic.getId()), parentStatic.getTitle());
                nodes.add(aux);
            } catch (Exception ex) {
                log.error("Error getting nodestatic: " + nodeLink.getNodeParentStaticId(), ex);
            }
        }
        return nodes;
    }

    /**
     * get parents from one nodeid
     *
     * @param id
     * @return
     */
    public static LinkedList<NodeDataObject> getGrandParentNodeLinkToMenu(String id) {
        List<NodeLink> parentLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(id);
        LinkedList<NodeDataObject> nodes = new LinkedList<NodeDataObject>();
        NodeStatic parentStatic = null;
        NodeDataObject aux = null;
        NodeStatic grandParentStatic = null;
        for (NodeLink nodeLink : parentLinks) {
            try {
                parentStatic = DataHandlerFacade.getNode(nodeLink.getNodeParentStaticId());
                if (!TreeMenuUtils.isTaxonomyRootNode(parentStatic)) {
                    try {
                        grandParentStatic = parentStatic = DataHandlerFacade.getNode(parentStatic.getParentID());
                        aux = new NodeDataObject(String.valueOf(grandParentStatic.getId()), grandParentStatic.getTitle());
                        nodes.add(aux);
                    } catch (Exception ex) {
                        log.error("Error getting nodestatic: " + parentStatic.getParentID(), ex);
                    }
                }

            } catch (Exception ex) {
                log.error("Error getting nodestatic: " + nodeLink.getNodeParentStaticId(), ex);
            }
        }
        return nodes;
    }

    /**
     * consult children from my grandparent in nodestatic
     *
     * @param id
     * @return
     */
    private List<NodeDataObject> consultParentChildrenNodeLinks(String id) {
        List<NodeLink> parentLinks = null;
        List<NodeDataObject> children = new ArrayList<NodeDataObject>();
        parentLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(id);
        if (parentLinks != null) {
            NodeStatic parentStatic = null;
            for (NodeLink nodeLink : parentLinks) {
                try {
                    parentStatic = DataHandlerFacade.getNode(nodeLink.getNodeParentStaticId());
                    //add the uncles
                    children.addAll(TreeMenuUtils.getChildren(parentStatic, id));

                } catch (Exception ex) {
                    log.error("Error getting nodestatic: " + nodeLink.getNodeParentStaticId(), ex);
                }
            }
        }
        return children;
    }

    /**
     * consult siblings from my parent in nodestatic
     *
     * @param id
     * @return
     */
    private List<NodeDataObject> consultParentSiblingsNodeLinks(String id) {
        List<NodeLink> parentLinks = null;
        List<NodeDataObject> sibling = new ArrayList<NodeDataObject>();
        parentLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(id);
        if (parentLinks != null) {
            NodeStatic parentStatic = null;
            for (NodeLink nodeLink : parentLinks) {
                try {
                    parentStatic = DataHandlerFacade.getNode(nodeLink.getNodeParentStaticId());
                    //add the uncles
                    sibling.addAll(TreeMenuUtils.getSiblings(parentStatic, id));

                } catch (Exception ex) {
                    log.error("Error getting nodestatic: " + nodeLink.getNodeParentStaticId(), ex);
                }
            }
        }
        return sibling;
    }

    /**
     * consult siblings from my parent in nodestatic
     *
     * @param id
     * @return
     */
    private List<NodeDataObject> consultGranParentSiblingsNodeLinks(String id) {
        List<NodeLink> parentLinks = null;
        List<NodeDataObject> siblingParents = new ArrayList<NodeDataObject>();
        parentLinks = DataHandlerFacade.getNodeLinksByNodeStaticLinkId(id);
        if (parentLinks != null) {
            NodeStatic parentStatic = null;
            NodeStatic grandParentStatic = null;
            for (NodeLink nodeLink : parentLinks) {
                try {
                    parentStatic = DataHandlerFacade.getNode(nodeLink.getNodeParentStaticId());
                } catch (Exception ex) {
                    log.error("Error getting nodestatic: " + nodeLink.getNodeParentStaticId(), ex);
                    continue;
                }
                if (!TreeMenuUtils.isTaxonomyRootNode(parentStatic)) {
                    try {
                        grandParentStatic = parentStatic = DataHandlerFacade.getNode(parentStatic.getParentID());
                        //add the siblingParents
                        siblingParents.addAll(TreeMenuUtils.getSiblings(grandParentStatic, id));
                    } catch (Exception ex) {
                        log.error("Error getting nodestatic: " + parentStatic.getParentID(), ex);
                    }
                }

            }
        }
        return siblingParents;
    }
}
