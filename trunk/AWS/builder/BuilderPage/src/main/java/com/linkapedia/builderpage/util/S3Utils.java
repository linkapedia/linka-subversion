package com.linkapedia.builderpage.util;

import com.intellisophic.linkapedia.amazon.services.S3.bo.ManageBucket;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class S3Utils {

    private static final Logger LOG = Logger.getLogger(S3Utils.class);

    /**
     * get string of file in amazon
     *
     * @param bucketName
     * @param key
     * @return string of file
     */
    public static String getStringFile(String bucketName, String key) {
        InputStream is = getInputStreamFile(bucketName, key);
        String data = readInputStream(is);
        return data;
    }

    /**
     * get inputstream of file in amazon
     *
     * @param bucketName
     * @param key
     * @return inputstream of file
     */
    public static InputStream getInputStreamFile(String bucketName, String key) {
        InputStream is = null;
        try {
            ManageBucket mb = new ManageBucket(bucketName);
            is = mb.getData(key);
            return is;
        } catch (Exception ex) {
            LOG.error("Exception reading inputstream", ex);
            return null;
        }
    }

    /**
     * get bufferedimage of file in amazon
     *
     * @param bucketName
     * @param key
     * @return bufferedimage of file
     */
    public static BufferedImage getBufferedImageFile(String bucketName, String key) {
        try {
            BufferedImage image = ImageIO.read(getInputStreamFile(bucketName, key));
            return image;
        } catch (IOException ex) {
            LOG.error("Error reading inputstraem", ex);
            return null;
        }
    }

    /**
     * read inputstream for convert in a string
     *
     * @param is
     * @return string version of inputstream
     */
    private static String readInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sentences = new StringBuilder();
        try {
            br = new BufferedReader(new InputStreamReader(is, "utf-8"));
            String line = "";
            //add sentences into array
            while ((line = br.readLine()) != null) {
                sentences.append(line).append(" ");
            }
            return sentences.toString();
        } catch (UnsupportedEncodingException ue) {
            LOG.error("Error. The encode is not supported", ue);
            return "";
        } catch (IOException ioe) {
            LOG.error("Error. IOException", ioe);
            return "";
        } finally {
            try {
                if (null != br) {
                    br.close();
                }
            } catch (IOException e) {
                LOG.error("Error closing resources", e);
            }
        }
    }
}
