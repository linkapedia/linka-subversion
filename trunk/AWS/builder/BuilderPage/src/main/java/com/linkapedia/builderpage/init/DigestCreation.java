package com.linkapedia.builderpage.init;

import com.intellisophic.linkapedia.api.beans.User;
import com.linkapedia.builderpage.bo.BuildPageBo;
import com.linkapedia.builderpage.bo.BuilderPageController;
import com.linkapedia.builderpage.bo.beans.DigestInfoObject;
import com.linkapedia.builderpage.bo.output.WritablePage;
import com.linkapedia.builderpage.init.config.TaxonomyValidator;
import com.linkapedia.builderpage.security.ControlMessageAPI;
import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author andres c
 */
public class DigestCreation extends HttpServlet {

    private static final Logger log = Logger.getLogger(DigestCreation.class);

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Creating digest page");
        HttpSession session = request.getSession(true);
        User user = new User();
        if (session.getAttribute("user") != null) {
            user = (User) session.getAttribute("user");
        }
        String nodeId = null;
        String docId = null;

        ControlMessageAPI apiMessage = new ControlMessageAPI(response, "");

        try {

            //read json data
            nodeId = request.getParameter("nodeId");
            docId = request.getParameter("docId");
            DigestInfoObject digestInfo = new DigestInfoObject();
            digestInfo.setNodeId(nodeId);
            digestInfo.setDocId(docId);


            //validate nodeId
            if (nodeId == null) {
                apiMessage.messageMissingParameters();
            } else {
                //validate the taxonomy
                String path = request.getRequestURI().substring(request.getContextPath().length());
                path = path + "?" + request.getQueryString();
                TaxonomyValidator val = new TaxonomyValidator(nodeId, response, path);
                val.validate();
                BuildPageBo bp = new BuildPageBo();
                WritablePage writable = new WritablePage(response);
                bp.addObserver(writable);
                boolean isOk;
                BuilderPageController bpc = new BuilderPageController(bp);
                isOk = bpc.createDigestPage(digestInfo, user);
                if (!isOk) {
                    if (UtilEnviroment.isLocal() || UtilEnviroment.isDev()) {
                        apiMessage.messageErrorPageCreation();
                    } else {
                        response.sendRedirect(UtilEnviroment.getContext() + "/home");
                    }
                }

            }

        } finally {
            if (apiMessage != null) {
                apiMessage.close();
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
