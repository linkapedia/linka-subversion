package com.linkapedia.builderpage.util;

import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class UtilEnviroment {

    private static final Logger log = Logger.getLogger(UtilEnviroment.class);
    private static String env;
    private static String facebookId;
    private static String kissmetricId;
    private static String disqusKey;
    private static String disqusSecret;
    private static String ROOTID_ENV_DEV = "31947981";
    private static String ROOTID_ENV_BUDDHISM = "56425581";
    private static String ROOTID_ENV_WINE = "39904581";
    private static String ROOTID_ENV_KNOMORE = "31947981";
    private static String ROOTID_ENV_DEFAULT = "27169481";
    private static String DEFAULT_ENV = "newdev";
    private static Boolean asLocal;
    private static List<String> ENVIROMENTS = Arrays.asList("dev", "buddhism", "wine", "knomore", "newdev", "local");

    static {
        initialize();
    }

    /**
     * get the resources in the first instance read the properties from file
     */
    private static void initialize() {
        String environment = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.environment");
        if (environment == null || environment.isEmpty()) {
            log.info("Environment not found. set default: \"newdev\"");
            environment = DEFAULT_ENV;
        }
        if (!ENVIROMENTS.contains(environment)) {
            log.info("Environment not found. set default: \"newdev\"");
            environment = DEFAULT_ENV;
        }

        //set the current environment
        UtilEnviroment.env = environment;

        //facebook
        UtilEnviroment.facebookId = null;
        UtilEnviroment.facebookId = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facebook." + getEnv(false) + ".client_id");
        //kissmetrics
        UtilEnviroment.kissmetricId = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.util.kissmetrics." + getEnv() + ".token");

        //DISQUS
        UtilEnviroment.disqusKey = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.util.disqus." + getEnv() + ".key");
        UtilEnviroment.disqusSecret = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.util.disqus." + getEnv() + ".secret");

        UtilEnviroment.asLocal = Boolean.parseBoolean(ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.environment.asLocal", "false"));


    }

    public static boolean asLocal() {
        return UtilEnviroment.asLocal;
    }

    public static String getKissmetricId() {
        return kissmetricId;
    }

    public static String getDisqusKey() {
        return disqusKey;
    }

    public static String getDisqusSecret() {
        return disqusSecret;
    }

    /**
     * default get enviroment
     *
     * @return
     */
    public static String getEnv() {
        return getEnv(true);
    }

    /**
     * get current enviroment
     *
     * @param localAsDev get local how newdev to get the files from s3
     * @return
     */
    private synchronized static String getEnv(boolean localAsDev) {
        if (UtilEnviroment.env == null) {
            initialize();
        }
        if (localAsDev) {
            //look if is local and point to newdev
            if (ENVIROMENTS.get(5).equals(UtilEnviroment.env)) {
                //return newdev enviroment
                return ENVIROMENTS.get(4);
            }
        }
        return UtilEnviroment.env;
    }

    /**
     * if the current environment is localhost
     *
     * @return
     */
    public static boolean isLocal() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(5))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isDev() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(4))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isKnomor() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(3))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isWine() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(2))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isBuddhism() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(1))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get the specific nodeid by server
     *
     * @return
     */
    public static String getRootIdByEnv() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(0))) {
            return ROOTID_ENV_DEV;
        } else if (UtilEnviroment.env.equals(ENVIROMENTS.get(1))) {
            return ROOTID_ENV_BUDDHISM;
        } else if (UtilEnviroment.env.equals(ENVIROMENTS.get(2))) {
            return ROOTID_ENV_WINE;
        } else if (UtilEnviroment.env.equals(ENVIROMENTS.get(3))) {
            return ROOTID_ENV_KNOMORE;
        } else if (UtilEnviroment.env.equals(ENVIROMENTS.get(4))) {
            return ROOTID_ENV_DEV;
        } else if (UtilEnviroment.env.equals(ENVIROMENTS.get(5))) {
            return ROOTID_ENV_KNOMORE;
        } else {
            return ROOTID_ENV_DEFAULT;
        }
    }

    /**
     * authenticate with the server
     *
     * @param user
     * @param password
     * @return
     */
    public static boolean auth(String user, String password) {
        UtilEnviroment.env = getEnv(false);
        String myUser;
        String myPassword;

        myUser = (ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.server." + getEnv() + ".user") == null) ? "" : ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.server." + getEnv() + ".user");
        myPassword = (ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.server." + getEnv() + ".password") == null) ? "" : ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.server." + getEnv() + ".password");

        //validate
        if (user.equalsIgnoreCase(myUser) && password.equalsIgnoreCase(myPassword)) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * get current context this is to test locally
     *
     * @return
     */
    public static String getContext() {
        UtilEnviroment.env = getEnv(false);
        if (UtilEnviroment.env.equals(ENVIROMENTS.get(5)) || UtilEnviroment.asLocal) {
            return "/BuilderPage"; //queme: this is temporal.
        }
        return "";
    }

    /**
     * get correct name to show in the templates
     *
     * @return
     */
    public static String getNameCalidoso() {
        return ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.environment." + getEnv() + ".name.calidoso");
    }

    /**
     * get facebook id by server
     *
     * @return
     */
    public static String getFaceBookClientId() {
        UtilEnviroment.env = getEnv(false);
        return UtilEnviroment.facebookId;
    }
}
