package com.linkapedia.builderpage.init.config;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.linkapedia.builderpage.util.UtilEnviroment;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * esta clase tiene quemes para los ids de las taxonomias y las urls
 * solo se valida para wine y para buddhism
 * @author andres
 */
public class TaxonomyValidator {

    private String nodeId;
    private HttpServletResponse res;
    private String path;
    private static final Logger log = Logger.getLogger(TaxonomyValidator.class);

    public TaxonomyValidator(String nodeId, final HttpServletResponse res, String path) {
        this.nodeId = nodeId;
        this.res = res;
        this.path = path;
    }

    public void validate() {
        NodeStatic nodeStatic = null;
        try {
            nodeStatic = DataHandlerFacade.getNode(nodeId);
        } catch (Exception e) {
            log.error("Error getting NodeStatic", e);
            return;
        }
        if (nodeStatic != null) {
            //validate the taxonomy
            String taxonomyId = nodeStatic.getTaxonomyID();
            if (UtilEnviroment.isBuddhism()) {
                if (taxonomyId.equals("2000001")) {
                    //redirect to wine
                    this.res.setStatus(301);
                    this.res.setHeader("Location", "http://www.whyknows.com"+path);
                    this.res.setHeader("Connection", "close");
                } else if (!taxonomyId.equals("3000001")) {
                    //redirect to interestplace
                    this.res.setStatus(301);
                    this.res.setHeader("Location", "http://www.interestplace.com"+path);
                    this.res.setHeader("Connection", "close");
                }
            }
            if (UtilEnviroment.isWine()) {
                if (taxonomyId.equals("3000001")) {
                    //redirect to buddhism
                    this.res.setStatus(301);
                    this.res.setHeader("Location", "http://www.buddhismplace.com"+path);
                    this.res.setHeader("Connection", "close");
                } else if (!taxonomyId.equals("2000001")) {
                    //redirect to interestplace
                    this.res.setStatus(301);
                    this.res.setHeader("Location", "http://www.interestplace.com"+path);
                    this.res.setHeader("Connection", "close");
                }
            }
        }
    }
}
