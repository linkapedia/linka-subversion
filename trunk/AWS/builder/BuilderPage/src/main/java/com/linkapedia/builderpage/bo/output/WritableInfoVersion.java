package com.linkapedia.builderpage.bo.output;

import com.linkapedia.builderpage.bo.beans.InfoVersionObject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class WritableInfoVersion implements IWritable<InfoVersionObject> {

    private static final Logger LOG = Logger.getLogger(WritableInfoVersion.class);
    private HttpServletResponse res;
    private PrintWriter pw;
    private String callback;

    /**
     *
     * @param res
     * @param callback
     * @throws IOException
     */
    public WritableInfoVersion(final HttpServletResponse res, String callback) throws IOException {
        this.res = res;
        this.res.setContentType("application/json;charset=UTF-8");
        this.pw = this.res.getWriter();
        this.callback = callback;
    }

    public boolean write(InfoVersionObject o) {
        if (o == null) {
            LOG.error("printJson list = null.");
            return false;
        }
        JSONObject jObj = new JSONObject();
        //jObj.put("docDigest", StringEscapeUtils.escapeHtml4(o.getDocDigest()));
        jObj.put("docDigest", o.getDocDigest());
        JSONArray jArr = new JSONArray();
        JSONObject jObjNodes;
        for (InfoVersionObject.NodeRelated nr : o.getNodesRelated()) {
            jObjNodes = new JSONObject();
            jObjNodes.put("id", nr.getId());
            jObjNodes.put("title", nr.getTitle());
            jObjNodes.put("weight", nr.getWeight());
            jArr.add(jObjNodes);
        }
        jObj.put("nodes", jArr);
        pw.println(callback + "(" + jObj.toString() + ");");
        return true;
    }
}
