package com.linkapedia.builderpage.bo;

import com.intellisophic.linkapedia.api.beans.NodeStatic;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import com.linkapedia.builderpage.bo.temp.Porter;
import com.linkapedia.builderpage.util.RegexUtil;
import com.linkapedia.builderpage.util.S3Utils;
import com.linkapedia.builderpage.util.DigestUtil;
import com.linkapedia.core.DocumentSentence;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
public class SentencesGenerator {

    private static final String BUCKET_NAME = "documentextracted";
    private static final int POINT_BY_NODE_TITLE = 50;
    private static final int POINT_BY_MUSTHAVES = 30;
    private static final int POINT_BY_SIGNATURES = 10;
    //Number the sentences for get
    private static final int DEFAULT_NUM_SENTENCES_TO_GET = 2;
    //if we found a good sentence then number the next sentences to get
    private static final int DEFAULT_COUNT_NEXT_SENTENCES = 2;
    private static final Logger LOG = Logger.getLogger(SentencesGenerator.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(SentencesGenerator.class.getName());
    private static final int TWO_WEEKS = 1296000;

    /**
     * get all sentences with the scores
     *
     * @param is
     * @param nodeTitle
     * @param musthaves
     * @param signatures
     * @return list of summary objects {sentence, scoreByNodeTitle,
     * scoreByMustHaves, scoreBySignatures}
     */
    public static List<DocumentSentence> getSentencesWithScores(String fileName, NodeStatic node,
            String[] musthaves, String[] signatures) {
        LOG.info("Get sentences score for debug mode");
        List<String> sentences = new ArrayList<String>();
        List<DocumentSentence> listToReturn = null;
        listToReturn = (List<DocumentSentence>) cache.get(node.getId() + "-" + fileName);
        if (listToReturn != null) {
            return listToReturn;
        } else {
            listToReturn = new ArrayList<DocumentSentence>();
            String nodeTitle = node.getTitle();
            InputStream is = S3Utils.getInputStreamFile(BUCKET_NAME, fileName);
            if (is == null) {
                LOG.debug("Error getting inputstream for the document");
                return null;
            }

            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(is, "utf-8"));
                String line = "";
                //add sentences into array
                while ((line = br.readLine()) != null) {
                    String normalized_string = line.replaceAll("[\u0000-\u001f]", "");//remove strange characters (unicode)
                    normalized_string = normalized_string.replaceAll("[^\\x20-\\x7e]", "");//remove strange characters ()
                    sentences.add(normalized_string);
                }
            } catch (UnsupportedEncodingException ue) {
                LOG.error("Error. The encode is not supported", ue);
            } catch (IOException ioe) {
                LOG.error("Error. IOException", ioe);
            } finally {
                try {
                    if (null != br) {
                        br.close();
                    }
                } catch (IOException e) {
                    LOG.error("Error closing resources", e);
                }
            }

            //build the score
            int numNodeTitle = 0;
            int numMusthaves = 0;
            int numSignatures = 0;
            DocumentSentence summaryObject = null;
            Pattern p = null;
            Matcher ma = null;
            for (String sentence : sentences) {
                numNodeTitle = 0;
                numMusthaves = 0;
                numSignatures = 0;
                //node title score
                if (nodeTitle != null) {
                    p = Pattern.compile(nodeTitle, Pattern.CASE_INSENSITIVE);
                    ma = p.matcher(sentence);
                    //node title score
                    while (ma.find()) {
                        numNodeTitle++;
                    }
                }
                //must haves score
                if (musthaves != null) {
                    for (int i = 0; i < musthaves.length; i++) {
                        //if (!musthaves[i].equalsIgnoreCase(nodeTitle)) {
                        String mustHaveAux = musthaves[i];
                        mustHaveAux = RegexUtil.unAccent(mustHaveAux);
                        mustHaveAux = RegexUtil.getRegexForSentences(mustHaveAux);
                        p = Pattern.compile(mustHaveAux, Pattern.CASE_INSENSITIVE);
                        ma = p.matcher(RegexUtil.unAccent(sentence));
                        while (ma.find()) {
                            numMusthaves++;
                        }
                        //search by stem
                        Porter porter = null;
                        String stem = "";
                        try {
                            porter = Porter.getInstance();
                            stem = porter.stem(mustHaveAux);
                        } catch (Exception e) {
                            LOG.error("Error getting class porter", e);
                        }
                        if (porter != null) {
                            mustHaveAux = RegexUtil.unAccent(stem);
                            mustHaveAux = RegexUtil.getRegexForSentences(mustHaveAux);
                            p = Pattern.compile(mustHaveAux, Pattern.CASE_INSENSITIVE);
                            ma = p.matcher(RegexUtil.unAccent(sentence));
                            while (ma.find()) {
                                numMusthaves++;
                            }
                        }


                    }
                }
                //signatures score
                if (signatures != null) {
                    for (int i = 0; i < signatures.length; i++) {
                        if (!signatures[i].equalsIgnoreCase(nodeTitle)) {
                            p = Pattern.compile(signatures[i], Pattern.CASE_INSENSITIVE);
                            ma = p.matcher(sentence);
                            while (ma.find()) {
                                numSignatures++;
                            }
                        }
                    }
                }

                //calculate score
                summaryObject = new DocumentSentence();
                summaryObject.setSentence(sentence);
                summaryObject.setScoreByNodeTitle(numNodeTitle * POINT_BY_NODE_TITLE);
                summaryObject.setScoreByMustHaves(numMusthaves * POINT_BY_MUSTHAVES);
                summaryObject.setScoreBySignatures(numSignatures * POINT_BY_SIGNATURES);

                if (musthaves != null) {
                    summaryObject.setMustHaves(org.apache.commons.lang.StringUtils.join(musthaves, ","));
                }
                if (signatures != null) {
                    summaryObject.setSignatures(org.apache.commons.lang.StringUtils.join(signatures, ","));
                }
                listToReturn.add(summaryObject);
            }

            cache.set(node.getId() + "-" + fileName, listToReturn, TWO_WEEKS);
            return listToReturn;
        }
    }

    /**
     * the the digest for see in the web page (generic)
     *
     * @param list
     * @return String with the digest for see in the web page
     */
    public static String getDigest(List<DocumentSentence> list) {
        return getDigest(list, DEFAULT_NUM_SENTENCES_TO_GET, DEFAULT_COUNT_NEXT_SENTENCES);
    }

    /**
     * the the digest for see in the web page
     *
     * @param list
     * @param numSentencesToGet
     * @param countNextSentences
     * @return String with the digest for see in the web page
     */
    public static String getDigest(List<DocumentSentence> list, int numSentencesToGet, int countNextSentences) {
        int i = 0;
        int contSentences = 0;

        StringBuilder sb = new StringBuilder("");
        while (i < list.size()) {
            if (contSentences >= numSentencesToGet) {
                break;
            }
            if (list.get(i).getScoreByMustHaves() >= POINT_BY_MUSTHAVES) {
                //get next sentences
                for (int k = 0; k < countNextSentences; k++) {
                    if (i + k < list.size()) {
                        sb.append(list.get(i + k).getSentence());
                    } else {
                        break;
                    }
                }
                sb.append("<br><br>");
                i += countNextSentences;
                contSentences++;
            } else {
                i++;
            }
        }

        return DigestUtil.FilterDigestSpaces(sb.toString());
    }
}
