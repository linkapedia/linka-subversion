package com.linkapedia.builderpage.bo.temp;

/**
 *
 * @author andres
 */
public class LinkInfoTest {

    private String title;
    private String summary;

    public LinkInfoTest(String title, String summary) {
        this.title = title;
        this.summary = summary;

    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
