package com.linkapedia.page.events;

import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.api.beans.FollowingUser;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import java.util.Date;

/**
 *
 * @author juan
 */
public class User {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(User.class);

    /**
     *
     * @param user
     * @param userId
     * @return
     * @throws Exception
     */
    public static boolean follow(com.intellisophic.linkapedia.api.beans.User user, String userId) throws Exception {
        com.intellisophic.linkapedia.api.beans.User userToFollow = null;
        FollowingUser followingUser = new FollowingUser();
        FollowedUser followedUser = new FollowedUser();
        Date dateTime = new Date();
        long timeStamp = dateTime.getTime();
        try {
            userToFollow = DataHandlerFacade.getUser(userId);
            if (userToFollow != null) {
                followingUser.setFollowingUserId(user.getUserId());
                followingUser.setFollowedUserId(userToFollow.getUserId());
                followingUser.setFollowedUserLastName(userToFollow.getLastName());
                followingUser.setFollowedUserName(userToFollow.getFirstName());
                followingUser.setFollowedUserFbId(userToFollow.getFacebookCredentials().getProperty("fbId"));
                followingUser.setTimeStamp(timeStamp);

                followedUser.setFollowedUserId(userToFollow.getUserId());
                followedUser.setFollowingUserId(user.getUserId());
                followedUser.setFollowingUserFbId(user.getFacebookCredentials().getProperty("fbId"));
                followedUser.setFollowingUserName(user.getFirstName());
                followedUser.setFollowingUserLastName(user.getLastName());
                followedUser.setTimeStamp(timeStamp);

                //FIXME: we need this process in a transaction
                DataHandlerFacade.saveFollowingUser(followingUser);
                DataHandlerFacade.saveFollowedUser(followedUser);
            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception in following user : " + ex.getMessage());
            return false;
        }

        return true;

    }

    /**
     * set all parameters to save this in events.
     *
     * @param user
     * @param userId
     * @return
     * @throws Exception
     */
    public static boolean unFollow(com.intellisophic.linkapedia.api.beans.User user, String userId) throws Exception {

        com.intellisophic.linkapedia.api.beans.User userToFollow = null;
        FollowingUser followingUser = new FollowingUser();
        FollowedUser followedUser = new FollowedUser();
        Date dateTime = new Date();
        long timeStamp = dateTime.getTime();

        try {

            userToFollow = DataHandlerFacade.getUser(userId);
            if (userToFollow != null) {

                followingUser.setFollowingUserId(user.getUserId());
                followingUser.setFollowedUserId(userToFollow.getUserId());
                followingUser.setFollowedUserName(userToFollow.getFirstName());
                followingUser.setFollowedUserFbId(userToFollow.getFacebookCredentials().getProperty("fbId"));
                followingUser.setTimeStamp(timeStamp);

                followedUser.setFollowedUserId(userToFollow.getUserId());
                followedUser.setFollowingUserId(user.getUserId());
                followedUser.setFollowingUserName(user.getFirstName());
                followedUser.setFollowingUserFbId(user.getFacebookCredentials().getProperty("fbId"));
                followedUser.setTimeStamp(timeStamp);

                //FIXME: we need this process in a transaction
                DataHandlerFacade.deleteFollowingUser(followingUser);
                DataHandlerFacade.deleteFollowedUser(followedUser);

            } else {
                return false;
            }
        } catch (Exception ex) {
            log.error("Exception in unfollowing user : " + ex.getMessage());
            return false;
        }
        return true;
    }
}
