package com.linkapedia.builderpage.util;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 *
 * @author andres
 */
public class RegexUtil {

    private static final char[] separatorTerms = {'|', '-', ';', ':', '_',
        '*', '+', '&', ' '};
    private static String REGEX;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        char token;
        for (int k = 0; k < separatorTerms.length; k++) {
            token = separatorTerms[k];
            //replace token for equivalent in regex
            if (token == ' ') {
                token = 's';
            }
            if (k == 0) {
                sb.append("\"\\").append(token).append("\"");
            } else {
                sb.append(",\"\\").append(token).append("\"");
            }
        }
        sb.append("]*");
        REGEX = sb.toString();
    }

    /**
     * Build regex with some rules
     *
     * @param term
     * @return
     */
    public static String getRegexForSentences(String term) {
        StringBuilder regex = new StringBuilder();
        boolean control;
        for (int k = 0; k < term.length(); k++) {
            control = false;
            for (int i = 0; i < separatorTerms.length; i++) {
                if (term.charAt(k) == separatorTerms[i]) {
                    control = true;
                    break;
                }
            }
            if (!control) {
                regex.append(term.charAt(k));
            } else {
                regex.append(REGEX);
            }

        }
        return regex.toString();
    }

    /**
     * remove accent for the string
     *
     * @param s
     * @return
     */
    public static String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }
}
