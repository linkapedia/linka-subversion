/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.builderpage.bo.comparators;

import com.intellisophic.linkapedia.api.beans.NodeDocument;
import java.util.Comparator;

/**
 *
 * @author andres
 *
 */
public class DocumentComparator implements Comparator<NodeDocument> {

    public int compare(NodeDocument o1, NodeDocument o2) {
        int result = (o1.getPagerank() < o2.getPagerank()) ? 1 : (o1.getPagerank() > o2.getPagerank()) ? -1 : 0;
        if (result == 0) {
            result = (o1.getScore01() < o2.getScore01()) ? 1 : (o1.getScore01() > o2.getScore01()) ? -1 : 0;
        }
        if (result == 0) {
            result = (o1.getScore11() < o2.getScore11()) ? 1 : (o1.getScore11() > o2.getScore11()) ? -1 : 0;
        }
        return result;
    }
}
