package com.linkapedia.builderpage.security;

import com.linkapedia.builderpage.util.ManageResourcesUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;

/**
 *
 * @author andres
 */
public class ControlMessageAPI {

    private Properties prop;
    private HttpServletResponse res;
    private PrintWriter pw;
    private String callback;

    public ControlMessageAPI(final HttpServletResponse res, String callback) throws IOException {
        prop = ManageResourcesUtils.getProperties();
        this.res = res;
        this.res.setContentType("application/json;charset=UTF-8");
        this.pw = this.res.getWriter();
        this.callback = callback;
    }

    /**
     * missing parameters
     */
    public void messageMissingParameters() {
        String message = prop.getProperty("com.linkapedia.builderpage.message1");
        JSONObject response = new JSONObject();
        response.put("message", message);
        pw.println(callback + "(" + response.toString() + ");");
    }

    /**
     * page is not generated
     */
    public void messageErrorPageCreation() {
        String message = prop.getProperty("com.linkapedia.builderpage.message2");
        JSONObject response = new JSONObject();
        response.put("message", message);
        pw.println(callback + "(" + response.toString() + ");");
    }

    /**
     * sentences not found
     */
    public void messageSentencesNotFound() {
        String message = prop.getProperty("com.linkapedia.builderpage.message3");
        JSONObject response = new JSONObject();
        response.put("message", message);
        pw.println(callback + "(" + response.toString() + ");");
    }

    /**
     * version not found
     */
    public void messageSentenceVersion() {
        String message = prop.getProperty("com.linkapedia.builderpage.message4");
        JSONObject response = new JSONObject();
        response.put("message", message);
        pw.println(callback + "(" + response.toString() + ");");
    }

    /**
     * error getting sentences
     */
    public void messageErrorSentenceExtracted() {
        String message = prop.getProperty("com.linkapedia.builderpage.message5");
        JSONObject response = new JSONObject();
        response.put("message", message);
        pw.println(callback + "(" + response.toString() + ");");
    }

    /**
     * close the out
     */
    public void close() {
        if (pw != null) {
            this.pw.flush();
            this.pw.close();
        }
    }
}
