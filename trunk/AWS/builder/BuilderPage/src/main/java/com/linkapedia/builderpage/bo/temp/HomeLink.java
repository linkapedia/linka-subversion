/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkapedia.builderpage.bo.temp;

import com.linkapedia.builderpage.util.UtilEnviroment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author juan
 */
public class HomeLink {

    private String nodeId;
    private String docId;
    private String title;
    private String digest;
    private String imageSrc;
    private String link;
    private String cols;

    public String getCols() {
        return cols;
    }

    public void setCols(String cols) {
        this.cols = cols;
    }
    private static List<HomeLink> homeLinks;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(HomeLink.class);

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public static void addHomeLink(HomeLink homeLink) {
        homeLinks.add(homeLink);
    }

    public static List<HomeLink> getHomeLinks() {
        try {
            if (homeLinks == null) {
                LoadData();
            }
        } catch (URISyntaxException ex) {
            Logger.getLogger(HomeLink.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HomeLink.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collections.shuffle(homeLinks);
        return homeLinks.subList(0, 10);
    }

    private static void LoadData() throws URISyntaxException, IOException {
        URL url = HomeLink.class.getResource("/system/temp/home_links.xlsx");
        InputStream inputstream = null;

        File f = new File(url.toURI());
        try {
            inputstream = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            System.err.println("Not found file: " + f.getAbsolutePath());
            return;
        }

        try {

            XSSFWorkbook book = new XSSFWorkbook(inputstream);
            XSSFSheet sheet = book.getSheetAt(0);
            XSSFRow row = null;
            XSSFCell cell = null;
            HomeLink homeLink = null;
            String[] cellId = null;
            Boolean foundEnvironment = false;
            String imageUrl = null;

            for (int i = 0, y = sheet.getLastRowNum(); i < y; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    if (!foundEnvironment) {
                        cell = row.getCell(0);
                        if (cell != null) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            cellId = cell.getStringCellValue().split(",");
                            if (Arrays.asList(cellId).contains(UtilEnviroment.getEnv())) {
                                foundEnvironment = true;
                                homeLinks = new LinkedList<HomeLink>();
                            }
                        }
                    } else {
                        //if already foundEnvironment but is in blank cell, break the for.
                        cell = row.getCell(0);
                        if (cell == null) {
                            break;
                        }
                        homeLink = new HomeLink();
                        row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                        homeLink.setNodeId(row.getCell(1).getStringCellValue());
                        row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
                        homeLink.setDocId(row.getCell(2).getStringCellValue());
                        row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
                        homeLink.setTitle(row.getCell(3).getStringCellValue());
                        row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
                        homeLink.setDigest(row.getCell(4).getStringCellValue());
                        row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
                        homeLink.setLink(row.getCell(5).getStringCellValue());

                        imageUrl = "https://s3.amazonaws.com/documentimage/" + homeLink.getDocId() + ".jpg";
                        homeLink.setImageSrc(imageUrl);

                        //calculate the columns
                        if (homeLink.getDigest().toCharArray().length < 300) {
                            homeLink.setCols("col1");
                        } else if (homeLink.getDigest().toCharArray().length < 650) {
                            homeLink.setCols("col2");
                        } else {
                            //truncate the digests
                            homeLink.setDigest(homeLink.getDigest().substring(0, 650).concat(" ..."));
                            homeLink.setCols("col2");
                        }
                        addHomeLink(homeLink);
                    }

                }
            }
        } catch (Exception ex) {

            log.error("Error in HomeLink: " + ex.getMessage());
        }
    }
}
