package com.linkapedia.builderpage.bo.output;

/**
 *
 * @author andres
 */
public interface IWritable<T> {

    public boolean write(T o);
}
