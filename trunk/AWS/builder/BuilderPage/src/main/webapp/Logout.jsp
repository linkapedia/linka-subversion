<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<f:view>
    <html>
        <head>
            <title>Logout</title>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="css/external.css">
            <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" charset="utf-8">
            </script>
            <script>
                $(document).ready(function() {
                    $('.btnLogin').bind('click', function() {
                        //alert($(this).text());
                    });
                    $('.btnLogin').trigger('click');
                });
            </script>
        </head>
        <body>

            <h:form  id="frmLogin"  styleClass="box login">
                <footer>
                    <h:commandButton value="Logout" action="#{LoginBean.logout}" styleClass="btnLogin"  />
                    <h:messages style="color:red;"  />
                </footer>
            </h:form>
        </body>
    </html>

</f:view>