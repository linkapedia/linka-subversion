<%@page import="com.linkapedia.builderpage.util.ManageResourcesUtils"%>
<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%
    String redirect_uri = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facebook.redirect_uri");  
    String clientId = ManageResourcesUtils.getProperties().getProperty("com.linkapedia.builderpage.facebook.client_id");
%>
<html>
    <head>
        <title>FB login</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/external.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" charset="utf-8">
        </script>
    </head>
    <body>

        <script>
            window.location = "https://www.facebook.com/dialog/oauth/?client_id=<%=clientId%>&redirect_uri=<%=redirect_uri%>";
        </script>

    </body>
</html>
