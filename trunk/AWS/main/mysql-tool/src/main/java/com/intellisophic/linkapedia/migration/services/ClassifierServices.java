package com.intellisophic.linkapedia.migration.services;

import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import com.intellisophic.linkapedia.migration.facade.ClassifierFacade;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class ClassifierServices {

    private static final Logger log = LoggerFactory.getLogger(ClassifierServices.class);
    private ClassifierFacade classifierFacade;

    public ClassifierServices() {
        this.classifierFacade = new ClassifierFacade();
    }

    public ClassifierServices(ClassifierFacade classifierFacade) {
        this.classifierFacade = classifierFacade;
    }

    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws Exception {
        log.debug("getCorpusInfoByNodeID(Long, Long)");
        return getClassifierFacade().getCorpusInfoByNodeID(corpusID, nodeID);
    }

    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws Exception {
        log.debug("getAllCorpusInfo(Long)");
        return getClassifierFacade().getAllCorpusInfo(corpusID);
    }

    public ClassifierFacade getClassifierFacade() {
        return classifierFacade;
    }

    public void setClassifierFacade(ClassifierFacade classifierFacade) {
        this.classifierFacade = classifierFacade;
    }
}
