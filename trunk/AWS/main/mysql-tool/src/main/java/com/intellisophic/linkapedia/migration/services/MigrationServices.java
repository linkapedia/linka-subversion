package com.intellisophic.linkapedia.migration.services;

import com.intellisophic.linkapedia.api.beans.DocumentNode;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.datasources.mysql.MySQLObjectTransformer;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class MigrationServices {

    private static final Logger log = LoggerFactory.getLogger(MigrationServices.class);
    private ClassifierServices classifierServices;

    public MigrationServices() {
        this.classifierServices = new ClassifierServices();
    }

    public MigrationServices(ClassifierServices classifierServices) {
        this.classifierServices = classifierServices;
    }

    public void migrateFromMySQLToDynamoDB(Long corpusID) throws Exception {
        log.debug("migrateFromMySQLToDynamoDB(Long)");
        List<CorpusInfo> corpusInfoList = null;
        try {
            corpusInfoList = getClassifierServices().getAllCorpusInfo(corpusID);
            for (CorpusInfo corpusInfo : corpusInfoList) {
                NodeDocument nodeDocument = (NodeDocument) MySQLObjectTransformer.transform(corpusInfo, MySQLObjectTransformer.NODEDOCUMENT);
                DataHandlerFacade.saveNodeDocument(nodeDocument);
                DocumentNode documentNode = (DocumentNode) MySQLObjectTransformer.transform(corpusInfo, MySQLObjectTransformer.DOCUMENTNODE);
                DataHandlerFacade.saveDocumentNode(documentNode);
            }
        } catch (Exception e) {
            log.error("An exception has ocurred on migrateFromMySQLToDynamoDB(Long)", e);
            throw e;
        }

    }

    public ClassifierServices getClassifierServices() {
        return classifierServices;
    }

    public void setClassifierServices(ClassifierServices classifierServices) {
        this.classifierServices = classifierServices;
    }
}
