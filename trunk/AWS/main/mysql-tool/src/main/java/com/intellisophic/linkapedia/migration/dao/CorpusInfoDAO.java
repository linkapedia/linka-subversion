package com.intellisophic.linkapedia.migration.dao;

import com.intellisophic.linkapedia.datasources.mysql.MySQLParamHandler;
import com.intellisophic.linkapedia.datasources.mysql.MySQLTemplate;
import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import com.intellisophic.linkapedia.migration.dao.interfaces.ICorpusInfoDAO;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CorpusInfoDAO extends MySQLTemplate implements ICorpusInfoDAO {

    private static final Logger log = LoggerFactory.getLogger(CorpusInfoDAO.class);

    @Override
    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws SQLException, Exception {
        log.debug("getCorpusInfoByNodeID(Long, Long)");
        List<CorpusInfo> classifierResult;
        CorpusInfo corpusInfo;
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String query;
        String docTitle;
        try {
            classifierResult = new ArrayList<CorpusInfo>();
            query = "SELECT NODEID, URI, WEBCACHEURI, DOCTITLE, TIMESTAMP, SCORE1, SCORE2, SCORE3, SCORE4, SCORE5, SCORE6, SCORE7, SCORE8, SCORE9, SCORE10, SCORE11, SCORE12, PAGERANK FROM %%tablename%% WHERE NODEID = ? GROUP BY URI, SCORE1";
            query = query.replace("%%tablename%%", "corpus" + corpusID);
            conn = createConnection(MySQLParamHandler.getProperty("hostname"), MySQLParamHandler.getProperty("dbname"), MySQLParamHandler.getProperty("username"), MySQLParamHandler.getProperty("password"));
            pstmt = conn.prepareStatement(query);
            pstmt.setLong(1, nodeID);
            rs = pstmt.executeQuery();

            String URL;
            while (rs.next()) {
                corpusInfo = new CorpusInfo();
                corpusInfo.setNodeID(nodeID);
                corpusInfo.setURI(rs.getString("URI"));
                docTitle = rs.getString("DOCTITLE");
                if (docTitle == null) {
                    docTitle = "";
                }
                corpusInfo.setDocTitle(docTitle);
                corpusInfo.setScore01(rs.getFloat("SCORE1"));
                corpusInfo.setScore02(rs.getFloat("SCORE2"));
                corpusInfo.setScore03(rs.getFloat("SCORE3"));
                corpusInfo.setScore04(rs.getFloat("SCORE4"));
                corpusInfo.setScore05(rs.getFloat("SCORE5"));
                corpusInfo.setScore06(rs.getFloat("SCORE6"));
                corpusInfo.setScore07(rs.getFloat("SCORE7"));
                corpusInfo.setScore08(rs.getFloat("SCORE8"));
                corpusInfo.setScore09(rs.getFloat("SCORE9"));
                corpusInfo.setScore10(rs.getFloat("SCORE10"));
                corpusInfo.setScore11(rs.getFloat("SCORE11"));
                corpusInfo.setScore12(rs.getFloat("SCORE12"));
                corpusInfo.setPagerank(rs.getFloat("PAGERANK"));
                classifierResult.add(corpusInfo);
            }
        } catch (CommunicationsException e) {
            log.error("Error in getCorpusInfoByNodeID (ConnectionFailure). ", e);
            throw e;
        } catch (SQLException e) {
            log.error("Error in getCorpusInfoByNodeID (SQLFailure). ", e);
            throw e;
        } catch (Exception e) {
            log.error("Error in getCorpusInfoByNodeID. ", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("General Error in getClassifierDataForNodeId() closing resources");
                throw e;
            }
        }
        return classifierResult;
    }

    @Override
    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws SQLException, Exception {
        log.debug("getAllCorpusInfo(Long)");
        List<CorpusInfo> corpusInfoByNodeList;
        CorpusInfo corpusInfo;
        Statement stmt = null;
        Connection conn = null;
        ResultSet rs = null;
        String query;
        String docTitle;
        try {
            corpusInfoByNodeList = new ArrayList<CorpusInfo>();
            query = "SELECT NODEID, URI, WEBCACHEURI, DOCTITLE, TIMESTAMP, SCORE1, SCORE2, SCORE3, SCORE4, SCORE5, SCORE6, SCORE7, SCORE8, SCORE9, SCORE10, SCORE11, SCORE12, PAGERANK FROM %%tablename%% GROUP BY URI, SCORE1";
            query = query.replace("%%tablename%%", "corpus" + corpusID);
            conn = createConnection(MySQLParamHandler.getProperty("hostname"), MySQLParamHandler.getProperty("dbname"), MySQLParamHandler.getProperty("username"), MySQLParamHandler.getProperty("password"));
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                corpusInfo = new CorpusInfo();
                corpusInfo.setNodeID(rs.getLong("NODEID"));
                corpusInfo.setURI(rs.getString("URI"));
                docTitle = rs.getString("DOCTITLE");
                if (docTitle == null) {
                    docTitle = "";
                }
                corpusInfo.setDocTitle(docTitle);
                corpusInfo.setScore01(rs.getFloat("SCORE1"));
                corpusInfo.setScore02(rs.getFloat("SCORE2"));
                corpusInfo.setScore03(rs.getFloat("SCORE3"));
                corpusInfo.setScore04(rs.getFloat("SCORE4"));
                corpusInfo.setScore05(rs.getFloat("SCORE5"));
                corpusInfo.setScore06(rs.getFloat("SCORE6"));
                corpusInfo.setScore07(rs.getFloat("SCORE7"));
                corpusInfo.setScore08(rs.getFloat("SCORE8"));
                corpusInfo.setScore09(rs.getFloat("SCORE9"));
                corpusInfo.setScore10(rs.getFloat("SCORE10"));
                corpusInfo.setScore11(rs.getFloat("SCORE11"));
                corpusInfo.setScore12(rs.getFloat("SCORE12"));
                corpusInfo.setPagerank(rs.getFloat("PAGERANK"));
                corpusInfoByNodeList.add(corpusInfo);
            }
        } catch (CommunicationsException e) {
            log.error("Error in getAllCorpusInfo (ConnectionFailure). ", e);
            throw e;
        } catch (SQLException e) {
            log.error("Error in getAllCorpusInfo (SQLFailure). ", e);
            throw e;
        } catch (Exception e) {
            log.error("Error in getAllCorpusInfo. ", e);
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("General Error in getClassifierDataForNodeId() closing resources");
                throw e;
            }
        }
        return corpusInfoByNodeList;
    }
}
