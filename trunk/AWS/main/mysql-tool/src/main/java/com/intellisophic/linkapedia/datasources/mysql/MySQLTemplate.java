package com.intellisophic.linkapedia.datasources.mysql;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class MySQLTemplate {

    private static final Logger log = LoggerFactory.getLogger(MySQLTemplate.class);

    /**
     *
     * @param ipAddress
     * @param database
     * @param user
     * @param password
     * @return
     * @throws CommunicationsException
     * @throws SQLException
     * @throws Exception
     */
    public static Connection createConnection(String ipAddress, String database, String user, String password) throws CommunicationsException, SQLException, Exception {
        log.debug("createConnection(String)");
        Connection conn = null;
        try {
            conn = createConnection("jdbc:mysql://" + ipAddress + "/" + database, user, password);
        } catch (CommunicationsException ex) {
            log.error("An exception ocurred. Cannot find database in host {" + ipAddress + "}. " + ex.getLocalizedMessage());
            throw ex;
        } catch (Exception ex) {
            log.error("An exception ocurred. " + ex.getLocalizedMessage());
            throw ex;
        }
        return conn;
    }

    /**
     *
     * @param dbURL
     * @param user
     * @param password
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private static Connection createConnection(String dbURL, String user, String password) throws SQLException, ClassNotFoundException {
        log.debug("createConnection(String,String,String)");
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(dbURL, user, password);
    }

    /**
     * Method to release a used connection.
     *
     * @param conn Connection to be released.
     * @return true if the connection was released successfully.
     */
    public static boolean releaseConnection(Connection conn) {
        log.debug("releaseConnection(Connection)");
        if (conn != null) {
            try {
                conn.close();
                conn = null;
                return true;
            } catch (SQLException ex) {
                log.error("An exception has ocurred while closing database resources.");
            }
        }
        return false;
    }
}
