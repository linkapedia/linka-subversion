package com.intellisophic.linkapedia.migration.bo;

import com.intellisophic.linkapedia.migration.bo.interfaces.ICorpusInfoBO;
import com.intellisophic.linkapedia.migration.bo.models.CorpusInfo;
import com.intellisophic.linkapedia.migration.dao.CorpusInfoDAO;
import com.intellisophic.linkapedia.migration.dao.interfaces.ICorpusInfoDAO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CorpusInfoBO implements ICorpusInfoBO {

    private static final Logger log = LoggerFactory.getLogger(CorpusInfoBO.class);
    private ICorpusInfoDAO corpusInfoDAO;

    public CorpusInfoBO() {
        corpusInfoDAO = new CorpusInfoDAO();
    }

    @Override
    public List<CorpusInfo> getCorpusInfoByNodeID(Long corpusID, Long nodeID) throws Exception {
        log.debug("getCorpusInfoByNodeID(Long, Long)");
        return getCorpusInfoDAO().getCorpusInfoByNodeID(corpusID, nodeID);
    }

    @Override
    public List<CorpusInfo> getAllCorpusInfo(Long corpusID) throws Exception {
        log.debug("getAllCorpusInfo(Long)");
        return getCorpusInfoDAO().getAllCorpusInfo(corpusID);
    }

    public ICorpusInfoDAO getCorpusInfoDAO() {
        return corpusInfoDAO;
    }

    public void setCorpusInfoDAO(ICorpusInfoDAO corpusInfoDAO) {
        this.corpusInfoDAO = corpusInfoDAO;
    }
}
