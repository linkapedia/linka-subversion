package com.intellisophic.linkapedia.migration.dao.interfaces;

import java.sql.SQLException;
import java.util.Map;

/**
 *
 * @author Xander Kno
 */
public interface ISignatureDAO {

    public Map<String, Long> getSignatureByNodeID(Long nodeID) throws SQLException, Exception;
}
