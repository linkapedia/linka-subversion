package com.intellisophic.linkapedia.migration.bo.interfaces;

import com.intellisophic.linkapedia.migration.bo.models.Node;
import java.util.List;

/**
 *
 * @author Xander Kno
 */
public interface INodeBO {

    public boolean saveNode(Node node) throws Exception;

    public Node getNode(Long nodeId) throws Exception;

    public boolean deleteNode(Long nodeId) throws Exception;
    //Lists

    public List<Node> getNodesByCorpusId(Long corpusId) throws Exception;

    public List<Node> getNodesByParentID(Long parentID) throws Exception;

    public List<Long> getChildrenByNodeId(Long corpusId) throws Exception;

    public List<String> getDomainsByNodeId(Long nodeId) throws Exception;

    public List<Node> getNodesByNodeId(Long nodeId, Boolean isRecursive) throws Exception;
    
    public List<String> getDomainsByCorpusId(Long corpusId) throws Exception;
}
