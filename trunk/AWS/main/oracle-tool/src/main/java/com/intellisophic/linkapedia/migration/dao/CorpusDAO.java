package com.intellisophic.linkapedia.migration.dao;

import com.intellisophic.linkapedia.datasources.oracle.OracleTemplate;
import com.intellisophic.linkapedia.migration.bo.models.Corpus;
import com.intellisophic.linkapedia.migration.dao.interfaces.ICorpusDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class CorpusDAO extends OracleTemplate implements ICorpusDAO {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(CorpusDAO.class);

    /**
     * Method to retrieve a corpus from the corpus table using the ID.
     * <p/>
     * @param corpusID Corpus identifier.
     * @return A full corpus object if the object is found, null otherwise.
     * @throws Exception
     */
    public Corpus getCorpus(Long corpusID) throws SQLException, Exception {
        log.debug("getCorpus(Long)");
        Corpus corpus = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT C.CORPUSID,C.CORPUS_NAME,C.CORPUSDESC,C.ROCF1,C.ROCF2,C.ROCF3,C.ROCC1,C.ROCC2,C.ROCC3,C.CORPUSACTIVE,C.CORPUSRUNFREQUENCY,C.CORPUSLASTRUN,C.CORPUSLASTSYNC,C.ROCSETTINGID,C.CORPUSAFFINITY,C.CORPUSTERMS,N.NODEID ROOTNODEID FROM CORPUS C, NODE N WHERE C.CORPUSID = N.CORPUSID AND N.PARENTID = -1 AND C.CORPUSID = ? ");
            pst.setLong(1, corpusID);
            rs = pst.executeQuery();
            while (rs.next()) {
                corpus = new Corpus();
                corpus.setID(rs.getLong("CORPUSID"));
                corpus.setRootNodeID(rs.getLong("ROOTNODEID"));
                corpus.setName(rs.getString("CORPUS_NAME"));
                corpus.setDescription(rs.getString("CORPUSDESC"));
                corpus.setRocf1(rs.getFloat("ROCF1"));
                corpus.setRocf2(rs.getFloat("ROCF2"));
                corpus.setRocf3(rs.getFloat("ROCF3"));
                corpus.setRocc1(rs.getFloat("ROCC1"));
                corpus.setRocc2(rs.getFloat("ROCC2"));
                corpus.setRocc3(rs.getFloat("ROCC3"));
                corpus.setActive(rs.getBoolean("CORPUSACTIVE"));
                corpus.setRunFrecuency(rs.getInt("CORPUSRUNFREQUENCY"));
                corpus.setLastRun(rs.getDate("CORPUSLASTRUN"));
                corpus.setLastSync(rs.getDate("CORPUSLASTSYNC"));
                corpus.setRocSettingID(rs.getInt("ROCSETTINGID"));
                corpus.setAffinity(rs.getBoolean("CORPUSAFFINITY"));
                corpus.setTerms(rs.getString("CORPUSTERMS"));
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getNode(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getNode(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return corpus;
    }

    /**
     * Method to retrieve all the corpus in the corpus table.
     * <p/>
     * @return List of corpus from the database.
     * @throws SQLException
     * @throws Exception
     */
    public List<Corpus> getAllCorpus() throws SQLException, Exception {
        log.debug("getAllCorpus()");
        List<Corpus> corpusList = new ArrayList<Corpus>();
        Corpus corpus = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT C.CORPUSID,C.CORPUS_NAME,C.CORPUSDESC,C.ROCF1,C.ROCF2,C.ROCF3,C.ROCC1,C.ROCC2,C.ROCC3,C.CORPUSACTIVE,C.CORPUSRUNFREQUENCY,C.CORPUSLASTRUN,C.CORPUSLASTSYNC,C.ROCSETTINGID,C.CORPUSAFFINITY,C.CORPUSTERMS,N.NODEID ROOTNODEID FROM CORPUS C, NODE N WHERE C.CORPUSID = N.CORPUSID AND N.PARENTID = -1");
            while (rs.next()) {
                corpus = new Corpus();
                corpus.setID(rs.getLong("CORPUSID"));
                corpus.setRootNodeID(rs.getLong("ROOTNODEID"));
                corpus.setName(rs.getString("CORPUS_NAME"));
                corpus.setDescription(rs.getString("CORPUSDESC"));
                corpus.setRocf1(rs.getFloat("ROCF1"));
                corpus.setRocf2(rs.getFloat("ROCF2"));
                corpus.setRocf3(rs.getFloat("ROCF3"));
                corpus.setRocc1(rs.getFloat("ROCC1"));
                corpus.setRocc2(rs.getFloat("ROCC2"));
                corpus.setRocc3(rs.getFloat("ROCC3"));
                corpus.setActive(rs.getBoolean("CORPUSACTIVE"));
                corpus.setRunFrecuency(rs.getInt("CORPUSRUNFREQUENCY"));
                corpus.setLastRun(rs.getDate("CORPUSLASTRUN"));
                corpus.setLastSync(rs.getDate("CORPUSLASTSYNC"));
                corpus.setRocSettingID(rs.getInt("ROCSETTINGID"));
                corpus.setAffinity(rs.getBoolean("CORPUSAFFINITY"));
                corpus.setTerms(rs.getString("CORPUSTERMS"));
                corpusList.add(corpus);
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getAllCorpus()");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getAllCorpus()");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return corpusList;
    }
}
