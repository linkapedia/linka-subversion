package com.intellisophic.linkapedia.migration.bo.interfaces;

/**
 *
 * @author Xander Kno
 */
public interface IMustHaveBO {

    public String getMustHavesByNodeID(Long nodeID) throws Exception;
}
