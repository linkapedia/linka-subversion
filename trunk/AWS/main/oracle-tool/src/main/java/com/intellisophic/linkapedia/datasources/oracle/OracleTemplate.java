package com.intellisophic.linkapedia.datasources.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class OracleTemplate {

    private static final Logger log = Logger.getLogger(OracleTemplate.class);
    private static final ResourceBundle jdbcConfig = ResourceBundle.getBundle("database/jdbc");

    /**
     * Method to create and return an Oracle connection.
     * <p/>
     * @return Newly created connection.
     * @throws SQLException
     */
    public static synchronized Connection getConnection() throws SQLException, ClassNotFoundException, InterruptedException {
        log.debug("getConnection()");
        Connection conn = null;
        try {
            // Load the JDBC driver
            String driverName = jdbcConfig.getString("jdbc.driver.name");
            Class.forName(driverName);

            // Create a connection to the database
            String url = jdbcConfig.getString("jdbc.database.url");
            String username = jdbcConfig.getString("jdbc.database.username");
            String password = jdbcConfig.getString("jdbc.database.password");

            while (conn == null) {
                try {
                    conn = DriverManager.getConnection(url, username, password);
                } catch (SQLException e) {
                    log.error("Could not connect to the database", e);
                }
                if (conn == null) {
                    Thread.sleep(10 * 1000);
                }
            }
        } catch (ClassNotFoundException e) {
            log.error("Could not find the database driver");
            throw e;
        } catch (InterruptedException e) {
            log.error("Could not get dabatase connection.");
            throw e;
        }
        return conn;
    }

    /**
     * Method to release a used connection.
     * <p/>
     * @param conn Connection to be released.
     * @return true if the connection was released successfully.
     */
    public static boolean releaseConnection(Connection conn) {
        log.debug("releaseConnection(Connection)");
        if (conn != null) {
            try {
                conn.close();
                conn = null;
                return true;
            } catch (SQLException ex) {
                log.error("An exception has ocurred while closing database resources.");
            }
        }
        return false;
    }
}
