package com.intellisophic.linkapedia.migration.bo.interfaces;

import java.util.Map;

/**
 *
 * @author Xander Kno
 */
public interface ISignatureBO {

    public Map<String, Long> getSignatureByNodeID(Long nodeID) throws Exception;
}
