package com.intellisophic.linkapedia.migration.dao;

import com.intellisophic.linkapedia.datasources.oracle.OracleTemplate;
import static com.intellisophic.linkapedia.datasources.oracle.OracleTemplate.getConnection;
import static com.intellisophic.linkapedia.datasources.oracle.OracleTemplate.releaseConnection;
import com.intellisophic.linkapedia.migration.bo.models.Node;
import com.intellisophic.linkapedia.migration.dao.interfaces.INodeDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class NodeDAO extends OracleTemplate implements INodeDAO {

    private static final Logger log = Logger.getLogger(NodeDAO.class);

    public boolean createNode(Node node) throws SQLException, Exception {
        log.debug("createNode(Node)");
        boolean success = false;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        int state = 0;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("INSERT INTO NODE (NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, "
                    + "DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, "
                    + "LISTNODE, NODETYPE, BRIDGENODE ) VALUES ( ? , ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
            pst.setLong(1, node.getId());
            pst.setLong(2, node.getCorpusId());
            pst.setString(3, node.getTitle());
            pst.setInt(4, node.getSize());
            pst.setLong(5, node.getParentId());
            pst.setInt(6, node.getIndexWithinParent());
            pst.setInt(7, node.getDepthFromRoot());
            if (node.getScanned() != null) {
                pst.setDate(8, new java.sql.Date(node.getScanned().getTime()));
            } else {
                pst.setDate(8, null);
            }

            if (node.getUpdated() != null) {
                pst.setDate(9, new java.sql.Date(node.getUpdated().getTime()));
            } else {
                pst.setDate(9, null);
            }
            pst.setInt(10, node.getStatus());
            pst.setString(11, node.getlTitle());

            if (node.getLastSync() != null) {
                pst.setDate(12, new java.sql.Date(node.getLastSync().getTime()));
            } else {
                pst.setDate(12, null);
            }
            pst.setString(13, node.getDescription());
            pst.setLong(14, node.getLinkNodeId());
            pst.setBoolean(15, node.getListNode());
            pst.setInt(16, node.getType());
            pst.setBoolean(17, node.getBridgeNode());
            state = pst.executeUpdate();
            if (state > 0) {
                success = true;
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on createNode(Node)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on createNode(Node)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return success;
    }

    public Node getNode(Long nodeID) throws SQLException, Exception {
        log.debug("getNode(Long)");
        Node node = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE FROM NODE WHERE NODEID = ?");
            pst.setLong(1, nodeID);
            rs = pst.executeQuery();
            while (rs.next()) {
                node = new Node();
                node.setId(rs.getLong("NODEID"));
                node.setCorpusId(rs.getLong("CORPUSID"));
                node.setTitle(rs.getString("NODETITLE"));
                node.setSize(rs.getInt("NODESIZE"));
                node.setParentId(rs.getLong("PARENTID"));
                node.setIndexWithinParent(rs.getInt("NODEINDEXWITHINPARENT"));
                node.setDepthFromRoot(rs.getInt("DEPTHFROMROOT"));
                node.setScanned(rs.getDate("DATESCANNED"));
                node.setUpdated(rs.getDate("DATEUPDATED"));
                node.setStatus(rs.getInt("NODESTATUS"));
                node.setlTitle("NODELTITLE");
                node.setLastSync(rs.getDate("NODELASTSYNC"));
                node.setDescription(rs.getString("NODEDESC"));
                node.setLinkNodeId(rs.getLong("LINKNODEID"));
                node.setListNode(rs.getBoolean("LISTNODE"));
                node.setType(rs.getInt("NODETYPE"));
                node.setBridgeNode(rs.getBoolean("BRIDGENODE"));
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getNode(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getNode(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return node;
    }

    public boolean updateNode(Node node) throws SQLException, Exception {
        log.debug("updateNode(Node)");
        boolean success = false;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        int state = 0;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("UPDATE NODE SET CORPUSID=?, NODETITLE=?, NODESIZE=?, PARENTID=?, NODEINDEXWITHINPARENT=?, DEPTHFROMROOT=?, DATESCANNED=?, DATEUPDATED=?, NODESTATUS=?, NODELTITLE=?, NODELASTSYNC=?, NODEDESC=?, LINKNODEID=?, LISTNODE=?, NODETYPE=?, BRIDGENODE=? WHERE NODEID = ?");
            pst.setLong(1, node.getCorpusId());
            pst.setString(2, node.getTitle());
            pst.setInt(3, node.getSize());
            pst.setLong(4, node.getParentId());
            pst.setInt(5, node.getIndexWithinParent());
            pst.setInt(6, node.getDepthFromRoot());
            if (node.getScanned() != null) {
                pst.setDate(7, new java.sql.Date(node.getScanned().getTime()));
            } else {
                pst.setDate(7, null);
            }

            if (node.getUpdated() != null) {
                pst.setDate(8, new java.sql.Date(node.getUpdated().getTime()));
            } else {
                pst.setDate(8, null);
            }
            pst.setInt(9, node.getStatus());
            pst.setString(10, node.getlTitle());

            if (node.getLastSync() != null) {
                pst.setDate(11, new java.sql.Date(node.getLastSync().getTime()));
            } else {
                pst.setDate(11, null);
            }
            pst.setString(12, node.getDescription());
            pst.setLong(13, node.getLinkNodeId());
            pst.setBoolean(14, node.getListNode());
            pst.setInt(15, node.getType());
            pst.setBoolean(16, node.getBridgeNode());
            pst.setLong(17, node.getId());
            state = pst.executeUpdate();
            if (state > 0) {
                success = true;
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on updateNode(Node)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on updateNode(Node)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return success;
    }

    public boolean deleteNode(Long nodeId) throws SQLException, Exception {
        log.debug("deleteNode(Long)");
        boolean success = false;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        int state = 0;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("DELETE * FROM NODE WHERE NODEID = ?");
            pst.setLong(1, nodeId);
            state = pst.executeUpdate();
            if (state > 0) {
                success = true;
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on deleteNode(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on deleteNode(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return success;
    }

    public List<Node> getNodesByCorpusID(Long corpusId) throws SQLException, Exception {
        log.debug("getNodesByCorpusId(Long)");
        List<Node> nodeList = new ArrayList<Node>();
        Node tmpNode = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE, DISCUSSIONID FROM NODE WHERE CORPUSID = ?");
            pst.setLong(1, corpusId);
            rs = pst.executeQuery();
            while (rs.next()) {
                tmpNode = new Node();
                tmpNode.setId(rs.getLong("NODEID"));
                tmpNode.setCorpusId(rs.getLong("CORPUSID"));
                tmpNode.setTitle(rs.getString("NODETITLE"));
                tmpNode.setSize(rs.getInt("NODESIZE"));
                tmpNode.setParentId(rs.getLong("PARENTID"));
                tmpNode.setIndexWithinParent(rs.getInt("NODEINDEXWITHINPARENT"));
                tmpNode.setDepthFromRoot(rs.getInt("DEPTHFROMROOT"));
                tmpNode.setScanned(rs.getDate("DATESCANNED"));
                tmpNode.setUpdated(rs.getDate("DATEUPDATED"));
                tmpNode.setStatus(rs.getInt("NODESTATUS"));
                tmpNode.setlTitle("NODELTITLE");
                tmpNode.setLastSync(rs.getDate("NODELASTSYNC"));
                tmpNode.setDescription(rs.getString("NODEDESC"));
                tmpNode.setLinkNodeId(rs.getLong("LINKNODEID"));
                tmpNode.setListNode(rs.getBoolean("LISTNODE"));
                tmpNode.setType(rs.getInt("NODETYPE"));
                tmpNode.setBridgeNode(rs.getBoolean("BRIDGENODE"));
                tmpNode.setDiscussId(rs.getString("DISCUSSIONID"));
                nodeList.add(tmpNode);
            }

        } catch (SQLException e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return nodeList;
    }

    /**
     * get all children from one node FIXME: use pagination if we need a lot
     * nodes
     *
     * @param id
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public List<Long> getChildrenByNodeId(Long id) throws SQLException, Exception {
        log.debug("getNodesIdsByParentId(Long)");
        List<Long> listReturn = new ArrayList<Long>();
        String sql = "SELECT n.NODEID FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getLong("NODEID"));
            }
            return listReturn;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
    }

    public List<Node> getNodesByParentID(Long parentID) throws SQLException, Exception {
        log.debug("getNodesByCorpusId(Long)");
        List<Node> nodeList = new ArrayList<Node>();
        Node tmpNode = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            pst = conn.prepareStatement("SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE FROM NODE WHERE PARENTID = ?");
            pst.setLong(1, parentID);
            rs = pst.executeQuery();
            while (rs.next()) {
                tmpNode = new Node();
                tmpNode.setId(rs.getLong("NODEID"));
                tmpNode.setCorpusId(rs.getLong("CORPUSID"));
                tmpNode.setTitle(rs.getString("NODETITLE"));
                tmpNode.setSize(rs.getInt("NODESIZE"));
                tmpNode.setParentId(rs.getLong("PARENTID"));
                tmpNode.setIndexWithinParent(rs.getInt("NODEINDEXWITHINPARENT"));
                tmpNode.setDepthFromRoot(rs.getInt("DEPTHFROMROOT"));
                tmpNode.setScanned(rs.getDate("DATESCANNED"));
                tmpNode.setUpdated(rs.getDate("DATEUPDATED"));
                tmpNode.setStatus(rs.getInt("NODESTATUS"));
                tmpNode.setlTitle("NODELTITLE");
                tmpNode.setLastSync(rs.getDate("NODELASTSYNC"));
                tmpNode.setDescription(rs.getString("NODEDESC"));
                tmpNode.setLinkNodeId(rs.getLong("LINKNODEID"));
                tmpNode.setListNode(rs.getBoolean("LISTNODE"));
                tmpNode.setType(rs.getInt("NODETYPE"));
                tmpNode.setBridgeNode(rs.getBoolean("BRIDGENODE"));
                nodeList.add(tmpNode);
            }
        } catch (SQLException e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return nodeList;
    }

    public List<Node> getNodesByNodeId(Long nodeId, Boolean isRecursive) throws SQLException, Exception {

        log.debug("getNodesByNodeId(Long,Boolean)");
        List<Node> nodeList = new ArrayList<Node>();
        Node tmpNode = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String sql = null;
        try {
            conn = getConnection();
            if (isRecursive) {
                sql = "SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE FROM NODE n START WITH n.NODEID = ? CONNECT BY PRIOR n.NODEID = n.PARENTID";
            } else {
                sql = "SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE FROM NODE n where n.NODEID = ? ";
            }

            //pst = conn.prepareStatement("SELECT NODEID, CORPUSID, NODETITLE, NODESIZE, PARENTID, NODEINDEXWITHINPARENT, DEPTHFROMROOT, DATESCANNED, DATEUPDATED, NODESTATUS, NODELTITLE, NODELASTSYNC, NODEDESC, LINKNODEID, LISTNODE, NODETYPE, BRIDGENODE FROM NODE WHERE CORPUSID = ?");
            pst = conn.prepareStatement(sql);
            pst.setLong(1, nodeId);
            rs = pst.executeQuery();
            while (rs.next()) {
                tmpNode = new Node();
                tmpNode.setId(rs.getLong("NODEID"));
                tmpNode.setCorpusId(rs.getLong("CORPUSID"));
                tmpNode.setTitle(rs.getString("NODETITLE"));
                tmpNode.setSize(rs.getInt("NODESIZE"));
                tmpNode.setParentId(rs.getLong("PARENTID"));
                tmpNode.setIndexWithinParent(rs.getInt("NODEINDEXWITHINPARENT"));
                tmpNode.setDepthFromRoot(rs.getInt("DEPTHFROMROOT"));
                tmpNode.setScanned(rs.getDate("DATESCANNED"));
                tmpNode.setUpdated(rs.getDate("DATEUPDATED"));
                tmpNode.setStatus(rs.getInt("NODESTATUS"));
                tmpNode.setlTitle("NODELTITLE");
                tmpNode.setLastSync(rs.getDate("NODELASTSYNC"));
                tmpNode.setDescription(rs.getString("NODEDESC"));
                tmpNode.setLinkNodeId(rs.getLong("LINKNODEID"));
                tmpNode.setListNode(rs.getBoolean("LISTNODE"));
                tmpNode.setType(rs.getInt("NODETYPE"));
                tmpNode.setBridgeNode(rs.getBoolean("BRIDGENODE"));
                nodeList.add(tmpNode);
            }

        } catch (SQLException e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } catch (Exception e) {
            log.error("An exception has ocurred on getNodesByCorpusId(Long)");
            throw e;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
        return nodeList;


    }

    public List<String> getDomainsByNodeId(Long nodeId) throws SQLException, Exception {
        log.debug("getDomainsByNodeId(Long)");
        List<String> listReturn = new ArrayList<String>();
        String sql = "select corpus_name from node "
                + "inner join corpus "
                + "on node.corpusid = corpus.corpusid "
                + "where node.corpusid in "
                + "(select corpusid from corpus where corpusdomain = 1) "
                + "and linknodeid != nodeid "
                + "and linknodeid = ?";

        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, nodeId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getString("corpus_name"));
            }
            return listReturn;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
    }

    public List<String> getDomainsByCorpusId(Long corpusId) throws SQLException, Exception {
        log.debug("getDomainsByCorpusId(Long)");
        List<String> listReturn = new ArrayList<String>();
        String sql = "select distinct corpus_name from node "
                + "inner join corpus "
                + "on node.corpusid = corpus.corpusid "
                + "where node.corpusid in "
                + "(select corpusid from corpus where corpusdomain = 1) "
                + "and linknodeid != nodeid "
                + "and linknodeid in (select nodeid from node where corpusid = ?)";
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, corpusId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listReturn.add(rs.getString("corpus_name"));
            }
            return listReturn;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    releaseConnection(conn);
                }
            } catch (Exception e) {
                log.error("An exception has ocurred", e);
            }
        }
    }
}
