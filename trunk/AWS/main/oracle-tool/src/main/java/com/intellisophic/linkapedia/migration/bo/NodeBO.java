package com.intellisophic.linkapedia.migration.bo;

import com.intellisophic.linkapedia.generic.services.cache.CacheHandler;
import com.intellisophic.linkapedia.generic.services.cache.CacheObject;
import com.intellisophic.linkapedia.migration.bo.interfaces.INodeBO;
import com.intellisophic.linkapedia.migration.bo.models.Node;
import com.intellisophic.linkapedia.migration.dao.NodeDAO;
import com.intellisophic.linkapedia.migration.dao.interfaces.INodeDAO;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class NodeBO implements INodeBO {

    private static final String NODE_CACHE_IDENTIFIER = "nodes";
    private INodeDAO nodeDAO;
    private static final Logger log = Logger.getLogger(NodeBO.class);

    public NodeBO() {
        nodeDAO = new NodeDAO();
        CacheHandler.initCacheHandler(new String[]{NODE_CACHE_IDENTIFIER});
    }

    public boolean saveNode(Node node) throws Exception {
        log.debug("saveNode(Node)");
        Node tmpNode = null;
        try {
            tmpNode = getNodeDAO().getNode(node.getId());
            if (tmpNode == null) {
                return getNodeDAO().createNode(node);
            } else {
                return getNodeDAO().updateNode(node);
            }
        } catch (Exception e) {
            log.error("An exception has ocurred on saveNode(Node)");
            throw e;
        }
    }

    public Node getNode(Long nodeId) throws Exception {
        log.debug("getNode(Long)");
        CacheObject cache = CacheHandler.getCachedObjectHandler(NODE_CACHE_IDENTIFIER);
        Node node = null;
        if (cache.getObject(nodeId) == null) {
            node = getNodeDAO().getNode(nodeId);
            if (node != null) {
                cache.setObject(node.getId(), node);
            } else {
                log.warn("Node with ID {" + nodeId + "} not found.");
            }
        } else {
            node = (Node) cache.getObject(nodeId);
        }
        return node;
    }

    public boolean deleteNode(Long nodeId) throws Exception {
        log.debug("deleteNode(Long)");
        return getNodeDAO().deleteNode(nodeId);
    }

    public List<Node> getNodesByCorpusId(Long corpusId) throws Exception {
        log.debug("getNodesByCorpusId(Long)");
        return getNodeDAO().getNodesByCorpusID(corpusId);
    }

    public List<Long> getChildrenByNodeId(Long nodeId) throws Exception {
        log.debug("getChildrenByNodeId(Long)");
        return getNodeDAO().getChildrenByNodeId(nodeId);
    }

    public List<Node> getNodesByParentID(Long parentID) throws Exception {
        log.debug("getNodesByParentID(Long)");
        List<Node> nodeList = getNodeDAO().getNodesByParentID(parentID);
        return nodeList;
    }

    public List<Node> getNodesByNodeId(Long nodeId, Boolean isRecursive) throws Exception {
        log.debug("getNodesByNodeId(Long,Boolean)");
        List<Node> nodeList = getNodeDAO().getNodesByNodeId(nodeId, isRecursive);
        return nodeList;
    }

    public INodeDAO getNodeDAO() {
        return nodeDAO;
    }

    public void setNodeDAO(INodeDAO nodeDAO) {
        this.nodeDAO = nodeDAO;
    }

    public List<String> getDomainsByNodeId(Long nodeId) throws Exception {
        log.debug("getDomainsByNodeId(Long)");
        return getNodeDAO().getDomainsByNodeId(nodeId);
    }

    public List<String> getDomainsByCorpusId(Long corpusId) throws Exception {
        log.debug("getDomainsByCorpusId(Long)");
        return getNodeDAO().getDomainsByCorpusId(corpusId);
    }
}
