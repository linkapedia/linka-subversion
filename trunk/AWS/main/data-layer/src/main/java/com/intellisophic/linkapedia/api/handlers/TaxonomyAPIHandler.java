/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.Taxonomy;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class TaxonomyAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(TaxonomyAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(TaxonomyAPIHandler.class.getName());
    private static final String TAXONOMY_CACHE_NAME = "Taxonomy";
    private static final int ONE_HOUR=3600;

    public static Taxonomy getTaxonomy(String corpusId) throws Exception {
        log.info("getTaxonomy - " + corpusId);
        Taxonomy taxonomy = null;
        Object obj = null;
        try {
            obj = cache.get(TAXONOMY_CACHE_NAME + corpusId);
            if (obj != null) {
                taxonomy = (Taxonomy) obj;
                return taxonomy;
            } else {
                taxonomy = getDynamoDBMapper().load(Taxonomy.class, corpusId);
                cache.set(TAXONOMY_CACHE_NAME + corpusId, taxonomy, ONE_HOUR);
                
            }
        } catch (Exception ex) {
            log.error("Exception in getTaxonomy - " + corpusId + " - " + ex.getMessage());
            throw ex;
        }
        
        return taxonomy;
    }

    public static void saveTaxonomy(Taxonomy taxonomy) {
       //TODO
        getDynamoDBMapper().save(taxonomy);
    }
}
