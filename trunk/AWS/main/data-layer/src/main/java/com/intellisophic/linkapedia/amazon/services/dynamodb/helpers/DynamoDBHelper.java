package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodb.model.CreateTableRequest;
import com.amazonaws.services.dynamodb.model.DeleteTableRequest;
import com.amazonaws.services.dynamodb.model.DeleteTableResult;
import com.amazonaws.services.dynamodb.model.DescribeTableRequest;
import com.amazonaws.services.dynamodb.model.KeySchema;
import com.amazonaws.services.dynamodb.model.KeySchemaElement;
import com.amazonaws.services.dynamodb.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodb.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodb.model.TableDescription;
import com.amazonaws.services.dynamodb.model.TableStatus;
import com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.DynamoAnnotationParser;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBClientAbs;
import com.intellisophic.linkapedia.amazon.services.dynamodb.exceptions.DynamoDBAnnotationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class DynamoDBHelper extends DynamoDBClientAbs {

    private static final Logger log = LoggerFactory.getLogger(DynamoDBHelper.class);

    /**
     * Method to create a table on dynamo db based on the metadata provided.
     * <p/>
     * @param metadata Metadata extracted from a Model.
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
     */
    public static void createTableFromMetadata(DynamoTableMetadata metadata) {
        log.debug("createTableFromMetadata(DynamoTableMetadata)");
        KeySchema ks = null;
        // Create a table with a primary key named 'name', which holds a string
        if (metadata.hasRangeKey()) {
            ks = new KeySchema(new KeySchemaElement().withAttributeName(metadata.getHashKey()).withAttributeType(metadata.getHashKeyType())).withRangeKeyElement(new KeySchemaElement().withAttributeName(metadata.getRangeKey()).withAttributeType(metadata.getRangeKeyType()));
        } else {
            ks = new KeySchema(new KeySchemaElement().withAttributeName(metadata.getHashKey()).withAttributeType(metadata.getHashKeyType()));
        }
        CreateTableRequest createTableRequest =
                new CreateTableRequest().withTableName(metadata.getTableName()).withKeySchema(ks).withProvisionedThroughput(
                new ProvisionedThroughput().withReadCapacityUnits(metadata.getReadCapacity()).withWriteCapacityUnits(metadata.getWriteCapacity()));
        TableDescription createdTableDescription = getDynamoDBClient().createTable(createTableRequest).getTableDescription();
        log.debug("Created Table: " + createdTableDescription);
        // Wait for it to become active
        waitForTableToBecomeAvailable(metadata.getTableName());
    }

    /**
     * Method to create a table on dynamo db based on the class provided.
     * <p/>
     * @param clazz Class used as base to create the table on AmazonDynamoDB.
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
     */
    public static void createTableFromClass(Class clazz) {
        log.debug("createTableFromClass(Class)");
        KeySchema ks = null;
        try {
            DynamoTableMetadata metadata = DynamoAnnotationParser.parse(clazz);
            // Create a table with a primary key named 'name', which holds a string
            if (metadata.hasRangeKey()) {
                ks = new KeySchema(new KeySchemaElement().withAttributeName(metadata.getHashKey()).withAttributeType(metadata.getHashKeyType())).withRangeKeyElement(new KeySchemaElement().withAttributeName(metadata.getRangeKey()).withAttributeType(metadata.getRangeKeyType()));
            } else {
                ks = new KeySchema(new KeySchemaElement().withAttributeName(metadata.getHashKey()).withAttributeType(metadata.getHashKeyType()));
            }
            CreateTableRequest createTableRequest =
                    new CreateTableRequest().withTableName(metadata.getTableName()).withKeySchema(ks).withProvisionedThroughput(
                    new ProvisionedThroughput().withReadCapacityUnits(metadata.getReadCapacity()).withWriteCapacityUnits(metadata.getWriteCapacity()));
            TableDescription createdTableDescription = getDynamoDBClient().createTable(createTableRequest).getTableDescription();
            log.debug("Created Table: " + createdTableDescription);
            // Wait for it to become active
            waitForTableToBecomeAvailable(metadata.getTableName());
        } catch (DynamoDBAnnotationException ex) {
            log.error("An exception has ocurred on createTableFromClass(Class) while parsing class annotations.", ex);
        } catch (Exception ex) {
            log.error("An exception has ocurred on createTableFromClass(Class).", ex);
        }
    }

    /**
     * Method to delete a table from dynamo db based on the class provided.
     * <p/>
     * @param clazz Class used as base to delete the table on AmazonDynamoDB.
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
     */
    public static void deleteTable(Class clazz) {
        log.debug("deleteTable(Class)");
        try {
            DynamoTableMetadata metadata = DynamoAnnotationParser.parse(clazz);
            DeleteTableRequest deleteTableRequest = new DeleteTableRequest().withTableName(metadata.getTableName());
            DeleteTableResult deletedTableResult = getDynamoDBClient().deleteTable(deleteTableRequest);
            log.debug("Deleted Table: " + deletedTableResult);
            // Wait for it to become active
            waitForTableToBeDeleted(metadata.getTableName());
        } catch (DynamoDBAnnotationException ex) {
            log.error("An exception has ocurred on deleteTable(Class) while parsing class annotations.", ex);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteTable(Class).", ex);
        }
    }

    /**
     * Method to delete a table from dynamo db based on the metadata provided.
     * <p/>
     * @param metadata Metadata extracted from a Model..
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata
     */
    public static void deleteTable(DynamoTableMetadata metadata) {
        log.debug("deleteTable(DynamoTableMetadata)");
        try {
            DeleteTableRequest deleteTableRequest =
                    new DeleteTableRequest().withTableName(metadata.getTableName());
            TableDescription deletedTableDescription = getDynamoDBClient().deleteTable(deleteTableRequest).getTableDescription();
            log.debug("Deleted Table: " + deletedTableDescription);
            // Wait for it to become active
            waitForTableToBeDeleted(metadata.getTableName());
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteTable(DynamoTableMetadata).", ex);
        }
    }

    /**
     * Method to delete a table from dynamo db based on the table name.
     * <p/>
     * @param tablename Table name
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata
     */
    public static void deleteTable(String tablename) {
        log.debug("deleteTable(String)");
        try {
            DeleteTableRequest deleteTableRequest =
                    new DeleteTableRequest().withTableName(tablename);
            TableDescription deletedTableDescription = getDynamoDBClient().deleteTable(deleteTableRequest).getTableDescription();
            log.debug("Deleted Table: " + deletedTableDescription);
            // Wait for it to become active
            waitForTableToBeDeleted(tablename);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteTable(String).", ex);
        }
    }

    /**
     * Method to halt a thread to wait until the creation of the table on Amazon is confirmed.
     * <p/>
     * @param tableName Table name to be verified.
     */
    private static void waitForTableToBecomeAvailable(String tableName) {
        log.info("Waiting for " + tableName + " to become ACTIVE...");
        System.out.println("Waiting for " + tableName + " to become ACTIVE...");
        long startTime = System.currentTimeMillis();
        long endTime = startTime + (10 * 60 * 1000);
        while (System.currentTimeMillis() < endTime) {
            try {
                Thread.sleep(1000 * 20);
            } catch (Exception e) {
            }
            try {
                DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
                TableDescription tableDescription = getDynamoDBClient().describeTable(request).getTable();
                String tableStatus = tableDescription.getTableStatus();
                log.info("  - current state: " + tableStatus);
                if (tableStatus.equals(TableStatus.ACTIVE.toString())) {
                    return;
                }
            } catch (AmazonServiceException ase) {
                if (ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException") == false) {
                    throw ase;
                }
            }
        }

        throw new RuntimeException("Table " + tableName + " never went active");
    }

    /**
     * Method to halt a thread to wait until the deletion of the table on Amazon is confirmed.
     * <p/>
     * @param tableName Table name to be deleted.
     */
    private static void waitForTableToBeDeleted(String tableName) {
        log.info("Waiting for " + tableName + " to be DELETED...");

        long startTime = System.currentTimeMillis();
        long endTime = startTime + (10 * 60 * 1000);
        while (System.currentTimeMillis() < endTime) {
            try {
                Thread.sleep(1000 * 20);
            } catch (Exception e) {
            }
            try {
                DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
                TableDescription tableDescription = getDynamoDBClient().describeTable(request).getTable();
                String tableStatus = tableDescription.getTableStatus();
                log.info("  - current state: " + tableStatus);
                if (tableStatus.equals(TableStatus.DELETING.toString())) {
                    return;
                }
            } catch (AmazonServiceException ase) {
                if (ase.getErrorCode().equalsIgnoreCase("ResourceNotFoundException") == false) {
                    throw ase;
                } else if (ase.getErrorCode().equalsIgnoreCase("ResourceInUseException") == true) {
                    log.error("Cannot delete the table {" + tableName + "} while is in use.");
                    throw ase;
                }
            }
        }

        throw new RuntimeException("Table " + tableName + " never was deleted");
    }

    /**
     * Method to validate the existence of a given table on AmazonDynamoDB
     * <p/>
     * @param tableName Table to be validated
     * @return true if the table exists.
     */
    public static boolean tableExist(String tableName) {
        DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
        try {
            TableDescription tableDescription = getDynamoDBClient().describeTable(request).getTable();
            String tableStatus = tableDescription.getTableStatus();
            if (tableStatus.equals(TableStatus.ACTIVE.toString())) {
                return true;
            }
        } catch (ResourceNotFoundException e) {
            log.error("The Resource was not found. {table='" + tableName + "'}");
            return false;
        }
        return false;
    }

    /**
     * Method to validate the existence of a given table on AmazonDynamoDB
     * <p/>
     * @param clazz Class containing the Annotation from where we extract the table name.
     * @return true if the table exists.
     */
    public static boolean tableExistForClass(Class clazz) throws DynamoDBAnnotationException {
        log.debug("tableExistForClass(Class)");
        DynamoTableMetadata metadata = null;
        try {
            metadata = DynamoAnnotationParser.parse(clazz);
            DescribeTableRequest request = new DescribeTableRequest().withTableName(metadata.getTableName());
            TableDescription tableDescription = getDynamoDBClient().describeTable(request).getTable();
            String tableStatus = tableDescription.getTableStatus();
            if (tableStatus.equals(TableStatus.ACTIVE.toString())) {
                return true;
            }
        } catch (DynamoDBAnnotationException ex) {
            log.error("The class needs to be marked with the Annotation: " + AmazonDynamoDB.class);
            throw ex;
        } catch (ResourceNotFoundException e) {
            log.error("The Resource was not found. {table='" + metadata.getTableName() + "'}");
            return false;
        }
        return false;
    }
}
