/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowingInterestCollection;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class FollowingInterestCollectionAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(FollowingInterestCollectionAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(FollowingInterestCollectionAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<FollowingInterestCollection> getFollowingInterestCollectionByCollectionId(String interestCollectionId) throws Exception {
        log.info("getFollowinInterestCollectionByCollectionId - interestCollectionId: " + interestCollectionId);
        List<FollowingInterestCollection> returnList = new ArrayList<FollowingInterestCollection>();

        Object cacheObj = null;
        try {
            cacheObj = cache.get(interestCollectionId);
            if (cacheObj != null) {
                return (List<FollowingInterestCollection>) cacheObj;
            } else {
                PaginatedQueryList<FollowingInterestCollection> paginatedQueryList = getDynamoDBMapper().query(
                        FollowingInterestCollection.class, new DynamoDBQueryExpression(new AttributeValue().withS(interestCollectionId)));

                for (FollowingInterestCollection n : paginatedQueryList) {
                    if (n != null) {
                        returnList.add(n);
                    }
                }
                cache.set(interestCollectionId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getFollowinInterestCollectionByCollectionId - " + interestCollectionId + " - " + ex.getMessage());
            throw ex;
        }

        return returnList;
    }

    public static int getFollowingInterestCollectionCount(String interestCollectionId) throws Exception {
        log.info("getFollowingInterestCollectionCount - interestCollectionId: " + interestCollectionId);
        List<FollowingInterestCollection> followingInterestCollectionList = new ArrayList<FollowingInterestCollection>();
        Object cacheObj = null;
        int count = 0;
        try {
            cacheObj = cache.get(interestCollectionId);
            if (cacheObj != null) {
                followingInterestCollectionList = (List<FollowingInterestCollection>) cacheObj;

            } else {
                followingInterestCollectionList = getFollowingInterestCollectionByCollectionId(interestCollectionId);

            }
            count = followingInterestCollectionList.size();
        } catch (Exception ex) {
            log.error("Exception in getFollowingInterestCollectionCount - interestCollectionId: " + interestCollectionId + " - " + ex.getMessage());
            throw ex;

        }

        return count;
    }
    
        public static void saveFollowingInterestCollection(FollowingInterestCollection followingInterestCollection) throws Exception {
        log.info("saveFollowingInterestCollection - FollowingInterestCollection");
        try {
            getDynamoDBMapper().save(followingInterestCollection);
            
            cache.delete(followingInterestCollection.getInterestCollectionId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveFollowingInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }

    public static void deleteFollowingInterestCollection(FollowingInterestCollection followingInterestCollection) throws Exception {
        log.info("deleteFollowingInterestCollection - FollowingInterestCollection");
        try {
            getDynamoDBMapper().delete(followingInterestCollection);
            cache.delete(followingInterestCollection.getInterestCollectionId());
        } catch (Exception ex) {
            log.error("Exception in deleteFollowingInterestCollection: " + ex.getMessage());
            throw ex;
        }

    }
}
