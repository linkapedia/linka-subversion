package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshalling;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers.StatusEnumMarshaller;
import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers.StringArrayMarshaller;
import com.intellisophic.linkapedia.api.beans.enums.Status;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Xander Kno
 */
@AmazonDynamoDB(tableName = "Taxonomy", hashKey = "ID", hashKeyType = "S")
@DynamoDBTable( tableName = "Taxonomy")
public class Taxonomy implements Serializable{

    private String ID;
    private String name;
    private String description;
    private Float ROCF1;
    private Float ROCF2;
    private Float ROCF3;
    private Float ROCC1;
    private Float ROCC2;
    private Float ROCC3;
    private boolean active;
    private Integer runFrecuency;
    private Date lastRun;
    private Date lastSync;
    private Integer ROCSettingID;
    private boolean affinity;
    private String[] terms;
    //Aditional fields
    private Status status;
    private String rootNodeID;

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setROCF1(Float ROCF1) {
        this.ROCF1 = ROCF1;
    }

    public void setROCF2(Float ROCF2) {
        this.ROCF2 = ROCF2;
    }

    public void setROCF3(Float ROCF3) {
        this.ROCF3 = ROCF3;
    }

    public void setROCC1(Float ROCC1) {
        this.ROCC1 = ROCC1;
    }

    public void setROCC2(Float ROCC2) {
        this.ROCC2 = ROCC2;
    }

    public void setROCC3(Float ROCC3) {
        this.ROCC3 = ROCC3;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setRunFrecuency(Integer runFrecuency) {
        this.runFrecuency = runFrecuency;
    }

    public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public void setROCSettingID(Integer ROCSettingID) {
        this.ROCSettingID = ROCSettingID;
    }

    public void setAffinity(boolean affinity) {
        this.affinity = affinity;
    }

    public void setTerms(String[] terms) {
        this.terms = terms;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setRootNodeID(String rootNodeID) {
        this.rootNodeID = rootNodeID;
    }

    //Getters
    @DynamoDBHashKey(attributeName = "ID")
    public String getID() {
        return ID;
    }

    @DynamoDBAttribute(attributeName = "name")
    public String getName() {
        return name;
    }

    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() {
        return description;
    }

    @DynamoDBAttribute(attributeName = "ROCF1")
    public Float getROCF1() {
        return ROCF1;
    }

    @DynamoDBAttribute(attributeName = "ROCF2")
    public Float getROCF2() {
        return ROCF2;
    }

    @DynamoDBAttribute(attributeName = "ROCF3")
    public Float getROCF3() {
        return ROCF3;
    }

    @DynamoDBAttribute(attributeName = "ROCC1")
    public Float getROCC1() {
        return ROCC1;
    }

    @DynamoDBAttribute(attributeName = "ROCC2")
    public Float getROCC2() {
        return ROCC2;
    }

    @DynamoDBAttribute(attributeName = "ROCC3")
    public Float getROCC3() {
        return ROCC3;
    }

    @DynamoDBAttribute(attributeName = "active")
    public boolean isActive() {
        return active;
    }

    @DynamoDBAttribute(attributeName = "runFrequency")
    public Integer getRunFrecuency() {
        return runFrecuency;
    }

    @DynamoDBAttribute(attributeName = "lastRun")
    public Date getLastRun() {
        return lastRun;
    }

    @DynamoDBAttribute(attributeName = "lastSync")
    public Date getLastSync() {
        return lastSync;
    }

    @DynamoDBAttribute(attributeName = "ROCSettingID")
    public Integer getROCSettingID() {
        return ROCSettingID;
    }

    @DynamoDBAttribute(attributeName = "affinity")
    public boolean isAffinity() {
        return affinity;
    }

    @DynamoDBAttribute(attributeName = "terms")
    @DynamoDBMarshalling(marshallerClass = StringArrayMarshaller.class)
    public String[] getTerms() {
        return terms;
    }

    @DynamoDBAttribute(attributeName = "status")
    @DynamoDBMarshalling(marshallerClass = StatusEnumMarshaller.class)
    public Status getStatus() {
        return status;
    }

    @DynamoDBAttribute(attributeName = "rootNodeID")
    public String getRootNodeID() {
        return rootNodeID;
    }

    @Override
    public String toString() {
        return "Taxonomy{" + "ID=" + ID + ", name=" + name + ", description=" + description + ", active=" + active + ", status=" + status + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.ID != null ? this.ID.hashCode() : 0);
        hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 47 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 47 * hash + (this.active ? 1 : 0);
        hash = 47 * hash + (this.status != null ? this.status.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Taxonomy other = (Taxonomy) obj;
        if (this.ID != other.ID && (this.ID == null || !this.ID.equals(other.ID))) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if (this.active != other.active) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        return true;
    }
}