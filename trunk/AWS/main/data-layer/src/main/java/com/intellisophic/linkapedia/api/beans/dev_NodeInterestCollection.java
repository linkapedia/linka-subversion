/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "dev_NodeInterestCollection", hashKey = "nodeId", hashKeyType = "S", rangeKey = "interestCollectionId", rangeKeyType = "S")
@DynamoDBTable(tableName = "dev_NodeInterestCollection")
public class dev_NodeInterestCollection implements Serializable {

    private String nodeId;
    private String interestCollectionId;
    private Float score;

    @DynamoDBRangeKey(attributeName = "nodeId")
    public String getNodeId() {
        return nodeId;
    }

    @DynamoDBHashKey(attributeName = "interestCollectionId")
    public String getInterestCollectionId() {
        return interestCollectionId;
    }

    @DynamoDBAttribute(attributeName = "score")
    public Float getScore() {
        return score;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public void setInterestCollectionId(String interestCollectionId) {
        this.interestCollectionId = interestCollectionId;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
