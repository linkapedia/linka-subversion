package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshalling;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers.PropertiesMarshaller;
import java.io.Serializable;
import java.util.Properties;

/**
 *
 * @author andres
 */
@AmazonDynamoDB(tableName = "UserFeed", hashKey = "userId", hashKeyType = "S", rangeKey = "timeStamp", rangeKeyType = "N")
@DynamoDBTable(tableName = "UserFeed")
public class UserFeed implements Serializable {

    private String userId;
    private String userName;
    private Long timeStamp;
    private Properties data; //neccesary data to the event

    @DynamoDBHashKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @DynamoDBRangeKey(attributeName = "userName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @DynamoDBRangeKey(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timestamp) {
        this.timeStamp = timestamp;
    }

    @DynamoDBAttribute(attributeName = "data")
    @DynamoDBMarshalling(marshallerClass = PropertiesMarshaller.class)
    public Properties getData() {
        return data;
    }

    public void setData(Properties data) {
        this.data = data;
    }
   
}
