package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.QueryRequest;
import com.amazonaws.services.dynamodb.model.QueryResult;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.FollowedUser;
import com.intellisophic.linkapedia.api.beans.FollowingUser;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.InterestCollectionElement;
import com.intellisophic.linkapedia.api.beans.InterestCollectionFollowed;
import com.intellisophic.linkapedia.api.beans.User;
import com.intellisophic.linkapedia.api.beans.UserFeed;
import com.intellisophic.linkapedia.api.beans.UserNode;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 * Esta clase esta una peye. favor cambiar el estilo para guardar los eventos
 *
 * @author andres
 */
public class UserFeedAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = Logger.getLogger(UserFeedAPIHandler.class);
    //***********************************
    // EVENTS TYPES
    //***********************************
    public static final int FOLLOW = 1;
    public static final int UNFOLLOW = 2;
    public static final int FOLLOWED = 3;
    public static final int UNFOLLOWED = 4;
    public static final int JOIN = 5;
    public static final int UNJOIN = 6;
    public static final int REGISTER = 7;
    public static final int CREATE_COLLECTION = 8;
    public static final int ADD_INTEREST_COLLECTION = 9;
    public static final int ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION = 10;
    public static final int MAKE_COLLECTION_PUBLIC = 11;
    public static final int FOLLOW_COLLECTION = 12;
    //***********************************
    // FIELDS
    //***********************************
    private String userId;
    private String userIdToShow;
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(UserFeedAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;
    private static User user = null;

    /**
     * unique constructor (the event is associate to one userId)
     *
     * @param userId
     */
    public UserFeedAPIHandler(String userId) {
        this.userId = userId;
        this.userIdToShow = null;
        Initialize(userId);

    }

    private void Initialize(String userId) {
        try {
            user = DataHandlerFacade.getUser(userId);
            if (user == null) {
                log.error("Error registerEvent() -> getting the user");

            }
        } catch (Exception ex) {
            log.error("Error registerEvent() -> getting the user", ex);

        }
    }

    public void setUserToShow(String userIdToShow) {
        this.userIdToShow = userIdToShow;
    }

    /**
     * register the event (save in UserFeed table)
     *
     * @param obj bean to save in data
     * @param type is the action for this event: use UserFeedAPIHandler.{field}
     * @return
     */
    public boolean registerEvent(Object obj, int type) throws ClassNotFoundException {
        if (userId == null) {
            log.error("Error registerEvent() -> userId can't be null");
            return false;
        }
        Properties prop = null;
        if (obj instanceof FollowingUser) {
            FollowingUser data = (FollowingUser) obj;
            prop = parsingFollowing(data);
        } else if (obj instanceof FollowedUser) {
            FollowedUser data = (FollowedUser) obj;
            prop = parsingFollowed(data);
        } else if (obj instanceof UserNode) {
            UserNode data = (UserNode) obj;
            prop = parsingUserNode(data);
        } else if (obj instanceof User) {
            User data = (User) obj;
            prop = parsingUser(data);
        } else if (obj instanceof InterestCollection) {
            InterestCollection data = (InterestCollection) obj;
            prop = parsingInterestCollection(data);
        } else if (obj instanceof InterestCollectionElement) {
            InterestCollectionElement data = (InterestCollectionElement) obj;
            prop = parsingInterestCollectionElement(data);
        } else if (obj instanceof InterestCollectionFollowed) {
            InterestCollectionFollowed data = (InterestCollectionFollowed) obj;
            prop = parsingInterestCollectionFollowed(data);
        } else {
            log.info("registerEvent(): obj not supported");
            prop = new Properties();
        }





        //save the user info
        UserFeed userFeed = new UserFeed();
        if (this.userIdToShow != null) {
            userFeed.setUserId(userIdToShow);
        } else {
            userFeed.setUserId(userId);
        }
        userFeed.setUserName(user.getFirstName());
        userFeed.setTimeStamp((Long) prop.get("timeStamp"));

        //set type in the data
        if (type == FOLLOW) {
            prop.put("TYPE", "FOLLOW");
        } else if (type == UNFOLLOW) {
            prop.put("TYPE", "UNFOLLOW");
        } else if (type == FOLLOWED) {
            prop.put("TYPE", "FOLLOWED");
        } else if (type == UNFOLLOWED) {
            prop.put("TYPE", "UNFOLLOWED");
        } else if (type == JOIN) {
            prop.put("TYPE", "JOIN");
        } else if (type == UNJOIN) {
            prop.put("TYPE", "UNJOIN");
        } else if (type == REGISTER) {
            prop.put("TYPE", "REGISTER");
        } else if (type == CREATE_COLLECTION) {
            prop.put("TYPE", "CREATE_COLLECTION");
        } else if (type == ADD_INTEREST_COLLECTION) {
            prop.put("TYPE", "ADD_INTEREST_COLLECTION");
        } else if (type == ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION) {
            prop.put("TYPE", "ADD_DIGEST_IN_FOLLOWED_TOPIC_TO_INTEREST_COLLECTION");
        } else if (type == MAKE_COLLECTION_PUBLIC) {
            prop.put("TYPE", "MAKE_COLLECTION_PUBLIC");
        } else if (type == FOLLOW_COLLECTION) {
            prop.put("TYPE", "FOLLOW_COLLECTION");
        } else {
            log.info("registerEvent(): type not supported");
            prop.put("TYPE", "NULL");
        }
        userFeed.setData(prop);
        //save the event
        return save(userFeed);
    }

    /**
     *
     * @param followingUser
     * @return
     */
    private Properties parsingFollowing(FollowingUser followingUser) {
        Properties prop = new Properties();
        Field fields[] = followingUser.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(followingUser));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param followedUser
     * @return
     */
    private Properties parsingFollowed(FollowedUser followedUser) {
        Properties prop = new Properties();
        Field fields[] = followedUser.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(followedUser));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param followedUser
     * @return
     */
    private Properties parsingUserNode(UserNode userNode) {
        Properties prop = new Properties();
        Field fields[] = userNode.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(userNode));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param User
     * @return
     */
    private Properties parsingUser(User user) {
        Properties prop = new Properties();
        Field fields[] = user.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(user));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param InterestCollection
     * @return
     */
    private Properties parsingInterestCollection(InterestCollection interestCollection) {
        Properties prop = new Properties();
        Field fields[] = interestCollection.getClass().getDeclaredFields();
        interestCollection.setInterestCollectionDescription("");
        interestCollection.setInterestCollectionElements(null);
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(interestCollection));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param InterestCollectionList
     * @return
     */
    private Properties parsingInterestCollectionElement(InterestCollectionElement interestCollectionElement) {
        Properties prop = new Properties();
        Field fields[] = interestCollectionElement.getClass().getDeclaredFields();
        interestCollectionElement.setDigestText("");
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(interestCollectionElement));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     *
     * @param InterestCollectionFollowed
     * @return
     */
    private Properties parsingInterestCollectionFollowed(InterestCollectionFollowed interestCollectionFollowed) {
        Properties prop = new Properties();
        Field fields[] = interestCollectionFollowed.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(interestCollectionFollowed));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     * Experiment! generic parser Save the bean in a properties OBJECT to save
     * in data field
     *
     * @param className
     * @param obj
     * @return
     * @throws ClassNotFoundException
     */
    private Properties parser(String className, Object obj) throws ClassNotFoundException {
        Properties prop = new Properties();
        Class cl = Class.forName(className);
        Field fields[] = cl.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            try {
                prop.put(fields[i].getName(), fields[i].get(obj));
            } catch (IllegalArgumentException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (IllegalAccessException ex) {
                log.error("parsingFollowing: error reflection", ex);
            } catch (NullPointerException e) {
                log.info("parsingFollowing: data not found");
            }
        }
        return prop;
    }

    /**
     * save the feed from the user
     *
     * @param userFeed
     * @return
     */
    private boolean save(UserFeed userFeed) {
        log.info("save - UserFeed");
        try {
            getDynamoDBMapper().save(userFeed);
            if (userIdToShow!=null){
             cache.delete(userIdToShow); //delete from cache to ensure the data consistence   
            }else{
            cache.delete(userId); //delete from cache to ensure the data consistence
            }
            
        } catch (Exception ex) {
            log.error("Exception in UserFeedAPIHandler method save(): " + ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * get feeds from the user
     *
     * @param userId
     * @return
     */
    public static List<UserFeed> getFeeds(String userId) {
        log.info("getFeeds - userId: " + userId);
        List<UserFeed> userFeeds = new ArrayList<UserFeed>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                return (List<UserFeed>) cacheObj;
            } else {
                DynamoDBQueryExpression exp = new DynamoDBQueryExpression(new AttributeValue(userId));
                exp.setScanIndexForward(false);
                PaginatedQueryList<UserFeed> result = getDynamoDBMapper().query(
                        UserFeed.class, exp);
                for (UserFeed userFeed : result) {
                    if (userFeed != null) {
                        userFeeds.add(userFeed);
                    }
                }
                cache.set(userId, userFeeds, ONE_HOUR);
            }
        } catch (Exception e) {
            log.error("Error ingetFeeds", e);
        }
        return userFeeds;
    }

    /**
     * Experiment! TODO: test this method
     *
     * @param userId //get the rows by this parameter
     * @param startIn //start in this position to get items
     * @param limit //number the result to get
     * @param scanIndexForward //true: order asc, false: order desc. (range key)
     * @return
     */
    public static List<UserFeed> getFeeds(String userId, Key startIn, int limit, boolean scanIndexForward) {
        log.info("getFeeds - userId: " + userId);
        List<UserFeed> userFeeds = new ArrayList<UserFeed>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(userId);
            if (cacheObj != null) {
                return (List<UserFeed>) cacheObj;
            } else {
                QueryRequest queryRequest = new QueryRequest()
                        .withTableName("")
                        .withScanIndexForward(Boolean.FALSE)
                        .withHashKeyValue(
                        new AttributeValue().withS(userId))
                        .withLimit(10).withExclusiveStartKey(new Key(new AttributeValue(userId)));
                QueryResult result = DynamoDBMapperAbs.getDynamoDBClient().query(queryRequest);

                UserFeed userF;
                for (Map<String, AttributeValue> item : result.getItems()) {
                    userF = new UserFeed();
                    userF.setUserId(item.get("userId").getS());
                    userF.setUserName(item.get("userName").getS());
                    userF.setTimeStamp(Long.parseLong(item.get("timeStamp").getN()));

                    //get properties from json
                    JSONObject jsonObject = JSONObject.fromObject(item.get("data").getS());
                    Properties myMap = (Properties) JSONObject.toBean(jsonObject, Properties.class);
                    userF.setData(myMap);
                    userFeeds.add(userF);
                }
                cache.set(userId, userFeeds, ONE_HOUR);
            }
        } catch (Exception e) {
            log.error("Error ingetFeeds", e);
        }
        return userFeeds;
    }
}
