/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodb.model.AttributeAction;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodb.model.ComparisonOperator;
import com.amazonaws.services.dynamodb.model.Condition;
import com.amazonaws.services.dynamodb.model.Key;
import com.amazonaws.services.dynamodb.model.UpdateItemRequest;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.NodeDocument;
import com.intellisophic.linkapedia.api.beans.NodeDocumentTemp;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class NodeDocumentAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(NodeDocumentAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(NodeDocumentAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<NodeDocument> getNodeDocumentsByNodeId(String nodeId) throws Exception {
        log.info("getNodeDocumentsByNodeId - " + nodeId);
        List<NodeDocument> returnList = new ArrayList<NodeDocument>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(nodeId);
            if (cacheObj != null) {
                return (List<NodeDocument>) cacheObj;
            } else {

                PaginatedQueryList<NodeDocument> paginatedQueryList = getDynamoDBMapper().query(
                        NodeDocument.class, new DynamoDBQueryExpression(new AttributeValue().withS(nodeId)));

                for (NodeDocument doc : paginatedQueryList) {
                    if (doc != null) {
                        returnList.add(doc);
                    }
                }
                cache.set(nodeId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getNodeDocumentsByNodeId - " + nodeId + " - " + ex.getMessage());
            throw ex;
        }

        return returnList;

    }

    public static NodeDocument getNodeDocument(String nodeId, String docId) throws Exception {
        log.info("getNodeDocument - " + nodeId + " " + docId);
        log.debug("getNode(Long)");
        Object cacheObj = null;
        NodeDocument nodeDocument = null;
        
        if (nodeId == null || docId == null) {
            throw new IllegalArgumentException("some parameters can't be null");
        }
        try {
            cacheObj = cache.get(nodeId + docId);
            if (cacheObj != null) {
                return (NodeDocument) cacheObj;
            } else {
                if (nodeDocument != null) {
                    return nodeDocument;
                } else {
                    nodeDocument = getDynamoDBMapper().load(NodeDocument.class, nodeId, docId);
                    if (nodeDocument != null) {
                        nodeDocument.setModified(false);
                        cache.set(nodeId + docId, nodeDocument, ONE_HOUR);
                    } else {
                        log.warn("No node found for docId {" + docId + "}");
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Exception in getNodeDocument for nodeId, docId- " + nodeId + " , " + docId + " - " + ex.getMessage());
            throw ex;
        }
        return nodeDocument;
    }

    /**
     * Find NodeDocuments With Score01= 0 Author: Sahar Ebadi
     *
     * @param
     * @return returnList
     */
    public static List<NodeDocument> FindNodeDocumentsWithScoreOneValueEqualToZero() throws Exception {
        log.info("FindNodeDocumentsWithScoreOneValueEqualToZero - ");
        List<NodeDocument> returnList = new ArrayList<NodeDocument>();
        Object cacheObj = null;
        try {
            Condition scanFilterCondition = new Condition()
                    .withComparisonOperator(ComparisonOperator.EQ)
                    .withAttributeValueList(new AttributeValue().withN("0"));
//            Condition scanFilterCondition2 = new Condition()
//                    .withComparisonOperator(ComparisonOperator.EQ)
//                    .withAttributeValueList(new AttributeValue().withN("0"));
            Map<String, Condition> conditions = new HashMap<String, Condition>();

            conditions.put("score01", scanFilterCondition);
            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
            scanExpression.addFilterCondition("score01", scanFilterCondition);
            PaginatedScanList<NodeDocument> result = getDynamoDBMapper().scan(NodeDocument.class, scanExpression);
            for (NodeDocument doc : result) {
                if (doc != null) {
                    returnList.add(doc);
                }
            }
        } catch (Exception ex) {
            log.error("Exception in FindNodeDocumentsWithScoreOneValueEqualToZero - " + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    /**
     * Find NodeDocuments With No timeStamp Author: Sahar Ebadi
     *
     * @param
     * @return returnList
     */
    public static List<NodeDocument> FindNodeDocumentsWithNoTimeStamp() throws Exception {
        log.info("FindNodeDocumentsWithNoTimeStamp - ");
        List<NodeDocument> returnList = new ArrayList<NodeDocument>();
        try {
            Condition scanFilterCondition = new Condition()
                    .withComparisonOperator(ComparisonOperator.NULL);
            // for test purpose, to retrive an smaller chunck of data
//            Condition scanFilterCondition2 = new Condition()
//                    .withComparisonOperator(ComparisonOperator.EQ)
//                    .withAttributeValueList(new AttributeValue().withS("80064581"));
            Map<String, Condition> conditions = new HashMap<String, Condition>();

            conditions.put("timeStamp", scanFilterCondition);
//            conditions.put("nodeID", scanFilterCondition2);

            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
            scanExpression.addFilterCondition("timeStamp", scanFilterCondition);
//            scanExpression.addFilterCondition("nodeID", scanFilterCondition2);
            PaginatedScanList<NodeDocument> result = getDynamoDBMapper().scan(NodeDocument.class, scanExpression);
            if (!result.isEmpty()) {
                log.info(" number of NodeDocuments retrived: " + result.size());
                for (NodeDocument nod : result) {
                    if (nod != null) {
                        returnList.add(nod);
                    }
                }
            } else {
                log.info("No data found for this Scan with assigned conditions. In FindNodeDocumentsWithNoTimeStamp() - ");
            }
        } catch (Exception ex) {
            log.error("Exception in FindNodeDocumentsWithNoTimeStamp - " + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    /**
     * update NodeDocuments With A Default(01/01/2013) TimeStamp Author: Sahar
     * Ebadi
     *
     * @param
     * @return returnList
     */
    public static void updateNodeDocumenttNodeWithATimeStamp(List<NodeDocument> nodes, long defaultDate) throws Exception {
        log.info("updateNodeDocumenttNodeWithATimeStamp - ");
//        List<NodeDocument> updateList = new ArrayList<NodeDocument>();
        try {
            for (int i = 0; i < nodes.size(); i++) {
                if (nodes.get(i).getTimeStamp() == 0) {
                    nodes.get(i).setTimeStamp(defaultDate);
                    getDynamoDBMapper().save(nodes.get(i));
                    log.info("upgrade node: " + nodes.get(i).toString());
                }
            }
        } catch (Exception ex) {
            log.error("Exception in FindNodeDocumentsWithNoTimeStamp - " + " - " + ex.getMessage());
            throw ex;
        }
    }

    public static void saveNodeDocument(NodeDocument node) {
        //TODO

        getDynamoDBMapper().save(node);

    }

    public static void saveNodeDocumentTemp(NodeDocumentTemp node) {
        //TODO

        getDynamoDBMapper().save(node);

    }

    /**
     * delete NodeDocuments (used to delete NodeDocuments With Score01= 0)
     * Author: Sahar Ebadi
     *
     * @param nodeDocument
     * @return
     */
    public static void deleteNodeDocument(NodeDocument nodeDocument) {
        //TODO cache
        getDynamoDBMapper().delete(nodeDocument);

    }
}
