package com.intellisophic.linkapedia.amazon.services.dynamodb.annotations;

import com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata;
import com.intellisophic.linkapedia.amazon.services.dynamodb.exceptions.DynamoDBAnnotationException;
import org.apache.log4j.Logger;

/**
 * Class to handle the AmazonDynamoDB annotation.
 * <p/>
 * @see com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB
 * @author Xander Kno
 * @version 1.0
 */
public class DynamoAnnotationParser {

    private static final Logger log = Logger.getLogger(DynamoAnnotationParser.class);

    /**
     * Method to convert the AmazonDynamoDB annotation to DynamoTableMetadata.
     * <p/>
     * @param clazz Class to extract the annotation info from
     * @return Metadata object containing all the required info to work in
     * dynamo db.
     * @throws Exception
     * @see com.intellisophic.linkapedia.amazon.services.dynamodb.DynamoTableMetadata
     */
    public static DynamoTableMetadata parse(Class clazz) throws DynamoDBAnnotationException {
        log.debug("parse(Class)");
        DynamoTableMetadata metadata = new DynamoTableMetadata();
        if (clazz.isAnnotationPresent(AmazonDynamoDB.class)) {
            AmazonDynamoDB annotation = (AmazonDynamoDB) clazz.getAnnotation(AmazonDynamoDB.class);
            metadata.setTableName(annotation.tableName());
            metadata.setHashKey(annotation.hashKey());
            metadata.setHashKeyType(annotation.hashKeyType());
            metadata.setRangeKey(annotation.rangeKey());
            metadata.setRangeKeyType(annotation.rangeKeyType());
            metadata.setReadCapacity(annotation.readCapacity());
            metadata.setWriteCapacity(annotation.writeCapacity());
        } else {
            log.warn("No annotation found for class {" + clazz.getName() + "}");
            throw new DynamoDBAnnotationException("No annotation found for class {" + clazz.getName() + "}");
        }
        return metadata;
    }
}
