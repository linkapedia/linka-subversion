package com.intellisophic.linkapedia.amazon.services.dynamodb;

/**
 *
 * @author Xander Kno
 */
public class DynamoTableMetadata {

    public static final String STRINGHASHKEYTYPE = "S";
    public static final String NUMERICHASHKEYTYPE = "N";
    private String tableName;
    private String hashKey;
    private String hashKeyType;
    private String rangeKey;
    private String rangeKeyType;
    private long readCapacity;
    private long writeCapacity;

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public void setHashKeyType(String hashKeyType) {
        this.hashKeyType = hashKeyType;
    }

    public void setRangeKey(String rangeKey) {
        this.rangeKey = rangeKey;
    }

    public void setRangeKeyType(String rangeKeyType) {
        this.rangeKeyType = rangeKeyType;
    }

    public void setReadCapacity(long readCapacity) {
        this.readCapacity = readCapacity;
    }

    public void setWriteCapacity(long writeCapacity) {
        this.writeCapacity = writeCapacity;
    }

    public String getTableName() {
        return tableName;
    }

    public String getHashKey() {
        return hashKey;
    }

    public String getHashKeyType() {
        return hashKeyType;
    }

    public String getRangeKey() {
        return rangeKey;
    }

    public String getRangeKeyType() {
        return rangeKeyType;
    }

    public long getReadCapacity() {
        return readCapacity;
    }

    public long getWriteCapacity() {
        return writeCapacity;
    }

    public boolean hasRangeKey() {
        if (rangeKey != null && !rangeKey.isEmpty() && rangeKeyType != null && !rangeKeyType.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "DynamoTableMetadata{" + "tableName=" + tableName + ", hashKey=" + hashKey + ", hashKeyType=" + hashKeyType + ", rangeKey=" + rangeKey + ", rangeKeyType=" + rangeKeyType + ", readCapacity=" + readCapacity + ", writeCapacity=" + writeCapacity + '}';
    }
}
