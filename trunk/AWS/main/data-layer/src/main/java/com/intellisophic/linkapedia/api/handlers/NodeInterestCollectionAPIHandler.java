/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.InterestCollection;
import com.intellisophic.linkapedia.api.beans.NodeInterestCollection;
import com.intellisophic.linkapedia.generic.services.datasource.DataHandlerFacade;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author juanidrobo
 */
public class NodeInterestCollectionAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = Logger.getLogger(NodeInterestCollection.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(NodeInterestCollection.class.getName());
    private static final int ONE_HOUR = 3600;

    public static void saveNodeInterestCollection(NodeInterestCollection nodeInterestCollection) throws Exception {
        log.debug("saveNodeInterestCollection (NodeInterestCollection)");
        try {
            getDynamoDBMapper().save(nodeInterestCollection);
            cache.delete(nodeInterestCollection.getNodeId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveNodeInterestCollection: " + ex.getMessage());
            throw ex;
        }

    }
      
  public static List<InterestCollection> getInterestCollectionByNodeId(String nodeId) throws Exception {
        log.debug("getNodeInterestCollection (String)");
        List<InterestCollection> returnList = new ArrayList<InterestCollection>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(nodeId);
            if (cacheObj != null) {
                return (List<InterestCollection>) cacheObj;
            } else {
                PaginatedQueryList<NodeInterestCollection> result = getDynamoDBMapper().query(
                        NodeInterestCollection.class, new DynamoDBQueryExpression(new AttributeValue(nodeId)));

                InterestCollection ic=null;
                for (NodeInterestCollection nodeInterestCollection : result) {
                    if (nodeInterestCollection != null) {
                        ic=DataHandlerFacade.getInterestCollection(nodeInterestCollection.getInterestCollectionId());
                        returnList.add(ic);
                    }
                }
                cache.set(nodeId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getNodeInterestCollection - nodeId: " + nodeId + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    
        public static void removeNodeInterestCollection(NodeInterestCollection nodeInterestCollection) throws Exception {
        log.debug("removeNodeInterestCollection (NodeInterestCollection)");
        try {
            getDynamoDBMapper().delete(nodeInterestCollection);
            cache.delete(nodeInterestCollection.getNodeId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in removeNodeInterestCollection: " + ex.getMessage());
            throw ex;
        }

    }
        
          public static void updateNodeInterestCollection(String nodeId) throws Exception {
        log.info("updateNodeInterestCollection - String");
        try {         
            cache.delete(nodeId); //delete from cache because the interestColection has been updated
        } catch (Exception ex) {
            log.error("Exception in updateNodeInterestCollection: " + ex.getMessage());
            throw ex;
        }
    }
}
