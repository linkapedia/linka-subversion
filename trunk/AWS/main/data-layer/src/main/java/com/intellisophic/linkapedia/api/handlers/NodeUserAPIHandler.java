/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.NodeUser;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class NodeUserAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(NodeUserAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(NodeUserAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<NodeUser> getNodeUsersByNodeId(String nodeId) throws Exception {
        log.info("getNodeUsersByNodeId - nodeId: " + nodeId);

        List<NodeUser> returnList = new ArrayList<NodeUser>();

        Object cacheObj = null;
        try {
            cacheObj = cache.get(nodeId);
            if (cacheObj != null) {
                return (List<NodeUser>) cacheObj;
            } else {
                PaginatedQueryList<NodeUser> paginatedQueryList = getDynamoDBMapper().query(
                        NodeUser.class, new DynamoDBQueryExpression(new AttributeValue().withS(nodeId)));

                for (NodeUser n : paginatedQueryList) {
                    if (n != null) {
                        returnList.add(n);
                    }
                }
                cache.set(nodeId, returnList, ONE_HOUR);

            }
        } catch (Exception ex) {
            log.error("Exception in getNodeUsersByNodeId - " + nodeId + " - " + ex.getMessage());
            throw ex;
        }

        return returnList;
    }

    public static int getNodeUsersCount(String nodeId) throws Exception {

        log.info("getNodeUsersCount - nodeId: " + nodeId);

        List<NodeUser> nodeUserList = new ArrayList<NodeUser>();
        Object cacheObj = null;
        int count;

        try {
            cacheObj = cache.get(nodeId);
            if (cacheObj != null) {
                nodeUserList = (List<NodeUser>) cacheObj;

            } else {
                nodeUserList = getNodeUsersByNodeId(nodeId);
            }
            count = nodeUserList.size();
        } catch (Exception ex) {
            log.error("Exception in getNodeUsersCount - nodeId: " + nodeId + " - " + ex.getMessage());
            throw ex;
        }

        return count;
    }

    public static void saveNodeUser(NodeUser nodeUser) throws Exception {
        log.info("saveNodeUser - NodeUser");
        try {
            getDynamoDBMapper().save(nodeUser);
            
            cache.delete(nodeUser.getNodeId()); //delete from cache to ensure the data consistence
        } catch (Exception ex) {
            log.error("Exception in saveNodeUser: " + ex.getMessage());
            throw ex;
        }
    }

    public static void deleteNodeUser(NodeUser nodeUser) throws Exception {
        log.info("deleteNodeUser - NodeUser");
        try {
            getDynamoDBMapper().delete(nodeUser);
            cache.delete(nodeUser.getNodeId());
        } catch (Exception ex) {
            log.error("Exception in deleteNodeUser: " + ex.getMessage());
            throw ex;
        }

    }
}
