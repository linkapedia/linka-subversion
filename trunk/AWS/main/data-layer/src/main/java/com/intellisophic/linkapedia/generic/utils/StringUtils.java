package com.intellisophic.linkapedia.generic.utils;

import java.net.URLEncoder;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class StringUtils {

    private static final Logger log = Logger.getLogger(StringUtils.class);

    public static String rCharTrim(String src, String character) {
        if (src != null && character != null && !src.isEmpty() && !character.isEmpty()) {
            StringBuilder sb = new StringBuilder(src);
            if (sb.lastIndexOf(character) == sb.length() - character.length()) {
                return sb.substring(0, sb.lastIndexOf(character));
            } else {
                return sb.toString();
            }
        }
        return null;
    }

    public static String rCharTrim(StringBuilder src, String character) {
        if (src != null && character != null && src.length() > 0 && !character.isEmpty()) {
            if (src.lastIndexOf(character) == src.length() - character.length()) {
                return src.substring(0, src.lastIndexOf(character));
            } else {
                return src.toString();
            }
        }
        return null;
    }

    /**
     * Method to encode the given URL to UTF-8
     *
     * @param URL URL to be encoded
     * @return
     */
    public static String urlEncode(String URL) {
        String encodedURL = null;
        try {
            encodedURL = URLEncoder.encode(URL, "UTF-8");
        } catch (Exception e) {
            log.error("An exception ocurred while encoding the URL.", e);
        }
        return encodedURL;
    }

    /**
     * Method to create a string reverting the order of the characters of the given string
     * <b>Example: </b> Given the string 12345L this function will return L54321.
     * <p/>
     * @param string
     * @return string inverted, in this case L54321.
     */
    public static String revert(String string) {
        if (string == null) {
            return null;
        }
        StringBuilder inverseStr = new StringBuilder(string);
        return inverseStr.reverse().toString();
    }
}
