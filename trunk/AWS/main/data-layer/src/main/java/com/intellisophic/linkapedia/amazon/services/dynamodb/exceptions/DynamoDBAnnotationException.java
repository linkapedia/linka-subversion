package com.intellisophic.linkapedia.amazon.services.dynamodb.exceptions;

/**
 *
 * @author Xander Kno
 */
public class DynamoDBAnnotationException extends Exception {

    /**
     * Creates a new instance of
     * <code>DynamoDBAnnotationException</code> without detail message.
     */
    public DynamoDBAnnotationException() {
    }

    /**
     * Constructs an instance of
     * <code>DynamoDBAnnotationException</code> with the specified detail message.
     * <p/>
     * @param msg the detail message.
     */
    public DynamoDBAnnotationException(String msg) {
        super(msg);
    }
}
