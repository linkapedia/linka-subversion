package com.intellisophic.linkapedia.api.handlers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.NodeLink;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author juanidrobo
 */
public class NodeLinkAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(NodeLinkAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(NodeLinkAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    public static List<NodeLink> getNodeLinksByNodeStaticLinkId(String nodeStaticLinkId) throws Exception {
        log.info("getNodeLinksByNodeStaticLinkId - " + nodeStaticLinkId);
        List<NodeLink> listReturn = new ArrayList<NodeLink>();
        Object cacheObj = null;
        try {
            cacheObj = cache.get(nodeStaticLinkId);
            if (cacheObj != null) {
                return (List<NodeLink>) cacheObj;
            } else {
                PaginatedQueryList<NodeLink> result = getDynamoDBMapper().query(
                        NodeLink.class, new DynamoDBQueryExpression(new AttributeValue(nodeStaticLinkId)));

                for (NodeLink nodeLink : result) {
                    if (nodeLink != null) {
                        listReturn.add(nodeLink);
                    }
                }
                cache.set(nodeStaticLinkId, listReturn, ONE_HOUR);
            }
        } catch (Exception ex) {
            log.error("Exception in getNodeLinksByNodeStaticLinkId - " + nodeStaticLinkId + " - " + ex.getMessage());
            throw ex;
        }

        return listReturn;
    }

    public static void saveNodeLink(NodeLink nodeLink) {
        log.debug("saveNodeLink(NodeLink)");
        if (nodeLink == null) {
            throw new IllegalArgumentException("The nodeLink to be saved can not be null");
        }

        getDynamoDBMapper().save(nodeLink);


    }
}
