package com.intellisophic.linkapedia.amazon.services.dynamodb.helpers.marshallers;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBMarshaller;
import com.intellisophic.linkapedia.api.beans.enums.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Xander Kno
 */
public class StatusEnumMarshaller implements DynamoDBMarshaller<Status> {

    private static final Logger log = LoggerFactory.getLogger(StatusEnumMarshaller.class);

    @Override
    public String marshall(Status status) {
        log.debug("marshall(Status)");
        return status.getStatus();
    }

    @Override
    public Status unmarshall(Class<Status> clazz, String obj) {
        log.debug("unmarshall(Class<Status>, String)");
        for (Status status : Status.values()) {
            if (status.getStatus().equals(obj)) {
                return status;
            }
        }
        log.warn("Status {" + obj + "} not found. Returning default value.");
        //Return default status.
        return Status.HIDDEN;
    }
}
