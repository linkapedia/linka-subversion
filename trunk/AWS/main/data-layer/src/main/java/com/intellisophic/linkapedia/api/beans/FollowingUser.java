
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;

/**
 *
 * @author juanidrobo
 */
@AmazonDynamoDB(tableName = "FollowingUser", hashKey = "followingUserId", hashKeyType = "S", rangeKey = "followedUserId", rangeKeyType = "S")
@DynamoDBTable(tableName = "FollowingUser")
public class FollowingUser implements Serializable {

    /**
     * unique/key/followingUserId
     */
    private String followingUserId;
    private String followedUserId;
    private String followedUserName;
    private String followedUserLastName;
    private String followedUserFbId;
    private Long timeStamp;

    @DynamoDBHashKey(attributeName = "followingUserId")
    public String getFollowingUserId() {
        return followingUserId;
    }

    @DynamoDBRangeKey(attributeName = "followedUserId")
    public String getFollowedUserId() {
        return followedUserId;
    }

    @DynamoDBAttribute(attributeName = "followedUserName")
    public String getFollowedUserName() {
        return followedUserName;
    }

    @DynamoDBAttribute(attributeName = "followedUserLastName")
    public String getFollowedUserLastName() {
        return followedUserLastName;
    }

    @DynamoDBAttribute(attributeName = "followedUserFbId")
    public String getFollowedUserFbId() {
        return followedUserFbId;
    }

    @DynamoDBAttribute(attributeName = "timeStamp")
    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setFollowedUserFbId(String followedUserFbId) {
        this.followedUserFbId = followedUserFbId;
    }

    public void setFollowingUserId(String followingUserId) {
        this.followingUserId = followingUserId;
    }

    public void setFollowedUserId(String followedUserId) {
        this.followedUserId = followedUserId;
    }

    public void setFollowedUserName(String followedUserName) {
        this.followedUserName = followedUserName;
    }

    public void setFollowedUserLastName(String followedUserLastName) {
        this.followedUserLastName = followedUserLastName;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
