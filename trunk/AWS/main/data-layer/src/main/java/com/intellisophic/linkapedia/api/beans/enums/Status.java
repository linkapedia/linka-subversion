package com.intellisophic.linkapedia.api.beans.enums;

/**
 *
 * @author Xander Kno
 */
public enum Status {

    QA("QA"), PRODUCTION("Production"), HIDDEN("Hidden");
    private String status;

    private Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Status{" + "status=" + status + '}';
    }
}
