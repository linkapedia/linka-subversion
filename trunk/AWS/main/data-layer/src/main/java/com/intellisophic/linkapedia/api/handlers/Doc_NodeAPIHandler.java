package com.intellisophic.linkapedia.api.handlers;

/**
 *
 * @author Sahar Ebadi
 *
 */
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodb.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodb.model.AttributeValue;
import com.amazonaws.services.dynamodb.model.ComparisonOperator;
import com.amazonaws.services.dynamodb.model.Condition;
import com.intellisophic.linkapedia.amazon.services.dynamodb.config.DynamoDBMapperAbs;
import com.intellisophic.linkapedia.api.beans.DocNodeWrapper;
import com.intellisophic.linkapedia.api.beans.Doc_Node;
import com.intellisophic.linkapedia.api.beans.Doc_NodeTemp;
import com.intellisophic.linkapedia.generic.services.xcache.CacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ConcreteCacheCreator;
import com.intellisophic.linkapedia.generic.services.xcache.ICache;
import com.intellisophic.linkapedia.generic.utils.DynamoReadUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Doc_NodeAPIHandler extends DynamoDBMapperAbs {

    private static final Logger log = LoggerFactory.getLogger(Doc_NodeAPIHandler.class);
    private static CacheCreator cacheCreator = new ConcreteCacheCreator();
    private static ICache cache = cacheCreator.create(Doc_NodeAPIHandler.class.getName());
    private static final int ONE_HOUR = 3600;

    /**
     * not store in cache
     *
     * @param docId
     * @return
     * @throws Exception
     */
    public static Doc_Node getDoc_NodesByDocumentId(String docId) throws Exception {
        log.info("getDoc_NodesByDocumentId - " + docId);
        Doc_Node doc_nodeResult = null;
        try {
            doc_nodeResult = getDynamoDBMapper().load(Doc_Node.class, docId);
        } catch (Exception ex) {
            log.error("Exception in getDoc_NodesByDocumentId - " + docId + " - " + ex.getMessage());
            throw ex;
        }
        return doc_nodeResult;
    }
    
    
        public static Doc_NodeTemp getDoc_NodesByDocumentIdTemp(String docId) throws Exception {
        log.info("getDoc_NodesByDocumentId - " + docId);
        Doc_NodeTemp doc_nodeResult = null;
        try {
            doc_nodeResult = getDynamoDBMapper().load(Doc_NodeTemp.class, docId);
        } catch (Exception ex) {
            log.error("Exception in getDoc_NodesByDocumentId - " + docId + " - " + ex.getMessage());
            throw ex;
        }
        return doc_nodeResult;
    }

    /**
     * store in cache use this in the builder
     *
     * @param docId
     * @return
     * @throws Exception
     */
    public static DocNodeWrapper getDoc_NodesByDocumentIdForBuilder(String docId) throws Exception {
        log.info("getDoc_NodesByDocumentId - " + docId);
        Doc_Node doc_nodeResult = null;
        DocNodeWrapper docNWrapper = null;
        Object cacheObj = null;
        try {
            cacheObj = cache.get(docId);
            if (cacheObj != null) {
                return (DocNodeWrapper) cacheObj;
            } else {
                doc_nodeResult = getDynamoDBMapper().load(Doc_Node.class, docId);
                if (doc_nodeResult != null) {
                    docNWrapper = new DocNodeWrapper();
                    docNWrapper.setDocId(doc_nodeResult.getDocId());
                    docNWrapper.setData(DynamoReadUtils.decompressDocNodeInfo(doc_nodeResult.getNodeList()));
                    cache.set(docId, docNWrapper, ONE_HOUR);
                } else {
                    log.warn("Doc_Node not found: " + docId);
                }
            }
        } catch (Exception ex) {
            log.error("Exception in getDoc_NodesByDocumentId - " + docId + " - " + ex.getMessage());
            throw ex;
        }
        return docNWrapper;
    }

    /**
     * Find Doc_Nodes With Score01= 0 Author: Sahar Ebadi
     *
     * @param
     * @return returnList
     */
    public static List<Doc_Node> FindDoc_NodesWithScoreOneValueEqualToZero() throws Exception {
        log.info("FindDoc_NodesWithScoreOneValueEqualToZero - ");
        List<Doc_Node> returnList = new ArrayList<Doc_Node>();
        try {
            Condition scanFilterCondition = new Condition()
                    .withComparisonOperator(ComparisonOperator.EQ)
                    .withAttributeValueList(new AttributeValue().withN("0"));
            Map<String, Condition> conditions = new HashMap<String, Condition>();
            conditions.put("score01", scanFilterCondition);
            DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
            scanExpression.addFilterCondition("score01", scanFilterCondition);
            PaginatedScanList<Doc_Node> result = getDynamoDBMapper().scan(Doc_Node.class, scanExpression);
            for (Doc_Node doc : result) {
                if (doc != null) {
                    returnList.add(doc);
                }
            }
        } catch (Exception ex) {
            log.error("Exception in FindDoc_NodesWithScoreOneValueEqualToZero - " + " - " + ex.getMessage());
            throw ex;
        }
        return returnList;
    }

    public static void saveDoc_Node(Doc_Node doc_Node) {
        //TODO cache
        try {
            getDynamoDBMapper().save(doc_Node);
        } catch (Exception e) {
            log.error("Exception in saveDoc_Node - " + " - " + e.getMessage());
        }

    }
    
    
        public static void saveDoc_NodeTemp(Doc_NodeTemp doc_Node) {
        //TODO cache
        try {
            getDynamoDBMapper().save(doc_Node);
        } catch (Exception e) {
            log.error("Exception in saveDoc_Node - " + " - " + e.getMessage());
        }

    }

    /**
     * delete Doc_Node (used to delete Doc_Nodes With Score01= 0) Author: Sahar
     * Ebadi
     *
     * @param doc_Node
     * @return
     */
    public static void deleteDoc_Node(Doc_Node doc_Node) {
        //TODO cache
        getDynamoDBMapper().delete(doc_Node);
    }
}
