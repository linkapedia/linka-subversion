package com.intellisophic.linkapedia.amazon.services.sqs.config;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.intellisophic.linkapedia.amazon.services.security.credentials.CredentialHandler;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public abstract class AmazonSQSClientAbs extends CredentialHandler {

    private static final Logger log = Logger.getLogger(AmazonSQSClientAbs.class);
    /*
     * Important: Be sure to fill in your AWS access credentials in the
     * AwsCredentials.properties file before you try to run this.
     * http://aws.amazon.com/security-credentials
     */
    private static AmazonSQSClient sqsClient;

    /**
     * The only information needed to create a client are security credentials
     * consisting of the AWS Access Key ID and Secret Access Key. All other
     * configuration, such as the service endpoints, are performed
     * automatically. Client parameters, such as proxies, can be specified in an
     * optional ClientConfiguration object when constructing a client.
     *
     * @see com.amazonaws.auth.BasicAWSCredentials
     * @see com.amazonaws.auth.PropertiesCredentials
     * @see com.amazonaws.ClientConfiguration
     * @see com.amazonaws.services.dynamodb.datamodeling.DynamoDBMapper
     */
    static {
        try {
            sqsClient = new AmazonSQSClient(getCredentials(), getConfiguration());
        } catch (Exception e) {
            log.error("An exception has ocurred while initializing Dynamo DB Client.", e);
        }
    }

    protected static AmazonSQSClient getAmazonSQSClient() {
        return sqsClient;
    }
}
