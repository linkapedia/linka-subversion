package com.intellisophic.linkapedia.generic.services.datasource;

import com.intellisophic.linkapedia.api.beans.*;
import com.intellisophic.linkapedia.api.handlers.DocumentNodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.FollowedUserAPIHandler;
import com.intellisophic.linkapedia.api.handlers.FollowingInterestCollectionAPIHandler;
import com.intellisophic.linkapedia.api.handlers.FollowingUserAPIHandler;
import com.intellisophic.linkapedia.api.handlers.InterestCollectionAPIHandler;
import com.intellisophic.linkapedia.api.handlers.InterestCollectionFollowedAPIHandler;
import com.intellisophic.linkapedia.api.handlers.InterestCollectionElementAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeDocumentAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeInterestCollectionAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeLinkAPIHandler;
import com.intellisophic.linkapedia.api.handlers.NodeUserAPIHandler;
import com.intellisophic.linkapedia.api.handlers.TaxonomyAPIHandler;
import com.intellisophic.linkapedia.api.handlers.UserAPIHandler;
import com.intellisophic.linkapedia.api.handlers.UserFbAPIHandler;
import com.intellisophic.linkapedia.api.handlers.UserFeedAPIHandler;
import com.intellisophic.linkapedia.api.handlers.UserNodeAPIHandler;
import com.intellisophic.linkapedia.api.handlers.Doc_NodeAPIHandler;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Xander Kno
 */
public class DataHandlerFacade {

    private static final Logger log = Logger.getLogger(DataHandlerFacade.class);

    //<editor-fold defaultstate="collapsed" desc="Node Functions">
    /**
     * Method to save and object in AmazonDynamoDB
     * <p/>
     * The object will be created if it does not exist otherwise it will be
     * updated.
     * <p/>
     * @param node {@link NodeStatic} object to be saved.
     * @throws Exception
     */
    public static void saveNode(NodeStatic node) throws Exception {
        log.debug("saveNode(NodeStatic)");
        try {

            NodeAPIHandler.saveNode(node);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the NodeStatic Object on DynamoDB.");
            throw e;
        }
    }

    /**
     * Method to retrieve a node form AmazonDynamoDB.
     * <p/>
     * @param nodeID node identifier.
     * @return {@link NodeStatic} for the given ID.
     */
    public static NodeStatic getNode(String nodeID) throws Exception {
        log.debug("getNode(String)");
        NodeStatic node = null;
        try {
            node = NodeAPIHandler.getNode(nodeID);
        } catch (Exception e) {
            log.error("An exception has ocurred while retriving the Node " + nodeID);
            throw e;
        }
        return node;
    }

    /**
     * Method used to delete a node from AmazonDynamoDB.
     * <p/>
     * @param node {@link NodeStatic} to be deleted.
     */
    public static void deleteNode(NodeStatic node) throws Exception {
        log.debug("deleteNode(NodeStatic)");
        try {
            NodeAPIHandler.deleteNode(node);
        } catch (Exception e) {
            log.error("An exception has ocurred on deleteNode(NodeStatic).");
            throw e;
        }
    }

    /**
     * Method to return a list of nodes represented by the list of id's.
     * <p/>
     * @param ids Array of node id's to be found.
     * @return List of full objects that represents the nodes id's.
     */
    public static List<NodeStatic> getNodesByNodeIDs(String[] ids) {
        log.debug("getNodesByNodeID(String[])");
        List<NodeStatic> nodeList = new ArrayList<NodeStatic>();
        for (int i = 0; i < ids.length; i++) {
            String nodeID = ids[i];
            try {
                nodeList.add(getNode(nodeID));
            } catch (NumberFormatException exception) {
                log.error("An exception has ocurred while parsing the nodeid", exception);
            } catch (Exception exception) {
                log.error("An exception has ocurred while retrieving the node.", exception);
            }
        }
        return nodeList;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Doc_Node Functions">
    /**
     * get all nodes related with a document
     * <p/>
     * @param docId
     * @return
     */
    public static Doc_Node getDoc_NodesByDocumentId(String docId) {
        log.debug("getNodesByDocumentId(String)");
        Doc_Node listReturn = null;
        if (docId == null || docId.isEmpty()) {
            log.error("Error. id = null or is is Empty.");
            return null;
        }
        try {
            listReturn = Doc_NodeAPIHandler.getDoc_NodesByDocumentId(docId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");
        }
        return listReturn;
    }

    public static DocNodeWrapper getDoc_NodesByDocumentIdForBuilder(String docId) {
        log.debug("getNodesByDocumentId(String)");
        DocNodeWrapper listReturn = null;
        if (docId == null || docId.isEmpty()) {
            log.error("Error. id = null or is is Empty.");
            return null;
        }
        try {
            listReturn = Doc_NodeAPIHandler.getDoc_NodesByDocumentIdForBuilder(docId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");
        }
        return listReturn;
    }

    /**
     * Scan and find nodes with score01 = 0e
     * <p/>
     * @param @return
     */
    public static List<Doc_Node> FindDoc_NodesWithScoreOneValueEqualToZero() {
        log.debug("FindDoc_NodesWithScoreOneValueEqualToZero()");
        List<Doc_Node> listReturn = null;
        try {
            listReturn = Doc_NodeAPIHandler.FindDoc_NodesWithScoreOneValueEqualToZero();
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");

        }
        return listReturn;
    }

    /**
     * delete Doc_Node object (that with score01 = 0, found by
     * FindDoc_NodesWithScoreOneValueEqualToZero)
     * <p/>
     * @param doc_Node
     * @return
     */
    public static void deleteDoc_Node(Doc_Node doc_Node) throws Exception {
        log.debug("delete doc_Node");
        try {
            Doc_NodeAPIHandler.deleteDoc_Node(doc_Node);
        } catch (Exception e) {
            log.error("An exception has ocurred while trying to delete the doc_Node object from DynamoDB.");
            throw e;
        }
    }

    /**
     * Method to save and object in AmazonDynamoDB
     * <p/>
     * The object will be created if it does not exist otherwise it will be
     * updated.
     * <p/>
     * @param node node object to be saved.
     * @throws Exception
     */
    public static void saveDoc_Node(Doc_Node doc_Node) {
        log.debug("saveDoc_Node(Doc_Node)");
        try {
            Doc_NodeAPIHandler.saveDoc_Node(doc_Node);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the saveDoc_Node.");

        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="DocumentNode Functions">

    /**
     * get all nodes related with a document
     * <p/>
     * @param docId
     * @return
     */
    public static List<DocumentNode> getDocumentNodesByDocumentId(String docId) {

        log.debug("getNodesByDocumentId(String)");
        List<DocumentNode> listReturn = null;
        if (docId == null || docId.isEmpty()) {
            log.error("Error. id = null or is is Empty.");
            return null;
        }
        try {
            listReturn = DocumentNodeAPIHandler.getDocumentNodesByDocumentId(docId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");

        }
        return listReturn;
    }

    /**
     * Scan and find nodes with score01 = 0e
     * <p/>
     * @param @return
     */
    public static List<DocumentNode> FindDocumentNodesWithScoreOneValueEqualToZero() {
        log.debug("FindDocumentNodesWithScoreOneValueEqualToZero()");
        List<DocumentNode> listReturn = null;
        try {
            listReturn = DocumentNodeAPIHandler.FindDocumentNodesWithScoreOneValueEqualToZero();
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");

        }
        return listReturn;
    }

    /**
     * Scan and find all the items
     * <p/>
     * @param @return
     */
    public static List<DocumentNode> ScanDocumentNode() {
        log.debug("Scan all the documents in ScanDocumentNode()");
        List<DocumentNode> listReturn = null;
        try {
            listReturn = DocumentNodeAPIHandler.ScanDocumentNode();
        } catch (Exception e) {
            log.error("An exception has ocurred while ScanDocumentNode()");

        }
        return listReturn;
    }

    /**
     * delete DocumentNode object (that with score01 = 0, found by
     * FindDocumentNodesWithScoreOneValueEqualToZero)
     * <p/>
     * @param documentNode
     * @return
     */
    public static void deleteDocumentNode(DocumentNode documentNode) throws Exception {
        log.debug("delete documentNode");
        try {
            DocumentNodeAPIHandler.deleteDocumentNode(documentNode);
        } catch (Exception e) {
            log.error("An exception has ocurred while trying to delete the documentNode object from DynamoDB.");
            throw e;
        }
    }

    /**
     * get one document specific in the query
     *
     * @param nodeId
     * @param docId
     * @return
     */
    public static DocumentNode getDocumentNode(String nodeId, String docId) {
        log.debug("getDocumentNode(String, String)");
        DocumentNode documentNode = null;
        if (docId == null || docId.isEmpty()) {
            log.error("Error. docId = null or is Empty.");
            return null;
        }
        if (nodeId == null || nodeId.isEmpty()) {
            log.error("Error. nodeId = null or is Empty.");
            return null;
        }
        try {
            documentNode = DocumentNodeAPIHandler.getDocumentNode(nodeId, docId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.", e);

        }
        return documentNode;
    }

    /**
     * Method to save and object in AmazonDynamoDB
     * <p/>
     * The object will be created if it does not exist otherwise it will be
     * updated.
     * <p/>
     * @param node node object to be saved.
     * @throws Exception
     */
    public static void saveDocumentNode(DocumentNode documentNode) {
        log.debug("saveDocumentNode(DocumentNode)");

        try {
            DocumentNodeAPIHandler.saveDocumentNode(documentNode);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the saveDocumentNode.");

        }

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="NodeDocument Functions">
    /**
     * get all documents related with a node
     * <p/>
     * @param id nodeId
     * @return List<NodeDocument>
     */
    public static List<NodeDocument> getNodeDocumentsByNodeId(String nodeId) {
        log.debug("getDocumentsByNodeId(String)");
        List<NodeDocument> listReturn = null;
        if (nodeId == null || nodeId.isEmpty()) {
            log.error("Error. id = null or is is Empty.");
            return null;
        }
        try {
            listReturn = NodeDocumentAPIHandler.getNodeDocumentsByNodeId(nodeId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Documents By NodeId.");

        }

        return listReturn;
    }

    /**
     * get one document specific in the query
     *
     * @param nodeId
     * @param docId
     * @return
     */
    public static NodeDocument getNodeDocument(String nodeId, String docId) {
        log.debug("getNodeDocument(String, String)");
        NodeDocument nodeDocument = null;
        if (docId == null || docId.isEmpty()) {
            log.error("Error. docId = null or is Empty.");
            return null;
        }
        if (nodeId == null || nodeId.isEmpty()) {
            log.error("Error. nodeId = null or is Empty.");
            return null;
        }
        try {
            nodeDocument = NodeDocumentAPIHandler.getNodeDocument(nodeId, docId);
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.", e);

        }
        return nodeDocument;
    }

    /**
     * Scan and find nodes with score01 = 0 author: Sahar Ebadi
     *
     * @param
     * @return
     */
    public static List<NodeDocument> FindNodeDocumentsWithScoreOneValueEqualToZero() {
        log.debug("FindNodeDocumentsWithScoreOneValueEqualToZero()");
        List<NodeDocument> listReturn = null;
        try {
            listReturn = NodeDocumentAPIHandler.FindNodeDocumentsWithScoreOneValueEqualToZero();
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");

        }
        return listReturn;
    }

    /**
     * Scan and find nodes with score01 = 0 author: Sahar Ebadi
     *
     * @param
     * @return
     */
    public static List<NodeDocument> FindNodeDocumentsWithNoTimeStamp() {
        log.debug("FindNodeDocumentsWithNoTimeStamp()");
        List<NodeDocument> listReturn = null;
        try {
            listReturn = NodeDocumentAPIHandler.FindNodeDocumentsWithNoTimeStamp();
        } catch (Exception e) {
            log.error("An exception has ocurred while getting Nodes By DocumentId.");

        }
        return listReturn;
    }

    /**
     * delete NodeDocument object (found by
     * FindNodeDocumentsWithScoreOneValueEqualToZero)
     * <p/>
     * author: Sahar Ebadi
     *
     * @param nodeDocument
     * @return
     */
    public static void deleteNodeDocument(NodeDocument nodeDocument) throws Exception {
        log.debug("delete nodeDocument");
        try {
            NodeDocumentAPIHandler.deleteNodeDocument(nodeDocument);
        } catch (Exception e) {
            log.error("An exception has ocurred while trying to delete the nodeDocument object from DynamoDB.");
            throw e;
        }
    }

    /**
     * update NodeDocument object (found by FindNodeDocumentsWithNoTimeStamp)
     * with a default date
     * <p/>
     * author: Sahar Ebadi
     *
     * @param nodeDocument
     * @return
     */
    public static void updateNodeDocumenttNodeWithATimeStamp(List<NodeDocument> nodeDocument, long defaultDate) throws Exception {
        log.info("updateNodeDocumenttNodeWithATimeStamp");
        try {
            NodeDocumentAPIHandler.updateNodeDocumenttNodeWithATimeStamp(nodeDocument, defaultDate);
        } catch (Exception ex) {
            log.error("An exception has ocurred on updateNodeDocumenttNodeWithATimeStamp.");
            throw ex;
        }

    }

    /**
     * Method to save and object in AmazonDynamoDB
     * <p/>
     * The object will be created if it does not exist otherwise it will be
     * updated.
     * <p/>
     * @param node node object to be saved.
     * @throws Exception
     */
    public static void saveNodeDocument(NodeDocument node) {
        log.debug("saveNodeDocument(NodeDocument)");

        try {
            NodeDocumentAPIHandler.saveNodeDocument(node);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the NodeDocument.");

        }

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Taxonomy Functions">
    public static void saveTaxonomy(Taxonomy taxonomy) throws Exception {
        log.debug("saveTaxonomy(Taxonomy)");
        try {
            TaxonomyAPIHandler.saveTaxonomy(taxonomy);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the Taxonomy Object.");
            throw e;
        }
    }

    /**
     *
     * @param corpusID
     * @return
     * @throws Exception
     */
    public static Taxonomy getTaxonomy(String corpusID) throws Exception {
        log.debug("getTaxonomy(Long)");
        Taxonomy taxonomy = null;
        try {
            taxonomy = TaxonomyAPIHandler.getTaxonomy(corpusID);
        } catch (Exception e) {
            log.error("An exception has ocurred while retriving the Taxonomy Object on DynamoDB.");
            throw e;
        }
        return taxonomy;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User Functions">

    public static void saveUser(User user) throws Exception {
        log.debug("saveUser(User)");
        try {
            UserAPIHandler.saveUser(user);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the User Object on DynamoDB.");
            throw e;
        }
    }

    public static void updateUser(User user) throws Exception {
        log.debug("updateUser(User,Update)");
        try {
            UserAPIHandler.updateUser(user);
        } catch (Exception e) {
            log.error("An exception has ocurred while updating the User Object on DynamoDB.");
            throw e;
        }
    }

    public static User getUser(String userId) throws Exception {
        log.debug("getUser(String)");
        User user = null;
        try {
            user = UserAPIHandler.getUser(userId);
        } catch (Exception e) {
            log.error("An exception has ocurred while retriving the User Object from DynamoDB.");
            throw e;
        }
        return user;
    }

    public static void deleteUser(User user) throws Exception {
        log.debug("deleteUser(User)");
        try {
            UserAPIHandler.deleteUser(user);
        } catch (Exception e) {
            log.error("An exception has ocurred while trying to delete the User object from DynamoDB.");
            throw e;
        }
    }

    public static void deleteCacheUser(User user) {
        if (user == null) {
            return;
        }
        UserAPIHandler.deleteCacheUser(user);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="UserFb Functions">

    public static void saveUserFb(UserFb userFb) throws Exception {
        log.debug("saveUserFb (UserFb)");
        try {
            UserFbAPIHandler.saveUserFb(userFb);
        } catch (Exception e) {
            log.error("An exception has ocurred while saving the UserFb Object on DynamoDB.");
            throw e;
        }
    }

    public static UserFb getUserFb(String fbId) throws Exception {
        log.debug("getUserFb(String)");
        UserFb userFb = null;
        try {
            userFb = UserFbAPIHandler.getUserFb(fbId);
        } catch (Exception e) {
            log.error("An exception has ocurred while retriving the UserFb Object from DynamoDB.");
            throw e;
        }
        return userFb;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="NodeLink Functions">

    public static List<NodeLink> getNodeLinksByNodeStaticLinkId(String nodeStaticLinkId) {
        log.debug("getNodeLinksByNodeStaticLinkId (String)");
        List<NodeLink> nodeLinks = null;
        try {
            nodeLinks = NodeLinkAPIHandler.getNodeLinksByNodeStaticLinkId(nodeStaticLinkId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getNodeLinksByNodeStaticLinkId.", ex);
        }
        return nodeLinks;

    }

    public static void saveNodeLink(NodeLink nodeLink) {
        log.debug("saveNodeLink (NodeLink)");
        // TODO Add logic to validate if we really need to save the object.
        try {
            NodeLinkAPIHandler.saveNodeLink(nodeLink);
        } catch (Exception ex) {
            log.error("An exception has ocurred while saving the NodeLink.");
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="NodeUser Functions">

    public static List<NodeUser> getNodeUsersByNodeId(String nodeId) throws Exception {
        log.info("getNodeUsersByNodeId");
        List<NodeUser> nodeUsers = null;
        try {
            nodeUsers = NodeUserAPIHandler.getNodeUsersByNodeId(nodeId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getNodeUsersByNodeId.");
            throw ex;
        }
        return nodeUsers;
    }

    public static int getNodeUsersCount(String nodeId) throws Exception {
        log.info("getNodeUsersCount");
        int count; //initial value
        try {
            count = NodeUserAPIHandler.getNodeUsersCount(nodeId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getNodeUsersCount.");
            throw ex;
        }
        return count;
    }

    public static void saveNodeUser(NodeUser nodeUser) throws Exception {
        log.info("saveNodeUser");
        try {
            NodeUserAPIHandler.saveNodeUser(nodeUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveNodeUser.");
            throw ex;
        }
    }

    public static void deleteNodeUser(NodeUser nodeUser) throws Exception {
        log.info("deleteNodeUser");
        try {
            NodeUserAPIHandler.deleteNodeUser(nodeUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteNodeUser.");
            throw ex;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="UserNode Functions">
    public static List<UserNode> getUserNodesByUserId(String userId) throws Exception {
        log.info("getUserNodesByUserId");
        List<UserNode> userNodes = null;
        try {
            userNodes = UserNodeAPIHandler.getUserNodesByUserId(userId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getUserNodesByUserId.");
            throw ex;
        }
        return userNodes;
    }

    public static UserNode getUserNodesByUserIdAndNodeId(String userId, String nodeId) throws Exception {
        log.info("getUserNodesByUserId");
        UserNode userNode = null;
        try {
            userNode = UserNodeAPIHandler.getUserNodesByUserIdAndNodeId(userId, nodeId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getUserNodesByUserId.");
            throw ex;
        }
        return userNode;
    }

    public static int getUserNodesCount(String userId) throws Exception {
        log.info("getUserNodesCount");
        int count; //initial value
        try {
            count = UserNodeAPIHandler.getUserNodesCount(userId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getUserNodesCount.");
            throw ex;
        }
        return count;
    }

    public static void saveUserNode(UserNode userNode) throws Exception {
        log.info("saveUserNode");
        try {
            UserNodeAPIHandler.saveUserNode(userNode);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveUserNode.");
            throw ex;
        }
    }

    public static void deleteUserNode(UserNode userNode) throws Exception {
        log.info("deleteUserNode");
        try {
            UserNodeAPIHandler.deleteUserNode(userNode);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteUserNode.");
            throw ex;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FollowedUser Functions">

    public static List<FollowedUser> getFollowerUsersByFollowedUserId(String followedUserId) throws Exception {
        log.info("getFollowerUsersByFollowedUserId");
        List<FollowedUser> followedUsers = null;
        try {
            followedUsers = FollowedUserAPIHandler.getFollowerUsersByFollowedUserId(followedUserId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowerUsersByFollowedUserId.");
            throw ex;
        }
        return followedUsers;
    }

    public static int getFollowersCount(String followedUserId) throws Exception {
        log.info("getFollowersCount");
        int count; //initial value
        try {
            count = FollowedUserAPIHandler.getFollowersCount(followedUserId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowersCount.");
            throw ex;
        }
        return count;
    }

    public static void saveFollowedUser(FollowedUser followedUser) throws Exception {
        log.info("saveFollowedUser");
        try {
            FollowedUserAPIHandler.saveFollowedUser(followedUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveFollowedUser.");
            throw ex;
        }
    }

    public static void deleteFollowedUser(FollowedUser followedUser) throws Exception {
        log.info("deleteFollowedUser");
        try {
            FollowedUserAPIHandler.deleteFollowedUser(followedUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteFollowedUser.");
            throw ex;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="FollowingUser Functions">

    public static List<FollowingUser> getFollowingUsersByFollowerUserId(String followerUserId) throws Exception {
        log.info("getFollowingUsersByFollowerUserId");
        List<FollowingUser> followingUsers = null;
        try {
            followingUsers = FollowingUserAPIHandler.getFollowingUsersByFollowerUserId(followerUserId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowingUsersByFollowerUserId.");
            throw ex;
        }
        return followingUsers;
    }

    public static int getFollowingsCount(String followerUserId) throws Exception {
        log.info("getFollowingsCount");
        int count;
        try {
            count = FollowingUserAPIHandler.getFollowingsCount(followerUserId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowingsCount.");
            throw ex;
        }
        return count;
    }

    public static void saveFollowingUser(FollowingUser followingUser) throws Exception {
        log.info("saveFollowingUser");
        try {
            FollowingUserAPIHandler.saveFollowingUser(followingUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveFollowingUser.");
            throw ex;
        }
    }

    public static void deleteFollowingUser(FollowingUser followingUser) throws Exception {
        log.info("deleteFollowingUser");
        try {
            FollowingUserAPIHandler.deleteFollowingUser(followingUser);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteFollowingUser.");
            throw ex;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="UserFeed Functions">

    public static List<UserFeed> getUserFeed(String userID) throws Exception {
        log.info("getFollowingUsersByFollowerUserId");
        List<UserFeed> feeds = null;
        try {

            feeds = UserFeedAPIHandler.getFeeds(userID);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowingUsersByFollowerUserId.");
            throw ex;
        }
        return feeds;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="InterestCollection Functions">
    public static void saveInterestCollection(InterestCollection interestCollection) throws Exception {
        log.info("saveInterestCollection");
        try {
            InterestCollectionAPIHandler.saveInterestCollection(interestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveInterestCollection.");
            throw ex;
        }

    }

    public static InterestCollection getInterestCollection(String interestCollectionId) throws Exception {
        log.info("getInterestCollection");
        return getInterestCollection(interestCollectionId, false);


    }

    public static InterestCollection getInterestCollection(String interestCollectionId, boolean retrieveElements) throws Exception {
        log.info("getInterestCollection");
        InterestCollection interestCollection = null;
        try {
            interestCollection = InterestCollectionAPIHandler.getInterestCollection(interestCollectionId, retrieveElements);

        } catch (Exception ex) {
            log.error("An exception has ocurred on saveInterestCollection.");
            throw ex;
        }
        return interestCollection;

    }

    public static void setPublicInterestCollection(InterestCollection interestCollection) throws Exception {
        log.info("saveInterestCollection");
        try {
            InterestCollectionAPIHandler.setPublicInterestCollection(interestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on setPublicInterestCollection.");
            throw ex;
        }

    }

    public static void updateInterestCollection(InterestCollection interestCollection) throws Exception {
        log.info("updateInterestCollection");
        try {
            InterestCollectionAPIHandler.updateInterestCollection(interestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on updateInterestCollection.");
            throw ex;
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="InterestCollectionList Functions">
    public static void addInterestCollection(InterestCollectionElement interestCollectionElement) throws Exception {
        log.info("addInterestCollection");
        try {
            InterestCollectionElementAPIHandler.addInterestCollection(interestCollectionElement);
        } catch (Exception ex) {
            log.error("An exception has ocurred on addInterestCollection.");
            throw ex;
        }

    }

    public static void removeInterestCollection(InterestCollectionElement interestCollectionElement) throws Exception {
        log.info("removeInterestCollection");
        try {
            InterestCollectionElementAPIHandler.removeInterestCollection(interestCollectionElement);
        } catch (Exception ex) {
            log.error("An exception has ocurred on removeInterestCollection.");
            throw ex;
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="FollowingInterestCollection Functions">
    public static List<FollowingInterestCollection> getFollowingInterestCollectionByCollectionId(String interestCollectionId) throws Exception {
        log.info("getFollowinInterestCollectionByCollectionId");
        List<FollowingInterestCollection> followingInterestCollections = null;
        try {
            followingInterestCollections = FollowingInterestCollectionAPIHandler.getFollowingInterestCollectionByCollectionId(interestCollectionId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowinInterestCollectionByCollectionId.");
            throw ex;
        }
        return followingInterestCollections;
    }

    public static int getFollowingInterestCollectionCount(String interestCollectionId) throws Exception {
        log.info("getFollowingInterestCollectionCount");
        int count;
        try {
            count = FollowingInterestCollectionAPIHandler.getFollowingInterestCollectionCount(interestCollectionId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getFollowingInterestCollectionCount.");
            throw ex;
        }
        return count;
    }

    public static void saveFollowingInterestCollection(FollowingInterestCollection followingInterestCollection) throws Exception {
        log.info("saveFollowingInterestCollection");
        try {
            FollowingInterestCollectionAPIHandler.saveFollowingInterestCollection(followingInterestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveFollowingInterestCollection.");
            throw ex;
        }
    }

    public static void deleteFollowingInterestCollection(FollowingInterestCollection followingInterestCollection) throws Exception {
        log.info("deleteFollowingInterestCollection");
        try {
            FollowingInterestCollectionAPIHandler.deleteFollowingInterestCollection(followingInterestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteFollowingInterestCollection.");
            throw ex;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="InterestCollectionFollowed Functions">
    public static List<InterestCollection> getInterestCollectionFollowedByUserId(String userId) throws Exception {
        log.info("getInterestCollectionFollowedByUserId");
        List<InterestCollection> interestCollectionsFollowed = null;
        try {
            interestCollectionsFollowed = InterestCollectionFollowedAPIHandler.getInterestCollectionFollowedByUserId(userId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getInterestCollectionFollowedByUserId.");
            throw ex;
        }
        return interestCollectionsFollowed;
    }

    public static List<InterestCollection> getInterestCollectionOwnedByUserId(String userId) throws Exception {
        return getInterestCollectionOwnedByUserId(userId, false);
    }

    public static List<InterestCollection> getInterestCollectionOwnedByUserId(String userId, boolean retrieveElements) throws Exception {
        log.info("getInterestCollectionOwnedByUserId");
        List<InterestCollection> interestCollectionsOwned = null;
        try {
            interestCollectionsOwned = InterestCollectionFollowedAPIHandler.getInterestCollectionOwnedByUserId(userId, retrieveElements);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getInterestCollectionOwnedByUserId.");
            throw ex;
        }
        return interestCollectionsOwned;
    }

    public static int getInterestCollectionFollowedCount(String userId) throws Exception {
        log.info("getInterestCollectionFollowedCount");
        int count;
        try {
            count = InterestCollectionFollowedAPIHandler.getInterestCollectionFollowedCount(userId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getInterestCollectionFollowedCount.");
            throw ex;
        }
        return count;
    }

    public static void saveInterestCollectionFollowed(InterestCollectionFollowed interestCollectionFollowed) throws Exception {
        log.info("saveFollowingInterestCollection");
        try {
            InterestCollectionFollowedAPIHandler.saveInterestCollectionFollowed(interestCollectionFollowed);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveFollowingInterestCollection.");
            throw ex;
        }
    }

    public static void deleteInterestCollectionFollowed(InterestCollectionFollowed interestCollectionFollowed) throws Exception {
        log.info("deleteInterestCollectionFollowed");
        try {
            InterestCollectionFollowedAPIHandler.deleteInterestCollectionFollowed(interestCollectionFollowed);
        } catch (Exception ex) {
            log.error("An exception has ocurred on deleteInterestCollectionFollowed.");
            throw ex;
        }
    }

    public static void updateInterestCollectionFollowed(String userId) throws Exception {
        log.info("deleteInterestCollectionFollowed");
        try {
            InterestCollectionFollowedAPIHandler.updateInterestCollectionFollowed(userId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on updateInterestCollectionFollowed.");
            throw ex;
        }
    }

    public static InterestCollectionFollowed getInterestCollectionFollowedByUserIdAndCollectionId(String userId, String collectionId) throws Exception {
        log.info("getInterestCollectionFollowedByUserIdAndCollectionId");
        try {
            return InterestCollectionFollowedAPIHandler.getInterestCollectionFollowedByUserIdAndCollectionId(userId, collectionId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getInterestCollectionFollowedByUserIdAndCollectionId.");
            throw ex;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="NodeInterestCollection Functions">
    public static void saveNodeInterestCollection(NodeInterestCollection nodeInterestCollection) throws Exception {
        log.info("saveNodeInterestCollection");
        try {
            NodeInterestCollectionAPIHandler.saveNodeInterestCollection(nodeInterestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on saveNodeInterestCollection.");
            throw ex;
        }
    }

    public static List<InterestCollection> getInterestCollectionByNodeId(String nodeId) throws Exception {
        log.info("getNodeInterestCollection");
        List<InterestCollection> interestCollections = null;
        try {
            interestCollections = NodeInterestCollectionAPIHandler.getInterestCollectionByNodeId(nodeId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on getNodeInterestCollection.");
            throw ex;
        }
        return interestCollections;

    }

    public static void removeNodeInterestCollection(NodeInterestCollection nodeInterestCollection) throws Exception {
        log.info("removeNodeInterestCollection");
        try {
            NodeInterestCollectionAPIHandler.removeNodeInterestCollection(nodeInterestCollection);
        } catch (Exception ex) {
            log.error("An exception has ocurred on removeNodeInterestCollection.");
            throw ex;
        }
    }

    public static void updateNodeInterestCollection(String nodeId) throws Exception {
        log.info("updateNodeInterestCollection");
        try {
            NodeInterestCollectionAPIHandler.updateNodeInterestCollection(nodeId);
        } catch (Exception ex) {
            log.error("An exception has ocurred on updateNodeInterestCollection.");
            throw ex;
        }
    }
    //</editor-fold>
}
