/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellisophic.linkapedia.api.beans;

import com.amazonaws.services.dynamodb.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodb.datamodeling.DynamoDBTable;
import com.intellisophic.linkapedia.amazon.services.dynamodb.annotations.AmazonDynamoDB;
import java.io.Serializable;
import java.nio.ByteBuffer;



/**
 *
 * @author Sahar Ebadi
 */
@AmazonDynamoDB(tableName = "Doc_Node", hashKey = "docId", hashKeyType = "S")
@DynamoDBTable(tableName = "Doc_Node")
public class Doc_Node implements Serializable {

    private String docId;

    /**
     * MD5 Hash of the docURL.
     */
    private ByteBuffer nodeList;
    
    private ByteBuffer sentences;


    public void setDocId(String docId) {
        this.docId = docId;
    }

    public void setNodeList(ByteBuffer nodeList) throws Exception {
        //ByteBuffer nodeListBytBuff =  compressString(nodeList);
        this.nodeList = nodeList;
    }


    @DynamoDBHashKey(attributeName = "docId")
    public String getDocId() {
        return docId;
    }

    @DynamoDBAttribute(attributeName = "nodeList")
    public ByteBuffer getNodeList(){
        return nodeList;
    }
    
    
   @DynamoDBAttribute(attributeName = "sentences")
    public ByteBuffer getSentences() {
        return sentences;
    }

    /**
     * @param sentences the sentences to set
     */
    public void setSentences(ByteBuffer sentences)  throws Exception {
        this.sentences = sentences;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Doc_Node{" + "docId=").append(docId).append(", nodeList=").append(nodeList).append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.docId != null ? this.docId.hashCode() : 0);
        hash = 29 * hash + (this.nodeList != null ? this.nodeList.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Doc_Node other = (Doc_Node) obj;
        if (this.docId != other.docId && (this.docId == null || !this.docId.equals(other.docId))) {
            return false;
        }
        if ((this.nodeList == null) ? (other.nodeList != null) : !this.nodeList.equals(other.nodeList)) {
            return false;
        }
        return true;
    }



}
