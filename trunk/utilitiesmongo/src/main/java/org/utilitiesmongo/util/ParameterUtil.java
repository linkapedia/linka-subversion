package org.utilitiesmongo.util;

import java.util.HashMap;
import java.util.Map;

public class ParameterUtil {

	public static final String MONGO_HOST = "mongo.host";
	public static final String MONGO_PORT = "mongo.port";
	public static final String MONGO_USER = "mongo.user";
	public static final String MONGO_PASS = "mongo.pass";

	public static final Map<String, String> OPTIONMAPPING = new HashMap<String, String>();

	static {
		OPTIONMAPPING.put("-t", "TAXONOMY");
		OPTIONMAPPING.put("-dfile", "PATH");
		OPTIONMAPPING.put("--h", "HELP");
	}

}
